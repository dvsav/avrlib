#ifndef _ADC_H
#define _ADC_H

#define ADC_REFERENCE_AREF      ( 0b00 << REFS0 )
#define ADC_REFERENCE_AVCC      ( 0b01 << REFS0 )
#define ADC_REFERENCE_INTERNAL  ( 0b11 << REFS0 )

#define ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC0  ( 0b00000 << MUX0 )
#define ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC1  ( 0b00001 << MUX0 )
#define ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC2  ( 0b00010 << MUX0 )
#define ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC3  ( 0b00011 << MUX0 )
#define ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC4  ( 0b00100 << MUX0 )
#define ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC5  ( 0b00101 << MUX0 )
#define ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC6  ( 0b00110 << MUX0 )
#define ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC7  ( 0b00111 << MUX0 )
#define ADC_INPUT_CHANNEL_SINGLE_ENDED_1_22V ( 0b11110 << MUX0 )
#define ADC_INPUT_CHANNEL_SINGLE_ENDED_0V    ( 0b11111 << MUX0 )

#define ADC_POSITIVE_ADC0 0
#define ADC_POSITIVE_ADC1 1
#define ADC_POSITIVE_ADC2 2
#define ADC_POSITIVE_ADC3 3
#define ADC_POSITIVE_ADC4 4
#define ADC_POSITIVE_ADC5 5
#define ADC_POSITIVE_ADC6 6
#define ADC_POSITIVE_ADC7 7

#define ADC_NEGATIVE_ADC0 0
#define ADC_NEGATIVE_ADC1 1
#define ADC_NEGATIVE_ADC2 2

#define ADC_GAIN_1   1
#define ADC_GAIN_10  10
#define ADC_GAIN_200 200

#define ADC_PRESCALER_2   ( 0b001 << ADPS0 )
#define ADC_PRESCALER_4   ( 0b010 << ADPS0 )
#define ADC_PRESCALER_8   ( 0b011 << ADPS0 )
#define ADC_PRESCALER_16  ( 0b100 << ADPS0 )
#define ADC_PRESCALER_32  ( 0b101 << ADPS0 )
#define ADC_PRESCALER_64  ( 0b110 << ADPS0 )
#define ADC_PRESCALER_128 ( 0b111 << ADPS0 )

#define ADC_TRIGGER_SOURCE_FREE_RUN             ( 0b000 << ADTS0 )
#define ADC_TRIGGER_SOURCE_ANALOG_COMPARATOR    ( 0b001 << ADTS0 )
#define ADC_TRIGGER_SOURCE_EXTERNAL_IRQ0        ( 0b010 << ADTS0 )
#define ADC_TRIGGER_SOURCE_TIMER0_COMP_MATCH    ( 0b011 << ADTS0 )
#define ADC_TRIGGER_SOURCE_TIMER0_OVERFLOW      ( 0b100 << ADTS0 )
#define ADC_TRIGGER_SOURCE_TIMER1_COMP_MATCH_B  ( 0b101 << ADTS0 )
#define ADC_TRIGGER_SOURCE_TIMER1_OVERFLOW      ( 0b110 << ADTS0 )
#define ADC_TRIGGER_SOURCE_TIMER1_CAPTURE_EVENT ( 0b111 << ADTS0 )

#include <avr/io.h>

/*
Selects the reference voltage source to the ADC.
'ref' parameter can be one of the following:
    ADC_REFERENCE_AREF     - AREF, Internal Vref turned off.
    ADC_REFERENCE_AVCC     - AVCC with external capacitor at AREF pin.
    ADC_REFERENCE_INTERNAL - Internal 2.56V Voltage Reference with external capacitor at AREF pin.
*/
void ADC_SetReferenceVoltage(unsigned char ref);

/*
Selects the single ended analog input to the ADC.
'channel' parameter can be one of the following:
    ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC0  - use ADC0 as single ended input  channel.
    ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC1  - use ADC1 as single ended input  channel.
    ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC2  - use ADC2 as single ended input  channel.
    ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC3  - use ADC3 as single ended input  channel.
    ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC4  - use ADC4 as single ended input  channel.
    ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC5  - use ADC5 as single ended input  channel.
    ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC6  - use ADC6 as single ended input  channel.
    ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC7  - use ADC7 as single ended input  channel.
    ADC_INPUT_CHANNEL_SINGLE_ENDED_1_22V - use constant 1.22V as single ended input channel.
    ADC_INPUT_CHANNEL_SINGLE_ENDED_0V    - use constant 0V as single ended input channel.
*/
void ADC_SetSingleEndedInputChannel(unsigned char channel);

/*
Selects the positive and negative inputs to the ADC as well as gain.
'positive' parameter can be one of the following:
    ADC_POSITIVE_ADC0 - use ADC0 as positive ADC input.
    ADC_POSITIVE_ADC1 - use ADC1 as positive ADC input.
    ADC_POSITIVE_ADC2 - use ADC2 as positive ADC input.
    ADC_POSITIVE_ADC3 - use ADC3 as positive ADC input.
    ADC_POSITIVE_ADC4 - use ADC4 as positive ADC input.
    ADC_POSITIVE_ADC5 - use ADC5 as positive ADC input.
    ADC_POSITIVE_ADC6 - use ADC6 as positive ADC input.
    ADC_POSITIVE_ADC7 - use ADC7 as positive ADC input.
'negative' parameter can be one of the following:
    ADC_NEGATIVE_ADC0 - use ADC0 as negative ADC input.
    ADC_NEGATIVE_ADC1 - use ADC1 as negative ADC input.
    ADC_NEGATIVE_ADC2 - use ADC2 as negative ADC input.
'gain' parameter can be one of the following:
    ADC_GAIN_1   - 1x gain.
    ADC_GAIN_10  - 10x gain.
    ADC_GAIN_200 - 200x gain.
See page 220 of ATMega8535 datasheet to find which combinations of these parameters are acceptable.
In case the combination of parameters is not acceptable, ADC0 is set as positive input, ADC1 is set as negative input
and the gain is set as 1.
*/
void ADC_SetDifferentialChannel(
    unsigned char positive,
    unsigned char negative,
    unsigned char gain);

/*
Determines the division factor between the XTAL frequency and the input
clock to the ADC.
'prescaler' parameter can be one of the following:
    ADC_PRESCALER_2   - sets frequency of ADC input clock as (XTAL_frequency)/2
    ADC_PRESCALER_4   - sets frequency of ADC input clock as (XTAL_frequency)/4
    ADC_PRESCALER_8   - sets frequency of ADC input clock as (XTAL_frequency)/8
    ADC_PRESCALER_16  - sets frequency of ADC input clock as (XTAL_frequency)/16
    ADC_PRESCALER_32  - sets frequency of ADC input clock as (XTAL_frequency)/32
    ADC_PRESCALER_64  - sets frequency of ADC input clock as (XTAL_frequency)/64
    ADC_PRESCALER_128 - sets frequency of ADC input clock as (XTAL_frequency)/128
*/
void ADC_SetPrescaler(unsigned char prescaler);

/*
Selects which source will trigger an ADC conversion.
'source' parameter can be one of the following:
    ADC_TRIGGER_SOURCE_FREE_RUN             - Free Running mode
    ADC_TRIGGER_SOURCE_ANALOG_COMPARATOR    - Analog Comparator
    ADC_TRIGGER_SOURCE_EXTERNAL_IRQ0        - External Interrupt Request 0
    ADC_TRIGGER_SOURCE_TIMER0_COMP_MATCH    - Timer/Counter0 Compare Match
    ADC_TRIGGER_SOURCE_TIMER0_OVERFLOW      - Timer/Counter0 Overflow
    ADC_TRIGGER_SOURCE_TIMER1_COMP_MATCH_B  - Timer/Counter1 Compare Match B
    ADC_TRIGGER_SOURCE_TIMER1_OVERFLOW      - Timer/Counter1 Overflow
    ADC_TRIGGER_SOURCE_TIMER1_CAPTURE_EVENT - Timer/Counter1 Capture Event
*/
void ADC_SetTriggerSource(unsigned char source);

// Enables ADC
inline void ADC_Enable() { ADCSRA |= (1 << ADEN); }

// Disables ADC
inline void ADC_Disable() { ADCSRA &= ~(1 << ADEN); }

// Starts ADC conversion. Use this function to start conversions in free running mode.
inline void ADC_StartConversion() { ADCSRA |= (1 << ADSC); }

// Enables interrupt requests from ADC.
inline void ADC_InterruptEnable() { ADCSRA |= (1 << ADIE); }

// Disables interrupt requests from ADC.
inline void ADC_InterruptDisable() { ADCSRA &= ~(1 << ADIE); }

// Returns ADC conversion result. Not recommended to use while conversion is on process.
inline int ADC_GetConversionResult() { return ( ADCL | (ADCH << 8) ); }

// Returns nonzero value if ADC is converting, NULL otherwise.
inline unsigned char ADC_IsConverting() { return ( ADCSRA & (1 << ADSC) ); }

// Returns nonzero value if ADC conversion result is negative. Recommended to use if ADC uses differential analog input.
inline unsigned char ADC_ConversionResultIsNegative() { return ( ADCH | (1 << 1) ); }

// Launches ADC conversion and returns its result. (10 bit)
unsigned int ADC_MakeConversion10bit();

// Launches ADC conversion and returns its result. (8 bit).
unsigned char ADC_MakeConversion8bit();

#endif
