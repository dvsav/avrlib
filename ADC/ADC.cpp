#include "ADC.h"

#define REFERENCE_MASK     ( 0b11 << REFS0 )
#define INPUT_CHANNEL_MASK ( 0b11111 << MUX0 )
#define PRESCALER_MASK     ( 0b111 << ADPS0 )
#define TRIGGER_SRC_MASK   ( 0b111 << ADTS0 )

void ADC_SetReferenceVoltage(unsigned char ref)
{
    unsigned char temp = ADMUX;
    temp &= ~REFERENCE_MASK;
    temp |= ref;
    ADMUX = temp;
}

void ADC_SetSingleEndedInputChannel(unsigned char channel)
{
    unsigned char temp = ADMUX;
    temp &= ~INPUT_CHANNEL_MASK;
    temp |= channel;
    ADMUX = temp;
}

void ADC_SetDifferentialChannel(
    unsigned char positive,
    unsigned char negative,
    unsigned char gain)
{
    unsigned char temp = ADMUX;
    unsigned char mux = 0b10000;
    temp &= ~INPUT_CHANNEL_MASK;
    switch (gain)
    {
        case ADC_GAIN_10:
        {
            switch (negative)
            {
                case ADC_NEGATIVE_ADC0:
                {
                    switch (positive)
                    {
                        case ADC_POSITIVE_ADC0: mux = 0b01000;
                        case ADC_POSITIVE_ADC1: mux = 0b01001;
                    }
                }
                case ADC_NEGATIVE_ADC2:
                {
                    switch (positive)
                    {
                        case ADC_POSITIVE_ADC2: mux = 0b01100;
                        case ADC_POSITIVE_ADC3: mux = 0b01101;
                    }
                }
            }
        }
        case ADC_GAIN_200:
        {
            switch (negative)
            {
                case ADC_NEGATIVE_ADC0:
                {
                    switch (positive)
                    {
                        case ADC_POSITIVE_ADC0: mux = 0b01010;
                        case ADC_POSITIVE_ADC1: mux = 0b01011;
                    }
                }
                case ADC_NEGATIVE_ADC2:
                {
                    switch (positive)
                    {
                        case ADC_POSITIVE_ADC2: mux = 0b01110;
                        case ADC_POSITIVE_ADC3: mux = 0b01111;
                    }
                }
            }
        }
        case ADC_GAIN_1:
        {
            switch (negative)
            {
                case ADC_NEGATIVE_ADC1:
                {
                    switch (positive)
                    {
                        case ADC_POSITIVE_ADC0: mux = 0b10000;
                        case ADC_POSITIVE_ADC1: mux = 0b10001;
                        case ADC_POSITIVE_ADC2: mux = 0b10010;
                        case ADC_POSITIVE_ADC3: mux = 0b10011;
                        case ADC_POSITIVE_ADC4: mux = 0b10100;
                        case ADC_POSITIVE_ADC5: mux = 0b10101;
                        case ADC_POSITIVE_ADC6: mux = 0b10110;
                        case ADC_POSITIVE_ADC7: mux = 0b10111;
                    }
                }
                case ADC_NEGATIVE_ADC2:
                {
                    switch (positive)
                    {
                        case ADC_POSITIVE_ADC0: mux = 0b11000;
                        case ADC_POSITIVE_ADC1: mux = 0b11001;
                        case ADC_POSITIVE_ADC2: mux = 0b11010;
                        case ADC_POSITIVE_ADC3: mux = 0b11011;
                        case ADC_POSITIVE_ADC4: mux = 0b11100;
                        case ADC_POSITIVE_ADC5: mux = 0b11101;
                    }
                }
            }
        }
    }
    temp |= mux;
    ADMUX = temp;
}

void ADC_SetPrescaler(unsigned char prescaler)
{
    unsigned char temp = ADCSRA;
    temp &= ~PRESCALER_MASK;
    temp |= prescaler;
    ADCSRA = temp;
}

void ADC_SetTriggerSource(unsigned char source)
{
    switch (source)
    {
        case ADC_TRIGGER_SOURCE_FREE_RUN:
        {
            ADCSRA &= ~( 1 << ADATE );
            SFIOR &= ~TRIGGER_SRC_MASK;
            break;
        }
        case ADC_TRIGGER_SOURCE_ANALOG_COMPARATOR:
        case ADC_TRIGGER_SOURCE_EXTERNAL_IRQ0:
        case ADC_TRIGGER_SOURCE_TIMER0_COMP_MATCH:
        case ADC_TRIGGER_SOURCE_TIMER0_OVERFLOW:
        case ADC_TRIGGER_SOURCE_TIMER1_COMP_MATCH_B:
        case ADC_TRIGGER_SOURCE_TIMER1_OVERFLOW:
        case ADC_TRIGGER_SOURCE_TIMER1_CAPTURE_EVENT:
        {
            ADCSRA |= ( 1 << ADATE );
            unsigned char temp = SFIOR;
            temp &= ~TRIGGER_SRC_MASK;
            temp |= source;
            SFIOR = temp;
            break;
        }
    }
}

unsigned int ADC_MakeConversion10bit()
{
    ADMUX &= ~(1 << ADLAR);
    ADC_StartConversion();
    while (ADC_IsConverting()){};
    return ADC_GetConversionResult();
}

unsigned char ADC_MakeConversion8bit()
{
    ADMUX |= (1 << ADLAR);
    ADC_StartConversion();
    while (ADC_IsConverting()){};
    return ADCH;
}
