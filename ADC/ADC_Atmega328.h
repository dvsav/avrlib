#if !defined(_ADC_H) && defined (__AVR_ATmega328__)
#define _ADC_H

#include <avr/interrupt.h>
#include <util/atomic.h>

#define ADC_REFERENCE_MASK (0b11 << REFS0)
#define ADC_REFERENCE_AREF     (0b00 << REFS0)
#define ADC_REFERENCE_AVCC     (0b01 << REFS0)
#define ADC_REFERENCE_INTERNAL (0b11 << REFS0)

#define ADC_INPUT_CHANNEL_MASK (0b1111 << MUX0)
#define ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC0  (0b0000 << MUX0)
#define ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC1  (0b0001 << MUX0)
#define ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC2  (0b0010 << MUX0)
#define ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC3  (0b0011 << MUX0)
#define ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC4  (0b0100 << MUX0)
#define ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC5  (0b0101 << MUX0)
#define ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC6  (0b0110 << MUX0)
#define ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC7  (0b0111 << MUX0)
#define ADC_INPUT_CHANNEL_TEMPERATURE_SENSOR (0b1000 << MUX0)
#define ADC_INPUT_CHANNEL_1_1V               (0b1110 << MUX0)
#define ADC_INPUT_CHANNEL_GND                (0b1111 << MUX0)

#define ADC_TRIGGER_SRC_MASK (0b111 << ADTS0)
#define ADC_TRIGGER_SOURCE_FREE_RUN             (0b000 << ADTS0)
#define ADC_TRIGGER_SOURCE_ANALOG_COMPARATOR    (0b001 << ADTS0)
#define ADC_TRIGGER_SOURCE_EXTERNAL_IRQ0        (0b010 << ADTS0)
#define ADC_TRIGGER_SOURCE_TIMER0_COMP_MATCH_A  (0b011 << ADTS0)
#define ADC_TRIGGER_SOURCE_TIMER0_OVERFLOW      (0b100 << ADTS0)
#define ADC_TRIGGER_SOURCE_TIMER1_COMP_MATCH_B  (0b101 << ADTS0)
#define ADC_TRIGGER_SOURCE_TIMER1_OVERFLOW      (0b110 << ADTS0)
#define ADC_TRIGGER_SOURCE_TIMER1_CAPTURE_EVENT (0b111 << ADTS0)

#define ADC_PRESCALER_MASK (0b111 << ADPS0)
#define ADC_PRESCALER_2   (0b001 << ADPS0)
#define ADC_PRESCALER_4   (0b010 << ADPS0)
#define ADC_PRESCALER_8   (0b011 << ADPS0)
#define ADC_PRESCALER_16  (0b100 << ADPS0)
#define ADC_PRESCALER_32  (0b101 << ADPS0)
#define ADC_PRESCALER_64  (0b110 << ADPS0)
#define ADC_PRESCALER_128 (0b111 << ADPS0)

// Enables ADC (other functions will not take effect until ADC is enabled)
inline void ADC_Enable()
{
    PRR &= ~(1 << PRADC);
    ADCSRA |= (1 << ADEN);
}

// Disables ADC
inline void ADC_Disable()
{
    PRR |= (1 << PRADC);
    ADCSRA &= ~(1 << ADEN);
}

/*
Selects the reference voltage source for the ADC.
[ref] parameter can be one of the following:
    ADC_REFERENCE_AREF     - AREF, Internal Vref turned off.
    ADC_REFERENCE_AVCC     - AVCC with external capacitor at AREF pin.
    ADC_REFERENCE_INTERNAL - Internal 1.1V Voltage Reference with external capacitor at AREF pin.
Remarks: If there is an external source connected to AREF, you should only use ADC_REFERENCE_AREF. Otherwise the MCU will be damaged.
*/
inline void ADC_SetReferenceVoltage(unsigned char ref)
{
    unsigned char temp = ADMUX;
    temp &= ~ADC_REFERENCE_MASK;
    temp |= ref;
    ADMUX = temp;
}

/*
Selects the single ended analog input to the ADC.
[channel] parameter can be one of the following:
    ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC0  - ADC0
    ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC1  - ADC1
    ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC2  - ADC2
    ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC3  - ADC3
    ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC4  - ADC4
    ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC5  - ADC5
    ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC6  - ADC6
    ADC_INPUT_CHANNEL_SINGLE_ENDED_ADC7  - ADC7
    ADC_INPUT_CHANNEL_TEMPERATURE_SENSOR - Temperature sensor
    ADC_INPUT_CHANNEL_1_1V               - 1.1V (VBG)
    ADC_INPUT_CHANNEL_GND                - 0V (GND)
Remarks: When selecting temperature sensor as input you must set reference voltage to internal 1.1V.
         Temperature sensor has the linear voltage vs temperature relationship:
             242 mV at -45 Celsius degrees
             314 mV at +25 Celsius degrees
             380 mV at +85 Celsius degrees
*/
inline void ADC_SetSingleEndedInputChannel(unsigned char channel)
{
    unsigned char temp = ADMUX;
    temp &= ~ADC_INPUT_CHANNEL_MASK;
    temp |= channel;
    ADMUX = temp;
}

/*
When the respective bits of [channel_mask] are written to logic one, the digital input buffer on the corresponding ADC pin is
disabled. The corresponding PIN Register bit will always read as zero when this bit is set. When an
analog signal is applied to the ADC7...0 pin and the digital input from this pin is not needed, this bit should
be written logic one to reduce power consumption in the digital input buffer.
This function can be used in combination with ADC_SetSingleEndedInputChannel().
*/
inline void ADC_DisableDigitalInputChannels(unsigned char channel_mask)
{
    DIDR0 |= channel_mask;
}

// See ADC_DisableDigitalInputChannels().
inline void ADC_EnableDigitalInputChannels(unsigned char channel_mask)
{
    DIDR0 &= ~channel_mask;
}

/*
Sets the division factor between the system clock frequency and the input clock to the ADC.
[prescaler] parameter can be one of the following:
    ADC_PRESCALER_2   - clk / 2   (16MHz / 2 = 8 MHz, 8-bit resolution only)
    ADC_PRESCALER_4   - clk / 4   (16MHz / 4 = 4 MHz, 8-bit resolution only)
    ADC_PRESCALER_8   - clk / 8   (16MHz / 8 = 2 MHz, 8-bit resolution only)
    ADC_PRESCALER_16  - clk / 16  (16MHz / 16 = 1 MHz, 8-bit resolution only)
    ADC_PRESCALER_32  - clk / 32  (16MHz / 32 = 500 kHz, 8-bit resolution only)
    ADC_PRESCALER_64  - clk / 64  (16MHz / 64 = 250 kHz, 8-bit resolution only)
    ADC_PRESCALER_128 - clk / 128 (16MHz / 128 = 125 kHz, 10-bit and 8-bit resolution)
Remarks: use ADC clock frequency below 200 kHz to get the maximum ADC resolution.
*/
void ADC_SetPrescaler(unsigned char prescaler)
{
    unsigned char temp = ADCSRA;
    temp &= ~ADC_PRESCALER_MASK;
    temp |= prescaler;
    ADCSRA = temp;
}

// Enables conversion autotriggering.
inline void ADC_EnableAutoTriggering()
{
    ADCSRA |= (1 << ADATE);
}

// Disables conversion autotriggering.
inline void ADC_DisableAutoTriggering()
{
    ADCSRA &= ~(1 << ADATE);
}

/*
Selects which source will trigger an ADC conversion.
[source] parameter can be one of the following:
    ADC_TRIGGER_SOURCE_FREE_RUN - constantly sampling and updating the ADC Data Register (ADC interrupt flag is the trigger source). Invoke ADC_InterruptEnable() in advance.
    ADC_TRIGGER_SOURCE_ANALOG_COMPARATOR    
    ADC_TRIGGER_SOURCE_EXTERNAL_IRQ0        
    ADC_TRIGGER_SOURCE_TIMER0_COMP_MATCH_A  
    ADC_TRIGGER_SOURCE_TIMER0_OVERFLOW      
    ADC_TRIGGER_SOURCE_TIMER1_COMP_MATCH_B  
    ADC_TRIGGER_SOURCE_TIMER1_OVERFLOW      
    ADC_TRIGGER_SOURCE_TIMER1_CAPTURE_EVENT
Remarks: 1) Invoke ADC_EnableAutoTriggering() before calling this function.
         2) ADC starts conversion only on the positive edge of the trigger source.
*/
inline void ADC_SetAutoTriggerSource(unsigned char source)
{
    unsigned char temp = ADCSRB;
    temp &= ~ADC_TRIGGER_SRC_MASK;
    temp |= source;
    ADCSRB = temp;
}

/*
Starts a single ADC conversion.
Use this function to start conversions in free running mode.
Make sure that conversion autotriggering is disabled (see ADC_DisableAutoTriggering()).
*/
inline void ADC_StartConversion()
{
    ADCSRA |= (1 << ADSC);
}

// Returns nonzero value if ADC is converting at the moment and zero otherwise.
inline unsigned char ADC_IsConverting()
{
    return ( ADCSRA & (1 << ADSC) );
}

/*
Returns ADC conversion result (10 bits).
Not recommended to use while conversion is in progress.
*/
inline int ADC_GetConversionResult_10bit()
{
    int temp;
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        // ADCL must be read first!
        temp = ( ADCL | (ADCH << 8) );
    }
    return temp;
}

/*
Returns ADC conversion result (10 bits).
Not recommended to use while conversion is in progress.
*/
inline unsigned char ADC_GetConversionResult_8bit()
{
    return ADCH;
}

/*
Launches a single ADC conversion and returns its result (10 bits)
Returns: the ADC result is Vin * 1024 / Vref
*/
inline int ADC_MakeConversion_10bit()
{
    ADMUX &= ~(1 << ADLAR);
    ADC_StartConversion();
    while (ADC_IsConverting()){};
    return ADC_GetConversionResult_10bit();
}

/*
Launches a single ADC conversion and returns its result (8 bits).
Returns: the ADC result is Vin * 256 / Vref
*/
inline unsigned char ADC_MakeConversion_8bit()
{
    ADMUX |= (1 << ADLAR);
    ADC_StartConversion();
    while (ADC_IsConverting()){};
    return ADC_GetConversionResult_8bit();
}

/*
Enables the interrupt on conversion completion event.
Remarks: use #include <avr/interrupt.h> ISR(ADC_vect) { ... } for interrupt handling
*/
inline void ADC_InterruptEnable()
{
    ADCSRA |= (1 << ADIE);
}

// Disables the interrupt on conversion completion event.
inline void ADC_InterruptDisable()
{
    ADCSRA &= ~(1 << ADIE);
}

#endif
