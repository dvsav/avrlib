// External interrupts library

#if !defined(_EXTINT_H) && defined (__AVR_ATmega328__)
#define _EXTINT_H

#include <avr/io.h>
#include <avr/interrupt.h>

// Enables external pin INT0 interrupt.
// Remarks: use ISR(INT0_vect) { ... } for interrupt handling
inline void EXTINT_INT0_Enable()
{
    EIMSK |= (1 << INT0);
}

// Disables external pin INT0 interrupt.
inline void EXTINT_INT0_Disable()
{
    EIMSK &= ~(1 << INT0);
}

inline bool EXTINT_INT0_IsEnabled()
{
    return (EIMSK & (1 << INT0)) != 0;
}

// Enables external pin INT1 interrupt.
// Remarks: use ISR(INT1_vect) { ... } for interrupt handling
inline void EXTINT_INT1_Enable()
{
    EIMSK |= (1 << INT1);
}

// Disables external pin INT1 interrupt.
inline void EXTINT_INT1_Disable()
{
    EIMSK &= ~(1 << INT1);
}

inline bool EXTINT_INT1_IsEnabled()
{
    return (EIMSK & (1 << INT1)) != 0;
}

// Setups the triggering mode for INT1.
#define EXTINT_INT1_TRIG_MASK (0b11 << ISC10)
// [mode] can be one of the follwing:
#define EXTINT_INT1_TRIG_LOW_LEVEL    (0b00 << ISC10) // The low level of INT1 generates an interrupt request
#define EXTINT_INT1_TRIG_ANY_CHANGE   (0b01 << ISC10) // Any logical change on INT1 generates an interrupt request
#define EXTINT_INT1_TRIG_FALLING_EDGE (0b10 << ISC10) // The falling edge of INT1 generates an interrupt request
#define EXTINT_INT1_TRIG_RISING_EDGE  (0b11 << ISC10) // The rising edge of INT1 generates an interrupt request
inline void EXTINT_INT1_SetTrigMode(unsigned char mode)
{
    unsigned char temp = EICRA & ~EXTINT_INT1_TRIG_MASK;
    temp |= ( mode & EXTINT_INT1_TRIG_MASK );
    EICRA = temp;
}

inline unsigned char EXTINT_INT1_GetTrigMode()
{
    return EICRA & EXTINT_INT1_TRIG_MASK;
}

// Setups the triggering mode for INT0.
#define EXTINT_INT0_TRIG_MASK (0b11 << ISC00)
// [mode] can be one of the follwing:
#define EXTINT_INT0_TRIG_LOW_LEVEL    (0b00 << ISC00) // The low level of INT0 generates an interrupt request
#define EXTINT_INT0_TRIG_ANY_CHANGE   (0b01 << ISC00) // Any logical change on INT0 generates an interrupt request
#define EXTINT_INT0_TRIG_FALLING_EDGE (0b10 << ISC00) // The falling edge of INT0 generates an interrupt request
#define EXTINT_INT0_TRIG_RISING_EDGE  (0b11 << ISC00) // The rising edge of INT0 generates an interrupt request
inline void EXTINT_INT0_SetTrigMode(unsigned char mode)
{
    unsigned char temp = EICRA & ~EXTINT_INT0_TRIG_MASK;
    temp |= ( mode & EXTINT_INT0_TRIG_MASK );
    EICRA = temp;
}

inline unsigned char EXTINT_INT0_GetTrigMode()
{
    return EICRA & EXTINT_INT0_TRIG_MASK;
}

// Clears INT0 interrupt flag.
inline void EXTINT_INT0_ClearInterruptFlag()
{
    EIFR |= (1 << INTF0);
}

// Clears INT1 interrupt flag.
inline void EXTINT_INT1_ClearInterruptFlag()
{
    EIFR |= (1 << INTF1);
}

// Enables pin change interrupt 2 on any enabled PCINT[23:16] pin.
// Remarks: use ISR(PCINT2_vect) { ... } for interrupt handling
// Related functions: EXTINT_PCINT2_SelectPins()
inline void EXTINT_PCINT2_Enable()
{
    PCICR |= (1 << PCIE2);
}

// Enables pin change interrupt 1 on any enabled PCINT[14:8] pin.
// Remarks: use ISR(PCINT1_vect) { ... } for interrupt handling
// Related functions: EXTINT_PCINT1_SelectPins()
inline void EXTINT_PCINT1_Enable()
{
    PCICR |= (1 << PCIE1);
}

// Enables pin change interrupt 0 on any enabled PCINT[7:0] pin.
// Remarks: use ISR(PCINT0_vect) { ... } for interrupt handling
// Related functions: EXTINT_PCINT0_SelectPins()
inline void EXTINT_PCINT0_Enable()
{
    PCICR |= (1 << PCIE0);
}

// Clears Pin Change Interrupt Flag 2.
inline void EXTINT_PCINT2_ClearInterruptFlag()
{
    PCIFR |= (1 << PCIF2);
}

// Clears Pin Change Interrupt Flag 1.
inline void EXTINT_PCINT1_ClearInterruptFlag()
{
    PCIFR |= (1 << PCIF1);
}

// Clears Pin Change Interrupt Flag 0.
inline void EXTINT_PCINT0_ClearInterruptFlag()
{
    PCIFR |= (1 << PCIF0);
}

// Selects whether pin change interrupt is enabled on the corresponding I/O pin.
// [pins] is a bitwise OR combination of the following flags:
#define EXTINT_PCINT2_16 (1 << 0)
#define EXTINT_PCINT2_17 (1 << 1)
#define EXTINT_PCINT2_18 (1 << 2)
#define EXTINT_PCINT2_19 (1 << 3)
#define EXTINT_PCINT2_20 (1 << 4)
#define EXTINT_PCINT2_21 (1 << 5)
#define EXTINT_PCINT2_22 (1 << 6)
#define EXTINT_PCINT2_23 (1 << 7)
inline void EXTINT_PCINT2_SelectPins(unsigned char pins)
{
    PCMSK2 = pins;
}

// Selects whether pin change interrupt is enabled on the corresponding I/O pin.
// [pins] is a bitwise OR combination of the following flags:
#define EXTINT_PCINT1_8  (1 << 0)
#define EXTINT_PCINT1_9  (1 << 1)
#define EXTINT_PCINT1_10 (1 << 2)
#define EXTINT_PCINT1_11 (1 << 3)
#define EXTINT_PCINT1_12 (1 << 4)
#define EXTINT_PCINT1_13 (1 << 5)
#define EXTINT_PCINT1_14 (1 << 6)
inline void EXTINT_PCINT1_SelectPins(unsigned char pins)
{
    PCMSK1 = pins;
}

// Selects whether pin change interrupt is enabled on the corresponding I/O pin.
// [pins] is a bitwise OR combination of the following flags:
#define EXTINT_PCINT0_0 (1 << 0)
#define EXTINT_PCINT0_1 (1 << 1)
#define EXTINT_PCINT0_2 (1 << 2)
#define EXTINT_PCINT0_3 (1 << 3)
#define EXTINT_PCINT0_4 (1 << 4)
#define EXTINT_PCINT0_5 (1 << 5)
#define EXTINT_PCINT0_6 (1 << 6)
#define EXTINT_PCINT0_7 (1 << 7)
inline void EXTINT_PCINT0_SelectPins(unsigned char pins)
{
    PCMSK0 = pins;
}

#endif