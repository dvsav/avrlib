// External interrupts library

#ifnedef _EXTINT_H
#define _EXTINT_H

#include <avr/io.h>
#include <avr/interrupt.h>

// Enables external pin INT0 interrupt.
// Remarks: use ISR(INT0_vect) { ... } for interrupt handling
inline void EXTINT_INT0_Enable()
{
    GICR |= (1 << INT0);
}

// Disables external pin INT0 interrupt.
inline void EXTINT_INT0_Disable()
{
    GICR &= ~(1 << INT0);
}

// Enables external pin INT1 interrupt.
// Remarks: use ISR(INT1_vect) { ... } for interrupt handling
inline void EXTINT_INT1_Enable()
{
    GICR |= (1 << INT1);
}

// Disables external pin INT1 interrupt.
inline void EXTINT_INT1_Disable()
{
    GICR &= ~(1 << INT1);
}

// Enables external pin INT2 interrupt.
// Remarks: use ISR(INT2_vect) { ... } for interrupt handling
inline void EXTINT_INT2_Enable()
{
    GICR |= (1 << INT2);
}

// Disables external pin INT2 interrupt.
inline void EXTINT_INT2_Disable()
{
    GICR &= ~(1 << INT2);
}

// Setups the triggering mode for INT1.
#define EXTINT_INT1_TRIG_MASK (0b11 << ISC10)
// [mode] can be one of the follwing:
#define EXTINT_INT1_TRIG_LOW_LEVEL    (0b00 << ISC10) // The low level of INT1 generates an interrupt request
#define EXTINT_INT1_TRIG_ANY_CHANGE   (0b01 << ISC10) // Any logical change on INT1 generates an interrupt request
#define EXTINT_INT1_TRIG_FALLING_EDGE (0b10 << ISC10) // The falling edge of INT1 generates an interrupt request
#define EXTINT_INT1_TRIG_RISING_EDGE  (0b11 << ISC10) // The rising edge of INT1 generates an interrupt request
inline void EXTINT_INT1_SetTrigMode(unsigned char mode)
{
    unsigned char temp = MCUCR & ~EXTINT_INT1_TRIG_MASK;
    temp |= ( mode & EXTINT_INT1_TRIG_MASK );
    MCUCR = temp;
}

// Setups the triggering mode for INT0.
#define EXTINT_INT0_TRIG_MASK (0b11 << ISC00)
// [mode] can be one of the follwing:
#define EXTINT_INT0_TRIG_LOW_LEVEL    (0b00 << ISC00) // The low level of INT0 generates an interrupt request
#define EXTINT_INT0_TRIG_ANY_CHANGE   (0b01 << ISC00) // Any logical change on INT0 generates an interrupt request
#define EXTINT_INT0_TRIG_FALLING_EDGE (0b10 << ISC00) // The falling edge of INT0 generates an interrupt request
#define EXTINT_INT0_TRIG_RISING_EDGE  (0b11 << ISC00) // The rising edge of INT0 generates an interrupt request
inline void EXTINT_INT0_SetTrigMode(unsigned char mode)
{
    unsigned char temp = MCUCR & ~EXTINT_INT0_TRIG_MASK;
    temp |= ( mode & EXTINT_INT0_TRIG_MASK );
    MCUCR = temp;
}

// Setups the triggering mode for INT2.
#define EXTINT_INT2_TRIG_MASK         (1 << ISC2)
// [mode] can be one of the follwing:
#define EXTINT_INT2_TRIG_FALLING_EDGE (0 << ISC2) // Falling edge on INT2 activates the interrupt
#define EXTINT_INT2_TRIG_RISING_EDGE  (1 << ISC2) // Rising edge on INT2 activates the interrupt
inline void EXTINT_INT2_SetTrigMode(unsigned char mode)
{
    unsigned char gicr = GICR; // store GICR in a local variable
    EXTINT_INT2_Disable(); // disable INT2 interrupt
    {
        unsigned char temp = MCUCSR & ~EXTINT_INT2_TRIG_MASK;
        temp |= ( mode & EXTINT_INT2_TRIG_MASK );
        MCUCSR = temp;
    }
    EXTINT_INT2_ClearInterruptFlag(); // clear INT2 interrupt flag
    GICR = gicr; // restore GICR from the local variable
}

// Clears INT0 interrupt flag.
inline void EXTINT_INT0_ClearInterruptFlag()
{
    GIFR |= (1 << INTF0);
}

// Clears INT1 interrupt flag.
inline void EXTINT_INT1_ClearInterruptFlag()
{
    GIFR |= (1 << INTF1);
}

// Clears INT2 interrupt flag.
inline void EXTINT_INT2_ClearInterruptFlag()
{
    GIFR |= (1 << INTF2);
}

#endif