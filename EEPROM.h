#ifndef _EEPROM_H
#define _EEPROM_H

#define EEPROM_READY_INTERRUPT_ENABLE  1
#define EEPROM_READY_INTERRUPT_DISABLE 0

#include <avr/io.h>
#include <avr/interrupt.h>

void EEPROM_Write(unsigned int uiAddress, unsigned char ucData)
{
    unsigned char temp = SREG & (1 << 7);
    cli();
    /* Wait for completion of previous write */
    while(EECR & (1 << EEWE));
    /* Set up Address and Data Registers */
    EEAR = uiAddress;
    EEDR = ucData;
    /* Write logical one to EEMWE */
    EECR |= (1 << EEMWE);
    /* Start eeprom write by setting EEWE */
    EECR |= (1 << EEWE);
    SREG |= temp;
}

unsigned char EEPROM_Read(unsigned int uiAddress)
{
    unsigned char temp = SREG & (1 << 7);
    cli();
    /* Wait for completion of previous write */
    while(EECR & (1 << EEWE));
    /* Set up Address Register */
    EEAR = uiAddress;
    /* Start eeprom read by writing EERE */
    EECR |= (1 << EERE);
    SREG |= temp;
    /* Return data from Data Register */
    return EEDR;
}

inline void EEPROM_SetupInterrputs(unsigned char mode)
{
    if (!mode)
        EECR &= ~(1 << EERIE);
    else if (mode == EEPROM_READY_INTERRUPT_ENABLE)
        EECR |= (1 << EERIE);
}

#endif
