#ifndef _ANALOG_COMPARATOR_H
#define _ANALOG_COMPARATOR_H

#define ANALOG_COMPARATOR_ADMUX_MASK (0b111 << MUX0)
#define ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC0 (0 << MUX0)
#define ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC1 (1 << MUX0)
#define ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC2 (2 << MUX0)
#define ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC3 (3 << MUX0)
#define ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC4 (4 << MUX0)
#define ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC5 (5 << MUX0)
#define ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC6 (6 << MUX0)
#define ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC7 (7 << MUX0)

#define ANALOG_COMPARATOR_POSITIVE_INPUT_AIN0 0
#define ANALOG_COMPARATOR_POSITIVE_INPUT_INTERNAL_VOLTAGE_REFERENCE 1

#define ANALOG_COMPARATOR_INTERRUPT_MODE_MASK (0b11 << ACIS0)
#define ANALOG_COMPARATOR_INTERRUPT_MODE_TOGGLE  (0b00 << ACIS0)
#define ANALOG_COMPARATOR_INTERRUPT_MODE_FALLING (0b10 << ACIS0)
#define ANALOG_COMPARATOR_INTERRUPT_MODE_RISING  (0b11 << ACIS0)

inline void AnalogComparator_Enable()
{
    unsigned char ACIE_flag = ACSR & (1 << ACIE);
    
    // disable analog comparator interrupt.
    ACSR &= ~(1 << ACIE);
    
    ACSR &= ~(1 << ACD);
    
    // turn ACIE flag to its previous state.
    ACSR |= ACIE_flag;
}

inline void AnalogComparator_Disable()
{
    unsigned char ACIE_flag = ACSR & (1 << ACIE);
    
    // disable analog comparator interrupt.
    ACSR &= ~(1 << ACIE);
    
    ACSR |= (1 << ACD);
    
    // turn ACIE flag to its previous state.
    ACSR |= ACIE_flag;
}

/*
Selects one of ADC channels as the negative input to the analog comparator.
[adc_input] parameter can be one of the following:
    ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC0 - ADC0
    ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC1 - ADC1
    ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC2 - ADC2
    ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC3 - ADC3
    ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC4 - ADC4
    ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC5 - ADC5
    ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC6 - ADC6
    ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC7 - ADC7
Remarks: Side effect: this function disables ADC.
*/
inline void AnalogComparator_SetNegativeInputAdcMux(
    unsigned char adc_input)
{
    PRR &= ~(1 << PRADC);   // power up ADC
    ADCSRA &= ~(1 << ADEN); // disable ADC
    ADCSRB |= (1 << ACME);  // use ADC mux for selecting negative input of analog comparator

    unsigned char temp = ADMUX;
    temp &= ~ANALOG_COMPARATOR_ADMUX_MASK;
    temp |= adc_input;
    ADMUX = temp;
}

// Selects AIN1 pin as the negative input to the analog comparator.
inline void AnalogComparator_SetNegativeInputAin1()
{
    ADCSRB &= ~(1 << ACME);
}

/*
Selects the positive input to the analog comparator.
[positive_input] parameter can be one of the following:
    ANALOG_COMPARATOR_POSITIVE_INPUT_INTERNAL_VOLTAGE_REFERENCE - internal 1.1 V reference voltage.
    ANALOG_COMPARATOR_POSITIVE_INPUT_AIN0 - AIN0 pin of the controller
*/
inline void AnalogComparator_SetPositiveInput(unsigned char positive_input)
{
    switch(positive_input)
    {
        case ANALOG_COMPARATOR_POSITIVE_INPUT_INTERNAL_VOLTAGE_REFERENCE:
            ACSR |= (1 << ACBG);
            break;
         
        case ANALOG_COMPARATOR_POSITIVE_INPUT_AIN0:
            ACSR &= ~(1 << ACBG);
            break;
    }
}

// Returns the output of the analog comaparator (0x00 or 0x01).
inline unsigned char AnalogComparator_Output()
{
    return (ACSR >> ACO) & 0x01;
}

/*
Clears analog comparator interrupt flag.
See also: AnalogComparator_InterruptDisable().
*/
inline void AnalogComparator_ClearInterruptFlag()
{
    ACSR |= (1 << ACI);
}

/*
Enables analog comparator interrupt.
See also: AnalogComparator_SetInterruptMode().
Remarks: use #include <avr/interrupt.h> ISR(ANALOG_COMP_vect) { ... } for interrupt handling
*/
inline void AnalogComparator_InterruptEnable()
{
    ACSR |= (1 << ACIE);
}

// Disables analog comparator interrupt.
inline void AnalogComparator_InterruptDisable()
{
    ACSR &= ~(1 << ACIE);
}

/*
Sets which comparator event trigger the analog comparator interrupt.
[mode] parameter can be one of the following:
    ANALOG_COMPARATOR_INTERRUPT_MODE_RISING  - Comparator Interrupt on Output Toggle
    ANALOG_COMPARATOR_INTERRUPT_MODE_FALLING - Comparator Interrupt on Falling Output Edge
    ANALOG_COMPARATOR_INTERRUPT_MODE_TOGGLE  - Comparator Interrupt on Rising Output Edge
*/
inline void AnalogComparator_SetInterruptMode(unsigned char mode)
{
    unsigned char temp = ACSR;
    temp &= ~ANALOG_COMPARATOR_INTERRUPT_MODE_MASK;
    temp |= mode;
    ACSR = temp;
}

/*
Enables triggering of the Input Capture function in Timer/Counter1
by the Analog Comparator.
*/
inline void AnalogComparator_InputCaptureEnable()
{
    ACSR |= (1 << ACIC);
}

/*
Disables triggering of the Input Capture function in Timer/Counter1
by the Analog Comparator.
*/
inline void AnalogComparator_InputCaptureDisable()
{
    ACSR &= ~(1 << ACIC);
}

// Disables the digital input buffer on AIN0 and AIN1 pins.
inline void AnalogComparator_DisableDigitalInputChannels()
{
    DIDR1 |= ( 0b11 << AIN0D );
}

// Enables the digital input buffer on AIN0 and AIN1 pins.
inline void AnalogComparator_EnableDigitalInputChannels()
{
    DIDR1 &= ~( 0b11 << AIN0D );
}

#endif
