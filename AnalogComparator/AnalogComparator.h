#ifndef _ANALOG_COMPARATOR_H
#define _ANALOG_COMPARATOR_H

#include <avr/io.h>

#define ANALOG_COMPARATOR_ENABLE 1
#define ANALOG_COMPARATOR_DISABLE 0

#define ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC0 0
#define ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC1 1
#define ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC2 2
#define ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC3 3
#define ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC4 4
#define ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC5 5
#define ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC6 6
#define ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC7 7
#define ANALOG_COMPARATOR_NEGATIVE_INPUT_AIN1 8

#define ANALOG_COMPARATOR_INTERRUPT_DISABLE 0x00
#define ANALOG_COMPARATOR_INTERRUPT_RISING  0x01
#define ANALOG_COMPARATOR_INTERRUPT_FALLING 0x02
#define ANALOG_COMPARATOR_INTERRUPT_TOGGLE  0x03

#define ANALOG_COMPARATOR_POSITIVE_INPUT_INTERNAL_VOLTAGE_REFERENCE 1
#define ANALOG_COMPARATOR_POSITIVE_INPUT_AIN0 0

#define ANALOG_COMPARATOR_INPUT_CAPTURE_ENABLE 1
#define ANALOG_COMPARATOR_INPUT_CAPTURE_DISABLE 0

/*
Enables or disables analog comparator.
'enable' parameter can be one of the following:
ANALOG_COMPARATOR_ENABLE - enables (switches on) analog comparator.
ANALOG_COMPARATOR_DISABLE - disables (switches off) analog comparator.
*/
inline void AnalogComparator_Enable(unsigned char enable)
{
    unsigned char ACIE_flag = ACSR & (1 << ACIE);
    
    // disable analog comparator interrupt.
    ACSR &= ~(1 << ACIE);
    
    switch(enable)
    {
        case ANALOG_COMPARATOR_ENABLE:
            ACSR &= ~(1 << ACD);
            break;
         
        case ANALOG_COMPARATOR_DISABLE:
            ACSR |= (1 << ACD);
            break;
    }
    
    // turn ACIE flag to its previous state.
    ACSR |= ACIE_flag;
}

/*
Selects the negative input to the analog comparator.
It can be either AIN1 pin or one of ADC pins.
'negative_input' parameter can be one of the following:
ANALOG_COMPARATOR_NEGATIVE_INPUT_AIN1 - AIN1
ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC0 - ADC0
ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC1 - ADC1
ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC2 - ADC2
ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC3 - ADC3
ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC4 - ADC4
ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC5 - ADC5
ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC6 - ADC6
ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC7 - ADC7
*/
inline void AnalogComparator_SelectNegativeInput(unsigned char negative_input)
{
    switch(negative_input)
    {
        case ANALOG_COMPARATOR_NEGATIVE_INPUT_AIN1:
        {
            SFIOR &= ~(1 << ACME);
            break;
        }
        case ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC0:
        case ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC1:
        case ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC2:
        case ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC3:
        case ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC4:
        case ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC5:
        case ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC6:
        case ANALOG_COMPARATOR_NEGATIVE_INPUT_ADC7:
        {
            SFIOR |= (1 << ACME);
            unsigned char temp = ADMUX;
            temp &= ~(0b111 << MUX0);
            temp |= (negative_input << MUX0);
            ADMUX = temp;
            break;
        }
    }
}

/*
Selects the positive input to the analog comparator.
'positive_input' parameter can be one of the following:
ANALOG_COMPARATOR_POSITIVE_INPUT_INTERNAL_VOLTAGE_REFERENCE - positive input of the analog comparator is connected to internal 1.23 V reference voltage.
ANALOG_COMPARATOR_POSITIVE_INPUT_AIN0 - positive input is connected to the AIN0 pin of the controller
*/
inline void AnalogComparator_SelectPositiveInput(unsigned char positive_input)
{
    switch(positive_input)
    {
        case ANALOG_COMPARATOR_POSITIVE_INPUT_INTERNAL_VOLTAGE_REFERENCE:
            ACSR |= (1 << ACBG);
            break;
         
        case ANALOG_COMPARATOR_POSITIVE_INPUT_AIN0:
            ACSR &= ~(1 << ACBG);
            break;
    }
}

/*
Returns the output of the analog comaprator.
*/
inline unsigned char AnalogComparator_Output()
{
    return ACSR & (1 << ACO);
}

/*
Sets up analog comparator interrupt.
'mode' parameter can be one of the following:
ANALOG_COMPARATOR_INTERRUPT_DISABLE - disable analog comparator interrupt.
ANALOG_COMPARATOR_INTERRUPT_RISING  - set comparator interrupt on rising output edge.
ANALOG_COMPARATOR_INTERRUPT_FALLING - set comparator interrupt on falling output edge.
ANALOG_COMPARATOR_INTERRUPT_TOGGLE  - set comparator interrupt on output toggle.
*/
inline void AnalogComparator_SetupInterrupt(unsigned char mode)
{
    // disable analog comparator interrupt.
    ACSR &= ~(1 << ACIE);
    
    switch (flags)
    {
        case COMP_INTERRUPT_DISABLE:
            return;
        
        case COMP_INTERRUPT_RISING: 
            ACSR |= ( (1 << ACIS1) | (1 << ACIS0) ); 
            break;

        case COMP_INTERRUPT_FALLING:
            ACSR &= ~(1 << ACIS0);
            ACSR |= (1 << ACIS1);
            break;

        case COMP_INTERRUPT_TOGGLE:
            ACSR &= ~( (1 << ACIS1) | (1 << ACIS0) );
            break;
    }
    
    ACSR |= (1 << ACIE);
}

/*
Enables or disables the Input Capture function in Timer/Counter1 to be triggered by the Analog Comparator.
'enable' parameter can be one of the following:
ANALOG_COMPARATOR_INPUT_CAPTURE_ENABLE - enables Input Capture function.
ANALOG_COMPARATOR_INPUT_CAPTURE_DISABLE - disables Input Capture function.
*/
inline void AnalogComparator_InputCaptureEnable(unsigned char enable)
{
    switch(enable)
    {
        case ANALOG_COMPARATOR_INPUT_CAPTURE_ENABLE:
            ACSR |= (1 << ACIC);
            break;

        case ANALOG_COMPARATOR_INPUT_CAPTURE_DISABLE:
            ACSR &= ~(1 << ACIC);
            break;
    }
}

#endif
