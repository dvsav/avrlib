#ifndef _SPI_H
#define _SPI_H

#include "IOPORT.h"

// DONT'T FORGET TO PULLUP ALL THE [SPI] LINES AND PARTICULARY [SS#] AND [SCK]!
#if defined (__AVR_ATmega8535__)
    #define SPI_PORT B
    #define SPI_SS 4
    #define SPI_MOSI 5
    #define SPI_MISO 6
    #define SPI_SCK 7
#elif defined (__AVR_ATmega16__)
    #define SPI_PORT B
    #define SPI_SS 4
    #define SPI_MOSI 5
    #define SPI_MISO 6
    #define SPI_SCK 7
#endif

// Initializes SPI as master.
inline void SPI_MasterInit()
{
    IOPORT_CONFIG_PIN_OUT(SPI_PORT, SPI_MOSI);
    IOPORT_CONFIG_PIN_OUT(SPI_PORT, SPI_SCK);
    IOPORT_CONFIG_PIN_OUT(SPI_PORT, SPI_SS);
    
    SPCR |= ((1 << SPE) | (1 << MSTR));
}

/*
Transmits one byte as master.
Parameters: [out_data] - data byte to be transmitted.
Returns: byte received from slave.
*/
inline unsigned char SPI_MasterTransmitByte(
    unsigned char out_data)
{
    SPDR = out_data;
    while(!(SPSR & (1 << SPIF))) {};
    return SPDR;
}

/*
Transmits a bunch of bytes as master.
Parameters:
[out_data] - data to be transmitted.
[in_buffer] - buffer in which the bytes incoming from slave are placed.
[size] - size of byte array.
*/
inline void SPI_MasterTransmit(
    unsigned char* out_data,
    unsigned char* in_buffer,
    int size)
{
    IOPORT_RESET_PIN(SPI_PORT, SPI_SS);
    for(int i = 0; i < size; i++)
    {
        in_buffer[i] = SPI_MasterTransmitByte(out_data[i]);
    }
    IOPORT_SET_PIN(SPI_PORT, SPI_SS);
}

// Initializes SPI as slave.
inline void SPI_SlaveInit()
{
    IOPORT_CONFIG_PIN_OUT(SPI_PORT, SPI_MISO);
    
    SPCR & ~(1 << MSTR);
    SPCR |= (1 << SPE);
}

/*
Receives one byte as slave.
Parameters: [out_data] - data byte to be transmitted by slave.
Returns: data byte received from slave.
*/
inline unsigned char SPI_SlaveReceiveByte(
    unsigned char out_data)
{
    SPDR = out_data;
    while(!(SPSR & (1 << SPIF))) {};
    return SPDR;
}

/*
Receives a bunch of bytes as slave.
Parameters:
[out_data] - data bytes to be transmitted by slave.
[in_buffer] - buffer in which the bytes incoming from master are placed.
[size] - size of byte array.
*/
inline void SPI_SlaveReceive(
    unsigned char* out_data,
    unsigned char* in_buffer,
    int size)
{
    for(int i = 0; i < size; i++)
    {
        in_buffer[i] = SPI_SlaveReceiveByte(out_data[i]);
    }
}

// Sets SPI master clock frequency divider.
// Parameters:
// [freq_divider] determines SPI master clock frequency (SCK).
// Can be one of the following:
// SPSR.SPI2X  SPCR.SPR1  SPCR.SPR0
#define SPI_FREQ_DIVIDER_4   0b000 // f_clock/4
#define SPI_FREQ_DIVIDER_16  0b001 // f_clock/16
//#define SPI_FREQ_DIVIDER_64  0b010 // f_clock/64
#define SPI_FREQ_DIVIDER_128 0b011 // f_clock/128
#define SPI_FREQ_DIVIDER_2   0b100 // f_clock/2
#define SPI_FREQ_DIVIDER_8   0b101 // f_clock/8
#define SPI_FREQ_DIVIDER_32  0b110 // f_clock/32
#define SPI_FREQ_DIVIDER_64  0b111 // f_clock/64
inline void SPI_SetFreqDivider(
    unsigned char freq_divider)
{
    if((freq_divider & 0b100) != 0)
        SPSR |= (1 << SPI2X);
    else
        SPSR &= ~(1 << SPI2X);
    
    unsigned char temp = SPCR;
    temp &= ~(0b011 << SPR0);
    temp |= ((freq_divider & 0b011) << SPR0);
    SPCR = temp;
}

// Enables interrupt.
inline void SPI_InterruptEnable()
{
    SPCR |= (1 << SPIE);
}

// Disables interrupt.
inline void SPI_InterruptDisable()
{
    SPCR &= ~(1 << SPIE);
}

// Disables SPI.
inline void SPI_Disable()
{
    IOPORT_CONFIG_PIN_IN(SPI_PORT, SPI_SS);
    IOPORT_CONFIG_PIN_IN(SPI_PORT, SPI_MISO);
    IOPORT_CONFIG_PIN_IN(SPI_PORT, SPI_MOSI);
    IOPORT_CONFIG_PIN_IN(SPI_PORT, SPI_SCK);

    SPCR &= ~(1 << SPE);
}

// Sets data order (endianness) of data transmission.
#define SPI_MSB_FIRST 0
#define SPI_LSB_FIRST 1
inline void SPI_SetDataOrder(
    unsigned char data_order)
{
    unsigned char temp = SPCR;
    temp &= ~(1 << DORD);
    temp |= ((data_order & 0x01) << DORD);
    SPCR = temp;
}

// Sets clock polarity.
#define SPI_SCK_LEADING_EDGE_RISING 0  // Leading edge - rising,  Trailing edge - falling
#define SPI_SCK_LEADING_EDGE_FALLING 1 // Leading edge - falling, Trailing edge - rising
inline void SPI_SetClockPolarity(
    unsigned char clock_polarity)
{
    unsigned char temp = SPCR;
    temp &= ~(1 << CPOL);
    temp |= ((clock_polarity & 0x01) << CPOL);
    SPCR = temp;
}

// Sets clock phase.
#define SPI_SCK_SAMPLE_LEADING 0  // Trailing edge - setup, Leading edge  - sample
#define SPI_SCK_SAMPLE_TRAILING 1 // Leading edge  - setup, Trailing edge - sample
inline void SPI_SetClockPhase(
    unsigned char clock_phase)
{
    unsigned char temp = SPCR;
    temp &= ~(1 << CPHA);
    temp |= ((clock_phase & 0x01) << CPHA);
    SPCR = temp;
}

#endif
