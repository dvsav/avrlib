#ifndef _UART_H
#define _UART_H

#include <avr/io.h>

#define UART_INTERRUPTS_DISABLED                   0
#define UART_RX_COMPLETE_INTERRUPT_ENABLE         (1 << RXCIE)
#define UART_TX_COMPLETE_INTERRUPT_ENABLE         (1 << TXCIE)
#define UART_DATA_REGISTER_EMPTY_INTERRUPT_ENABLE (1 << UDRIE)

#define UART_DATA_BITS_5 0
#define UART_DATA_BITS_6 1
#define UART_DATA_BITS_7 2
#define UART_DATA_BITS_8 3
#define UART_DATA_BITS_9 4

#define UART_PARITY_OFF  0
#define UART_PARITY_EVEN 2
#define UART_PARITY_ODD  3

#define UART_1_STOP_BIT  0
#define UART_2_STOP_BITS 1

#define UART_UBRR(baud_rate) (F_CPU / (baud_rate) / 16 - 1)

/*
Enables or disables UART interrupts.
'mode' parameter can be either UART_INTERRUPTS_DISABLED
or the logical OR of the following flags:
    UART_RX_COMPLETE_INTERRUPT_ENABLE - enable IRQ after receiving a data frame.
    UART_TX_COMPLETE_INTERRUPT_ENABLE - enable IRQ after transmitting a data frame.
    UART_DATA_REGISTER_EMPTY_INTERRUPT_ENABLE - enable IRQ when UDR is empty.
*/
void UART_SetupInterrputs(unsigned char mode);

/*
Selects the number of data bits.
'size' parameter can be one of the following:
    UART_DATA_BITS_5 - set 5 data bits.
    UART_DATA_BITS_6 - set 6 data bits.
    UART_DATA_BITS_7 - set 7 data bits.
    UART_DATA_BITS_8 - set 8 data bits.
    UART_DATA_BITS_9 - set 9 data bits.
Note that if the character size is 9 bit user must refer to corresponding functions to recieve and transmit data via UART.
*/
void UART_SetDataBitsNumber(unsigned char size);

/*
Sets the type of parity generation and check.
'mode' parameter can be one of the following:
    UART_PARITY_OFF  - disable parity generation and checking.
    UART_PARITY_EVEN - enable parity generation and checking. Even mode.
    UART_PARITY_ODD  - enable parity generation and checking. Odd mode.
*/
void UART_SetParityCheck(unsigned char mode);

/*
Sets the number of stop bits.
'mode' parameter can be one of the following:
    UART_1_STOP_BIT  - 1 stop bit.
    UART_2_STOP_BITS - 2 stop bits.
*/
void UART_SetStopBitsNumber(unsigned char mode);

/*
Sets the UART baud rate.
Use UART_UBRR() macro for the calculation of 'ubrr' parameter.
For example:
    UART_SetBaudRate(UART_UBRR(9600));
where 9600 is the baud rate.
*/
inline void UART_SetBaudRate(unsigned int ubrr)
{
    UBRRH = (unsigned char)((ubrr >> 8) & 0x7F);
    UBRRL = (unsigned char)ubrr;
}

// Enables UART Receiver.
inline void UART_ReceiverEnable()
{ 
    UCSRB |= (1 << RXEN);
}

// Disables UART Receiver.
inline void UART_ReceiverDisable()
{ 
    UCSRB &= ~(1 << RXEN);
}

// Enables UART Transmitter.
inline void UART_TransmitterEnable() 
{ 
    UCSRB |= (1 << TXEN);
}

// Disables UART Transmitter.
inline void UART_TransmitterDisable() 
{
    UCSRB &= ~(1 << TXEN); 
}

// Sends 5 to 8 data bits.
void UART_SendByte(unsigned char data);

// Sends unsigned long (4 bytes, LSB first)
void UART_SendUlong(unsigned long data);

// Sends long (4 bytes, LSB first)
inline void UART_SendLong(long data)
{
    UART_SendUlong(*reinterpret_cast<unsigned long*>(&data));
}

// Sends double (4 bytes, LSB first)
inline void UART_SendDouble(double data)
{
    UART_SendUlong(*reinterpret_cast<unsigned long*>(&data));
}

// Sends 9 data bits.
void UART_Send9DataBits(unsigned int data);

// Receive one byte of data.
char UART_ReceiveByte();

// Receives unsigned long int (4 bytes, LSB first)
unsigned long UART_ReceiveUlong();

// Receives long int (4 bytes, LSB first)
inline long UART_ReceiveLong()
{
    unsigned long val = UART_ReceiveUlong();
    return *reinterpret_cast<long*>(&val);
}

// Receives double (4 bytes, LSB first)
inline double UART_ReceiveDouble()
{
    unsigned long val = UART_ReceiveUlong();
    return *reinterpret_cast<double*>(&val);
}

// Receives 9 data bits.
unsigned int UART_Receive9DataBits();

/*
Sends a string.
'message' parameter must be a zero-terminated array of charachters (C-string).
*/
void UART_SendStr(const char* message);

/*
Gets a string.
Parameters:
'buffer' - an array of bytes where the message will be stored.
'size' - the maximum number of bytes to be stored.
Returns: number of received bytes.
*/
int UART_GetStr(char* buffer, int size);

#endif
