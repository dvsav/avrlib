#include "UART.h"
#include <avr/interrupt.h>
#include <util/atomic.h>

#define UART_IRQ_MASK ( 0b111 << UDRIE )
#define UART_DATA_BITS_MASK ( 0b11 << UCSZ0 )
#define UART_PARITY_MODE_MASK ( 0b11 << UPM0 )

unsigned char UART_ReadUCSRC()
{
    /*
    unsigned char temp = SREG & (1 << 7);
    cli();
    unsigned char ucsrc;
    {
        ucsrc = UBRRH;
        ucsrc = UCSRC;
    }
    SREG |= temp;
    return ucsrc | (1 << URSEL);
    */
    
    unsigned char ucsrc;
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        ucsrc = UBRRH;
        ucsrc = UCSRC;
    }
    return ucsrc | (1 << URSEL);
}

void UART_SetupInterrputs(unsigned char mode)
{
    unsigned char ucsrb = UCSRB;
    ucsrb &= ~UART_IRQ_MASK;
    ucsrb |= mode;
    UCSRB = ucsrb;
}

void UART_SetDataBitsNumber(unsigned char size)
{
    unsigned char ucsrc = UART_ReadUCSRC();
    if (size == UART_DATA_BITS_9)
    {
        UCSRB |= (1 << UCSZ2);
        UCSRC = ucsrc | UART_DATA_BITS_MASK; 
    }
    else
    {
        ucsrc &= ~UART_DATA_BITS_MASK;
        UCSRB &= ~(1 << UCSZ2);
        switch (size)
        {
            case UART_DATA_BITS_5: { break; }
            case UART_DATA_BITS_6: { ucsrc |= (1 << UCSZ0); break; }
            case UART_DATA_BITS_7: { ucsrc |= (1 << UCSZ1); break; }
            case UART_DATA_BITS_8: { ucsrc |= (1 << UCSZ0) | (1 << UCSZ1); break; }
        }
        UCSRC = ucsrc;
    }
}

void UART_SetParityCheck(unsigned char mode)
{
    unsigned char ucsrc = UART_ReadUCSRC();
    ucsrc &= ~UART_PARITY_MODE_MASK;
    ucsrc |= mode;
    UCSRC = ucsrc;
}

void UART_SetStopBitsNumber(unsigned char mode)
{
    unsigned char ucsrc = UART_ReadUCSRC();
    switch(mode)
    {
        case UART_1_STOP_BIT:
            UCSRC = ucsrc & ~(1 << USBS);
            break;
        case UART_2_STOP_BITS:
            UCSRC = ucsrc | (1 << USBS);
            break;
    }
}

void UART_SendByte(unsigned char data)
{
    // Wait for empty transmit buffer
    while (!(UCSRA & (1 << UDRE))) {}
    // Put data into the buffer, sends the data
    UDR = data;
}

void UART_SendUlong(unsigned long data)
{
    UART_SendByte((unsigned char)data);
    UART_SendByte((unsigned char)(data >> 8));
    UART_SendByte((unsigned char)(data >> 16));
    UART_SendByte((unsigned char)(data >> 24));
}

void UART_Send9DataBits(unsigned int data)
{
    UCSRB |= ((((1 << 8) & data) >> 8) << TXB8);
    UDR = (unsigned char)data;
    while(!(UCSRA & (1 << TXC)));
}

char UART_ReceiveByte()
{	
	// Wait for receive
	while (!(UCSRA & (1<<RXC))); 
	// Return data from buffer
	return UDR;
}

unsigned long UART_ReceiveUlong()
{
    return  static_cast<unsigned long>(UART_ReceiveByte()) |
           (static_cast<unsigned long>(UART_ReceiveByte()) << 8) |
           (static_cast<unsigned long>(UART_ReceiveByte()) << 16) |
           (static_cast<unsigned long>(UART_ReceiveByte()) << 24);
}

unsigned int UART_Receive9DataBits()
{
	// Wait for receive
	while (!(UCSRA & (1<<RXC))); 
	// Return data from buffer
    return ((UCSRB & (1 << RXB8)) >> RXB8) & UDR;
}

void UART_SendStr(const char* message)
{
    int i = 0;
    char ch = message[0];
    while (ch != '\0')
    {
        UART_SendByte(ch);
        ch = message[++i];
    } 
}

int UART_GetStr(char* buffer, int size)
{
    char byte;
    int i = 0;
    do
    {
        byte = UART_ReceiveByte();
        buffer[i] = byte;
        ++i;
    } while(byte != '\n' && i < size-1);
    buffer[i] = '\0';
    return i;
}

#undef UART_IRQ_MASK
#undef UART_CHARSIZE_MASK
#undef UART_PARITY_MODE_MASK
