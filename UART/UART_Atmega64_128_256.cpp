#include <avr/interrupt.h>
#include <util/atomic.h>

#include "UART_Atmega64_128_256.h"

#define UART_IRQ_MASK ( 0b111 << UDRIE0 )
#define UART_DATA_BITS_MASK ( 0b11 << UCSZ01 )
#define UART_PARITY_MODE_MASK ( 0b11 << UPM00 )

unsigned char UART_ReadUCSRC()
{
    unsigned char ucsrc;
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        ucsrc = UBRR0H;
        ucsrc = UCSR0C;
    }
    return ucsrc | (1 << UMSEL01);
}

void UART_SetupInterrputs(unsigned char mode)
{
    unsigned char ucsrb = UCSR0B;
    ucsrb &= ~UART_IRQ_MASK;
    ucsrb |= mode;
    UCSR0B = ucsrb;
}

void UART_SetDataBitsNumber(unsigned char size)
{
    unsigned char ucsrc = UART_ReadUCSRC();
    if (size == UART_DATA_BITS_9)
    {
        UCSR0B |= (1 << UCSZ02);
        UCSR0C = ucsrc | UART_DATA_BITS_MASK; 
    }
    else
    {
        ucsrc &= ~UART_DATA_BITS_MASK;
        UCSR0B &= ~(1 << UCSZ02);
        switch (size)
        {
            case UART_DATA_BITS_5: { break; }
            case UART_DATA_BITS_6: { ucsrc |= (1 << UCSZ00); break; }
            case UART_DATA_BITS_7: { ucsrc |= (1 << UCSZ01); break; }
            case UART_DATA_BITS_8: { ucsrc |= (1 << UCSZ00) | (1 << UCSZ01); break; }
        }
        UCSR0C = ucsrc;
    }
}

void UART_SetParityCheck(unsigned char mode)
{
    unsigned char ucsrc = UART_ReadUCSRC();
    ucsrc &= ~UART_PARITY_MODE_MASK;
    ucsrc |= mode;
    UCSR0C = ucsrc;
}

void UART_SetStopBitsNumber(unsigned char mode)
{
    unsigned char ucsrc = UART_ReadUCSRC();
    switch(mode)
    {
        case UART_1_STOP_BIT:
            UCSR0C = ucsrc & ~(1 << USBS0);
            break;
        case UART_2_STOP_BITS:
            UCSR0C = ucsrc | (1 << USBS0);
            break;
    }
}

void UART_SendByte(unsigned char data)
{
    // Wait until transmitter buffer is empty
    while (!(UCSR0A & (1 << UDRE0))) {}
    // Put data into the buffer, sends the data
    UDR0 = data;
}

void UART_SendUlong(unsigned long data)
{
    UART_SendByte((unsigned char)data);
    UART_SendByte((unsigned char)(data >> 8));
    UART_SendByte((unsigned char)(data >> 16));
    UART_SendByte((unsigned char)(data >> 24));
}

void UART_Send9DataBits(unsigned int data)
{
    UCSR0B |= ((((1 << 8) & data) >> 8) << TXB80);
    UDR0 = (unsigned char)data;
    while(!(UCSR0A & (1 << TXC0)));
}

char UART_ReceiveByte()
{	
	// Wait for a data packet reception
	while (!(UCSR0A & (1<<RXC0))); 
	// Return data from buffer
	return UDR0;
}

unsigned long UART_ReceiveUlong()
{
    return  static_cast<unsigned long>(UART_ReceiveByte()) |
           (static_cast<unsigned long>(UART_ReceiveByte()) << 8) |
           (static_cast<unsigned long>(UART_ReceiveByte()) << 16) |
           (static_cast<unsigned long>(UART_ReceiveByte()) << 24);
}

unsigned int UART_Receive9DataBits()
{
	// Wait for a data packet reception
	while (!(UCSR0A & (1<<RXC0))); 
	// Return data from buffer
    return ((UCSR0B & (1 << RXB80)) >> RXB80) & UDR0;
}

void UART_SendStr(const char* message)
{
    int i = 0;
    char ch = message[0];
    while (ch != '\0')
    {
        UART_SendByte(ch);
        ch = message[++i];
    } 
}

int UART_GetStr(char* buffer, int size)
{
    char byte;
    int i = 0;
    do
    {
        byte = UART_ReceiveByte();
        buffer[i] = byte;
        ++i;
    } while(byte != '\n' && i < size-1);
    buffer[i] = '\0';
    return i;
}

