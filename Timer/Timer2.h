#ifndef _TIMER2_H
#define _TIMER2_H

#include <avr/io.h>

#include "IOPORT.h"

#if defined(__AVR_ATmega8535__) || defined(__AVR_ATmega16__)
    #define TIMER2_OC2_PORT D
    #define TIMER2_OC2_PIN 7
#endif

#define TIMER2_CLOCK_SOURCE_MASK              (0b111 << CS20)
#define TIMER2_CLOCK_SOURCE_NONE               0              // No clock source (Timer/counter stopped)
#define TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1    (0b001 << CS20) // clk
#define TIMER2_CLOCK_SOURCE_CLK_PRESCALE_8    (0b010 << CS20) // clk/8
#define TIMER2_CLOCK_SOURCE_CLK_PRESCALE_32   (0b011 << CS20) // clk/32
#define TIMER2_CLOCK_SOURCE_CLK_PRESCALE_64   (0b100 << CS20) // clk/64
#define TIMER2_CLOCK_SOURCE_CLK_PRESCALE_128  (0b101 << CS20) // clk/128
#define TIMER2_CLOCK_SOURCE_CLK_PRESCALE_256  (0b110 << CS20) // clk/256
#define TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1024 (0b111 << CS20) // clk/1024
#define TIMER2_CLOCK_SOURCE_TOSC1             (1 << AS2)      // External clock source from TOSC1 pin

#define TIMER2_WAVEFORM_GENERATION_MODE_MASK     ( (1 << WGM21) | (1 << WGM20) )
#define TIMER2_WAVEFORM_GENERATION_MODE_NORMAL   ( (0 << WGM21) | (0 << WGM20) ) // Normal mode
#define TIMER2_WAVEFORM_GENERATION_MODE_PHC_PWM  ( (0 << WGM21) | (1 << WGM20) ) // PWM, Phase Correct
#define TIMER2_WAVEFORM_GENERATION_MODE_CTC      ( (1 << WGM21) | (0 << WGM20) ) // CTC
#define TIMER2_WAVEFORM_GENERATION_MODE_FAST_PWM ( (1 << WGM21) | (1 << WGM20) ) // Fast PWM

#define TIMER2_COMPARE_OUTPUT_MODE_MASK                   (0b11 << COM20)
#define TIMER2_COMPARE_OUTPUT_MODE_NORMAL                 (0b00 << COM20) // Non-PWM mode: Normal port operation, OC2 disconnected
#define TIMER2_COMPARE_OUTPUT_MODE_TOGGLE_OC2             (0b01 << COM20) // Non-PWM mode: Toggle OC2 on Compare Match
#define TIMER2_COMPARE_OUTPUT_MODE_CLEAR_OC2              (0b10 << COM20) // Non-PWM mode: Clear OC2 on Compare Match
#define TIMER2_COMPARE_OUTPUT_MODE_SET_OC2                (0b11 << COM20) // Non-PWM mode: Set OC2 on Compare Match
#define TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_NORMAL        (0b00 << COM20) // Fast PWM mode: Normal port operation, OC2 disconnected
#define TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING (0b10 << COM20) // Fast PWM mode: Clear OC2 on Compare Match, set OC2 at TOP (Non-Inverting)
#define TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_INVERTING     (0b11 << COM20) // Fast PWM mode: Set OC2 on Compare Match, clear OC2 at TOP (Inverting)
#define TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_NORMAL         (0b00 << COM20) // Phase Correct PWM mode: Normal port operation, OC2 disconnected
#define TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING  (0b10 << COM20) // Phase Correct PWM mode: Clear OC2 on Compare Match when up-counting. Set OC2 on Compare Match when down-counting.
#define TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_INVERTING      (0b11 << COM20) // Phase Correct PWM mode: Set OC2 on Compare Match when up-counting. Clear OC2 on Compare Match when down-counting.

#define TIMER2_INTERRUPT_MASK             (0b11 << TOIE2)
#define TIMER2_INTERRUPT_DISABLE           0           // Disable interrupts
#define TIMER2_INTERRUPT_ON_OVERFLOW      (1 << TOIE2) // Enable overflow interrupt
#define TIMER2_INTERRUPT_ON_COMPARE_MATCH (1 << OCIE2) // Enable output compare match interrupt

/*
Sets clock source and prescaler for Timer2.
If either [internal_clock_source] or [external_clock_source] is nonzero Timer2 starts counting.
[internal_clock_source] parameter can be one of the following:
    TIMER2_CLOCK_SOURCE_NONE - No clock source (Timer/counter stopped).
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1    - clk
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_8    - clk/8
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_32   - clk/32
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_64   - clk/64
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_128  - clk/128
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_256  - clk/256
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1024 - clk/1024
    TIMER2_CLOCK_SOURCE_TOSC1             - External clock source from TOSC1 pin
*/
inline void Timer2_SetClockSource(unsigned char clock_source)
{
    unsigned char temp = TCCR2 & ~TIMER2_CLOCK_SOURCE_MASK;
    temp |= ( clock_source & TIMER2_CLOCK_SOURCE_MASK );
    TCCR2 = temp;
    
    temp = ASSR & ~TIMER2_CLOCK_SOURCE_TOSC1;
    temp |= ( clock_source & TIMER2_CLOCK_SOURCE_TOSC1 );
    ASSR = temp;
}

/*
Sets waveform generation mode for Timer2.
[mode] parameter can be one of the following:
    TIMER2_WAVEFORM_GENERATION_MODE_NORMAL   - Normal
    TIMER2_WAVEFORM_GENERATION_MODE_PHC_PWM  - PWM, Phase Correct
    TIMER2_WAVEFORM_GENERATION_MODE_CTC      - CTC
    TIMER2_WAVEFORM_GENERATION_MODE_FAST_PWM - Fast PWM
*/
inline void Timer2_SetWaveformGenerationMode(unsigned char mode)
{
    unsigned char temp = TCCR2;
    temp &= ~TIMER2_WAVEFORM_GENERATION_MODE_MASK;
    temp |= ( mode & TIMER2_WAVEFORM_GENERATION_MODE_MASK );
    TCCR2 = temp;
}

/*
Sets compare output mode for Timer2.
[mode] parameter can be one of the following:
    TIMER2_COMPARE_OUTPUT_MODE_NORMAL     - Non-PWM mode: Normal port operation, OC2 disconnected
    TIMER2_COMPARE_OUTPUT_MODE_TOGGLE_OC2 - Non-PWM mode: Toggle OC2 on Compare Match
    TIMER2_COMPARE_OUTPUT_MODE_CLEAR_OC2  - Non-PWM mode: Clear OC2 on Compare Match
    TIMER2_COMPARE_OUTPUT_MODE_SET_OC2    - Non-PWM mode: Set OC2 on Compare Match
    TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_NORMAL        - Fast PWM mode: Normal port operation, OC2 disconnected
    TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING - Fast PWM mode: Clear OC2 on Compare Match, set OC2 at TOP (Non-Inverting)
    TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_INVERTING     - Fast PWM mode: Set OC2 on Compare Match, clear OC2 at TOP (Inverting)
    TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_NORMAL        - Phase Correct PWM mode: Normal port operation, OC2 disconnected
    TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING - Phase Correct PWM mode: Clear OC2 on Compare Match when up-counting. Set OC2 on Compare Match when down-counting.
    TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_INVERTING     - Phase Correct PWM mode: Set OC2 on Compare Match when up-counting. Clear OC2 on Compare Match when down-counting.
*/
inline void Timer2_SetCompareOutputMode(unsigned char mode)
{
    unsigned char temp = TCCR2;
    temp &= ~TIMER2_COMPARE_OUTPUT_MODE_MASK;
    temp |= ( mode & TIMER2_COMPARE_OUTPUT_MODE_MASK );
    TCCR2 = temp;
}

// Gets Timer2 counter register value.
inline unsigned char Timer2_GetTimerCounter() { return TCNT2; }

// Sets Timer2 counter register value.
inline void Timer2_SetTimerCounter(unsigned char value) { TCNT2 = value; }

// Resets the prescaler module of Timer2
// for synchronizing the Timer/Counter to program execution.
inline void Timer2_Timer1_ResetPrescaler() { SFIOR |= (1 << PSR2); }

/*
Enables or disables interrupts for Timer2.
[mode] parameter is a bitwise OR combination of the following flags:
    TIMER2_INTERRUPT_DISABLE          - Disable interrupts
    TIMER2_INTERRUPT_ON_OVERFLOW      - Enable overflow interrupt
    TIMER2_INTERRUPT_ON_COMPARE_MATCH - Enable output compare match interrupt
*/
inline void Timer2_SetupInterrupts(unsigned char mode)
{
    unsigned char temp = TIMSK & ~TIMER2_INTERRUPT_MASK;
    temp |= ( mode & TIMER2_INTERRUPT_MASK );
    TIMSK = temp;
}

// Configures Timer2's Output Complare Match Output pin as output.
inline void Timer2_EnableOC2()
{
    IOPORT_CONFIG_PIN_OUT(TIMER2_OC2_PORT, TIMER2_OC2_PIN);
}

// Configures Timer2's Output Complare Match Output pin as input.
inline void Timer2_DisableOC2()
{
    IOPORT_CONFIG_PIN_IN(TIMER2_OC2_PORT, TIMER2_OC2_PIN);
}

// Stops Timer2 and clears its counter register.
inline void Timer2_Stop()
{
    Timer2_SetClockSource(TIMER2_CLOCK_SOURCE_NONE);
    TCNT2 = 0; // Reset timer counter register value.
}

inline void Timer2_Init()
{
    TCCR2 = 0; // Stop Timer2.
    TCNT2 = 0; // Reset timer counter register value.
    Timer2_SetupInterrupts(TIMER2_INTERRUPT_DISABLE); // Disable all Timer2 interrupts.
}

/*
Sets up CTC mode (Clear Timer on Comapare Match) for Timer2 and sets timer frequency in Hz.
[freq] - timer compare match interrupt frequency in Hz
Limitations: [freq] >= clk / 2^18 Hz (4 Hz  for clk = 1 MHz).
             [freq] <= clk           (1 MHz for clk = 1 MHz).
*/
inline void Timer2_SetupCtcMode(unsigned long freq)
{
    // freq = F_CPU / (PRESCALER * (1 + OCR2))
    // OCR2 = F_CLK / (PRESCALER * freq) - 1

    Timer2_Stop();
    Timer2_SetWaveformGenerationMode(TIMER2_WAVEFORM_GENERATION_MODE_CTC);
    
    if((F_CPU >> 8) < freq)
    {
        OCR2 = F_CPU / freq - 1;
        Timer2_SetClockSource(TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1);
    }
    else if((F_CPU >> 11) < freq)
    {
        OCR2 = (F_CPU >> 3) / freq - 1;
        Timer2_SetClockSource(TIMER2_CLOCK_SOURCE_CLK_PRESCALE_8);
    }
    else if((F_CPU >> 13) < freq)
    {
        OCR2 = (F_CPU >> 5) / freq - 1;
        Timer2_SetClockSource(TIMER2_CLOCK_SOURCE_CLK_PRESCALE_32);
    }
    else if((F_CPU >> 14) < freq)
    {
        OCR2 = (F_CPU >> 6) / freq - 1;
        Timer2_SetClockSource(TIMER2_CLOCK_SOURCE_CLK_PRESCALE_64);
    }
    else if((F_CPU >> 15) < freq)
    {
        OCR2 = (F_CPU >> 7) / freq - 1;
        Timer2_SetClockSource(TIMER2_CLOCK_SOURCE_CLK_PRESCALE_128);
    }
    else if((F_CPU >> 16) < freq)
    {
        OCR2 = (F_CPU >> 8) / freq - 1;
        Timer2_SetClockSource(TIMER2_CLOCK_SOURCE_CLK_PRESCALE_256);
    }
    else if((F_CPU >> 18) < freq)
    {
        OCR2 = (F_CPU >> 10) / freq - 1;
        Timer2_SetClockSource(TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1024);
    }
}

/*
Sets up meander on OC2 output using CTC mode.
[freq] - meander frequency in Hz.
*/
inline void Timer2_SetupMeander(unsigned long freq)
{
    Timer2_SetCompareOutputMode(TIMER2_COMPARE_OUTPUT_MODE_TOGGLE_OC2);
    Timer2_SetupCtcMode(freq << 1);
    Timer2_EnableOC2();
}

/*
Sets up Fast Pulse Width Modulation mode for Timer2.
PWM frequency equals clk/(PRESCALER * 256).
Parameters:
[prescaler] can be one of the following:
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_8
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_32
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_64
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_128
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_256
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1024
[duty_cycle] - actual duty cycle of PWM signal is calculated as duty_cycle/255; should range from 0 to 255.
*/
inline void Timer2_SetupFastPwmMode(
    unsigned char prescaler,
    unsigned char compare_output_mode,
    unsigned char duty_cycle)
{
    Timer2_Stop();
    OCR2 = duty_cycle;
    Timer2_SetWaveformGenerationMode(TIMER2_WAVEFORM_GENERATION_MODE_FAST_PWM);
    Timer2_SetCompareOutputMode(TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING);
    Timer2_SetClockSource(prescaler);
    Timer2_EnableOC2();
}

/*
Sets up Phase Correct Pulse Width Modulation mode for Timer2.
PWM frequency equals clk/(PRESCALER * 510).
[prescaler] can be one of the following:
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_8
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_32
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_64
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_128
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_256
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1024
[duty_cycle] - Should range from 0 to 255. Actual duty cycle of PWM signal is calculated as duty_cycle/255.
*/
inline void Timer0_SetupPhaseCorrectPwmMode(
    unsigned char prescaler,
    unsigned char duty_cycle)
{
    Timer2_Stop();
    OCR2 = duty_cycle;
    Timer2_SetWaveformGenerationMode(TIMER2_WAVEFORM_GENERATION_MODE_PHC_PWM);
    Timer2_SetCompareOutputMode(TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING);
    Timer2_SetClockSource(prescaler);
    Timer2_EnableOC2();
}

#endif
