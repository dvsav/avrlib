#if !defined(_TIMER2_H) && defined (__AVR_ATmega328__)
#define _TIMER2_H

#include <avr/io.h>
#include <avr/interrupt.h>

#include "IOPORT.h"

    #define TIMER2_OC2A_PORT B
    #define TIMER2_OC2A_PIN 3
    #define TIMER2_OC2B_PORT D
    #define TIMER2_OC2B_PIN 3

#define TIMER2_CLOCK_SOURCE_MASK              (0b111 << CS20)
#define TIMER2_CLOCK_SOURCE_NONE               0              // No clock source (Timer/counter stopped)
#define TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1    (0b001 << CS20) // clk
#define TIMER2_CLOCK_SOURCE_CLK_PRESCALE_8    (0b010 << CS20) // clk/8
#define TIMER2_CLOCK_SOURCE_CLK_PRESCALE_32   (0b011 << CS20) // clk/32
#define TIMER2_CLOCK_SOURCE_CLK_PRESCALE_64   (0b100 << CS20) // clk/64
#define TIMER2_CLOCK_SOURCE_CLK_PRESCALE_128  (0b101 << CS20) // clk/128
#define TIMER2_CLOCK_SOURCE_CLK_PRESCALE_256  (0b110 << CS20) // clk/256
#define TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1024 (0b111 << CS20) // clk/1024
#define TIMER2_CLOCK_SOURCE_TOSC1             (1 << AS2)      // External clock source from TOSC1 pin

#define TIMER2_WAVEFORM_GENERATION_MODE_MASK     (0b11 << WGM20)
#define TIMER2_WAVEFORM_GENERATION_MODE_NORMAL   (0b00 << WGM20) // Normal mode
#define TIMER2_WAVEFORM_GENERATION_MODE_PHC_PWM  (0b01 << WGM20) // PWM, Phase Correct
#define TIMER2_WAVEFORM_GENERATION_MODE_CTC      (0b10 << WGM20) // CTC
#define TIMER2_WAVEFORM_GENERATION_MODE_FAST_PWM (0b11 << WGM20) // Fast PWM

#define TIMER2_COMPARE_OUTPUT_MODE_MASK_A                   (0b11 << COM2A0)
#define TIMER2_COMPARE_OUTPUT_MODE_NORMAL_A                 (0b00 << COM2A0) // Non-PWM mode: Normal port operation, OC2A disconnected
#define TIMER2_COMPARE_OUTPUT_MODE_TOGGLE_OC2_A             (0b01 << COM2A0) // Non-PWM mode: Toggle OC2A on Compare Match
#define TIMER2_COMPARE_OUTPUT_MODE_CLEAR_OC2_A              (0b10 << COM2A0) // Non-PWM mode: Clear OC2A on Compare Match
#define TIMER2_COMPARE_OUTPUT_MODE_SET_OC2_A                (0b11 << COM2A0) // Non-PWM mode: Set OC2A on Compare Match
#define TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_NORMAL_A        (0b00 << COM2A0) // Fast PWM mode: Normal port operation, OC2A disconnected
#define TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_A (0b10 << COM2A0) // Fast PWM mode: Clear OC2A on Compare Match, set OC2A at BOTTOM (Non-Inverting)
#define TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_INVERTING_A     (0b11 << COM2A0) // Fast PWM mode: Set OC2A on Compare Match, clear OC2A at BOTTOM (Inverting)
#define TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_NORMAL_A         (0b00 << COM2A0) // Phase Correct and Phase and Frequency Correct PWM mode: Normal port operation, OC2A disconnected
#define TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_A  (0b10 << COM2A0) // Phase Correct and Phase and Frequency Correct PWM mode: Clear OC2A on Compare Match when up-counting. Set OC2A on Compare Match when down-counting.
#define TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_INVERTING_A      (0b11 << COM2A0) // Phase Correct and Phase and Frequency Correct PWM mode: Set OC2A on Compare Match when up-counting. Clear OC2A on Compare Match when down-counting.

#define TIMER2_COMPARE_OUTPUT_MODE_MASK_B                   (0b11 << COM2B0)
#define TIMER2_COMPARE_OUTPUT_MODE_NORMAL_B                 (0b00 << COM2B0) // Non-PWM mode: Normal port operation, OC2B disconnected
#define TIMER2_COMPARE_OUTPUT_MODE_TOGGLE_OC2_B             (0b01 << COM2B0) // Non-PWM mode: Toggle OC2B on Compare Match
#define TIMER2_COMPARE_OUTPUT_MODE_CLEAR_OC2_B              (0b10 << COM2B0) // Non-PWM mode: Clear OC2B on Compare Match
#define TIMER2_COMPARE_OUTPUT_MODE_SET_OC2_B                (0b11 << COM2B0) // Non-PWM mode: Set OC2B on Compare Match
#define TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_NORMAL_B        (0b00 << COM2B0) // Fast PWM mode: Normal port operation, OC2B disconnected
#define TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_B (0b10 << COM2B0) // Fast PWM mode: Clear OC2B on Compare Match, set OC2B at BOTTOM (Non-Inverting)
#define TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_INVERTING_B     (0b11 << COM2B0) // Fast PWM mode: Set OC2B on Compare Match, clear OC2B at BOTTOM (Inverting)
#define TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_NORMAL_B         (0b00 << COM2B0) // Phase Correct and Phase and Frequency Correct PWM mode: Normal port operation, OC2B disconnected
#define TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_B  (0b10 << COM2B0) // Phase Correct and Phase and Frequency Correct PWM mode: Clear OC2B on Compare Match when up-counting. Set OC2B on Compare Match when down-counting.
#define TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_INVERTING_B      (0b11 << COM2B0) // Phase Correct and Phase and Frequency Correct PWM mode: Set OC2B on Compare Match when up-counting. Clear OC2B on Compare Match when down-counting.

#define TIMER2_INTERRUPT_MASK               (0b111 << TOIE2)
#define TIMER2_INTERRUPT_DISABLE             0            // Disable interrupts
#define TIMER2_INTERRUPT_ON_OVERFLOW        (1 << TOIE2)  // Enable overflow interrupt
#define TIMER2_INTERRUPT_ON_COMPARE_MATCH_A (1 << OCIE2A) // Enable output compare match A interrupt
#define TIMER2_INTERRUPT_ON_COMPARE_MATCH_B (1 << OCIE2B) // Enable output compare match B interrupt

// Enables Timer2 module (in Power Reduction Register).
inline void Timer2_Enable() { PRR &= ~(1 << PRTIM2); }

// Disables Timer2 module (in Power Reduction Register).
inline void Timer2_Disable() { PRR |= (1 << PRTIM2); }

/*
Sets clock source and prescaler for Timer2.
If either [internal_clock_source] or [external_clock_source] is nonzero Timer2 starts counting.
[internal_clock_source] parameter can be one of the following:
    TIMER2_CLOCK_SOURCE_NONE - No clock source (Timer/counter stopped).
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1    - clk
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_8    - clk/8
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_32   - clk/32
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_64   - clk/64
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_128  - clk/128
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_256  - clk/256
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1024 - clk/1024
    TIMER2_CLOCK_SOURCE_TOSC1             - External clock source from TOSC1 pin
*/
inline void Timer2_SetClockSource(unsigned char clock_source)
{
    unsigned char temp = TCCR2B & ~TIMER2_CLOCK_SOURCE_MASK;
    temp |= ( clock_source & TIMER2_CLOCK_SOURCE_MASK );
    TCCR2B = temp;
    
    temp = ASSR & ~TIMER2_CLOCK_SOURCE_TOSC1;
    temp |= ( clock_source & TIMER2_CLOCK_SOURCE_TOSC1 );
    ASSR = temp;
}

/*
Sets waveform generation mode for Timer2.
[mode] parameter can be one of the following:
    TIMER2_WAVEFORM_GENERATION_MODE_NORMAL   - Normal
    TIMER2_WAVEFORM_GENERATION_MODE_PHC_PWM  - PWM, Phase Correct
    TIMER2_WAVEFORM_GENERATION_MODE_CTC      - CTC
    TIMER2_WAVEFORM_GENERATION_MODE_FAST_PWM - Fast PWM
*/
inline void Timer2_SetWaveformGenerationMode(unsigned char mode)
{
    unsigned char temp = TCCR2A;
    temp &= ~TIMER2_WAVEFORM_GENERATION_MODE_MASK;
    temp |= ( mode & TIMER2_WAVEFORM_GENERATION_MODE_MASK );
    TCCR2A = temp;
    
    TCCR2B &= ~(1 << WGM22);
}

/*
Sets compare output mode for Timer2's OC2A output.
[mode] parameter can be one of the following:
    TIMER2_COMPARE_OUTPUT_MODE_NORMAL_A     - Non-PWM mode: Normal port operation, OC2A disconnected
    TIMER2_COMPARE_OUTPUT_MODE_TOGGLE_OC2_A - Non-PWM mode: Toggle OC2A on Compare Match
    TIMER2_COMPARE_OUTPUT_MODE_CLEAR_OC2_A  - Non-PWM mode: Clear OC2A on Compare Match
    TIMER2_COMPARE_OUTPUT_MODE_SET_OC2_A    - Non-PWM mode: Set OC2A on Compare Match
    TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_NORMAL_A        - Fast PWM mode: Normal port operation, OC2A disconnected
    TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_A - Fast PWM mode: Clear OC2A on Compare Match, set OC2A at BOTTOM (Non-Inverting)
    TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_INVERTING_A     - Fast PWM mode: Set OC2A on Compare Match, clear OC2A at BOTTOM (Inverting)
    TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_NORMAL_A        - Phase Correct and Phase and Frequency Correct PWM mode: Normal port operation, OC2A disconnected
    TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_A - Phase Correct and Phase and Frequency Correct PWM mode: Clear OC2A on Compare Match when up-counting. Set OC2A on Compare Match when down-counting.
    TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_INVERTING_A     - Phase Correct and Phase and Frequency Correct PWM mode: Set OC2A on Compare Match when up-counting. Clear OC2A on Compare Match when down-counting.
*/
inline void Timer2_SetCompareOutputMode_A(unsigned char mode)
{
    unsigned char temp = TCCR2A & ~TIMER2_COMPARE_OUTPUT_MODE_MASK_A;
    temp |= ( mode & TIMER2_COMPARE_OUTPUT_MODE_MASK_A );
    TCCR2A = temp;
}

/*
Sets compare output mode for Timer2's OC2B output.
[mode] parameter can be one of the following:
    TIMER2_COMPARE_OUTPUT_MODE_NORMAL_B     - Non-PWM mode: Normal port operation, OC2B disconnected
    TIMER2_COMPARE_OUTPUT_MODE_TOGGLE_OC2_B - Non-PWM mode: Toggle OC2B on Compare Match
    TIMER2_COMPARE_OUTPUT_MODE_CLEAR_OC2_B  - Non-PWM mode: Clear OC2B on Compare Match
    TIMER2_COMPARE_OUTPUT_MODE_SET_OC2_B    - Non-PWM mode: Set OC2B on Compare Match
    TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_NORMAL_B        - Fast PWM mode: Normal port operation, OC2B disconnected
    TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_B - Fast PWM mode: Clear OC2B on Compare Match, set OC2B at BOTTOM (Non-Inverting)
    TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_INVERTING_B     - Fast PWM mode: Set OC2B on Compare Match, clear OC2B at BOTTOM (Inverting)
    TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_NORMAL_B        - Phase Correct and Phase and Frequency Correct PWM mode: Normal port operation, OC2B disconnected
    TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_B - Phase Correct and Phase and Frequency Correct PWM mode: Clear OC2B on Compare Match when up-counting. Set OC2B on Compare Match when down-counting.
    TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_INVERTING_B     - Phase Correct and Phase and Frequency Correct PWM mode: Set OC2B on Compare Match when up-counting. Clear OC2B on Compare Match when down-counting.
*/
inline void Timer2_SetCompareOutputMode_B(unsigned char mode)
{
    unsigned char temp = TCCR2A & ~TIMER2_COMPARE_OUTPUT_MODE_MASK_B;
    temp |= ( mode & TIMER2_COMPARE_OUTPUT_MODE_MASK_B );
    TCCR2A = temp;
}

/*
Enables or disables interrupts for Timer2.
[mode] parameter is a bitwise OR combination of the following flags:
    TIMER2_INTERRUPT_DISABLE            - Disable interrupts
    TIMER2_INTERRUPT_ON_OVERFLOW        - Enable overflow interrupt
    TIMER2_INTERRUPT_ON_COMPARE_MATCH_A - Enable output compare match A interrupt
    TIMER2_INTERRUPT_ON_COMPARE_MATCH_B - Enable output compare match B interrupt
*/
inline void Timer2_SetupInterrupts(unsigned char mode)
{
    unsigned char temp = TIMSK2 & ~TIMER2_INTERRUPT_MASK;
    temp |= ( mode & TIMER2_INTERRUPT_MASK );
    TIMSK2 = temp;
}

// Resets the prescaler module of Timer2
// for synchronizing the Timer/Counter to program execution.
inline void Timer2_ResetPrescaler() { GTCCR |= (1 << PSRASY); }

// Gets Timer2 counter register value.
inline unsigned char Timer2_GetTimerCounter() { return TCNT2; }

// Sets Timer2 counter register value.
inline void Timer2_SetTimerCounter(unsigned char value) { TCNT2 = value; }

// Configures Timer2's Output Complare Match Output pin OC2A as output.
inline void Timer2_EnableOC2A()
{
    IOPORT_CONFIG_PIN_OUT(TIMER2_OC2A_PORT, TIMER2_OC2A_PIN);
}

// Configures Timer2's Output Complare Match Output pin OC2A as input.
inline void Timer2_DisableOC2A()
{
    IOPORT_CONFIG_PIN_IN(TIMER2_OC2A_PORT, TIMER2_OC2A_PIN);
}

// Configures Timer2's Output Complare Match Output pin OC2B as output.
inline void Timer2_EnableOC2B()
{
    IOPORT_CONFIG_PIN_OUT(TIMER2_OC2B_PORT, TIMER2_OC2B_PIN);
}

// Configures Timer2's Output Complare Match Output pin OC2B as input.
inline void Timer2_DisableOC2B()
{
    IOPORT_CONFIG_PIN_IN(TIMER2_OC2B_PORT, TIMER2_OC2B_PIN);
}

// Stops Timer2 and clears its counter register.
inline void Timer2_Stop()
{
    Timer2_SetClockSource(TIMER2_CLOCK_SOURCE_NONE);
    TCNT2 = 0; // Reset timer counter register value.
}

inline void Timer2_Init()
{
    // Stop Timer2, set Normal waveform generation mode, set Normal compare output mode.
    TCCR2A = 0;
    TCCR2B = 0;
    TCNT2 = 0; // Reset timer counter register value.
    TIMSK2 = 0; // Disable all Timer2 interrupts.
    Timer2_Enable();
}

/*
Sets up CTC mode (Clear Timer on Comapare Match) for Timer2 and sets timer frequency in Hz.
[freq] - timer compare match interrupt frequency in Hz
Limitations: [freq] >= clk / 2^18 Hz (4 Hz  for clk = 1 MHz).
             [freq] <= clk           (1 MHz for clk = 1 MHz).
*/
#define TIMER2_MIN_CTC_FREQUENCY (F_CPU >> 18)
inline void Timer2_SetupCtcMode(unsigned long freq)
{
    // freq = clk / (PRESCALER * (1 + OCR2A))
    // OCR2A = clk / (PRESCALER * freq) - 1
    
    Timer2_Stop();
    Timer2_SetWaveformGenerationMode(TIMER2_WAVEFORM_GENERATION_MODE_CTC);
    
    if(F_CPU < freq)
    {
        OCR2A = 0;
        Timer2_SetClockSource(TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1);
    }
    else if((F_CPU >> 8) < freq)
    {
        OCR2A = F_CPU / freq - 1;
        Timer2_SetClockSource(TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1);
    }
    else if((F_CPU >> 11) < freq)
    {
        OCR2A = (F_CPU >> 3) / freq - 1;
        Timer2_SetClockSource(TIMER2_CLOCK_SOURCE_CLK_PRESCALE_8);
    }
    else if((F_CPU >> 13) < freq)
    {
        OCR2A = (F_CPU >> 5) / freq - 1;
        Timer2_SetClockSource(TIMER2_CLOCK_SOURCE_CLK_PRESCALE_32);
    }
    else if((F_CPU >> 14) < freq)
    {
        OCR2A = (F_CPU >> 6) / freq - 1;
        Timer2_SetClockSource(TIMER2_CLOCK_SOURCE_CLK_PRESCALE_64);
    }
    else if((F_CPU >> 15) < freq)
    {
        OCR2A = (F_CPU >> 7) / freq - 1;
        Timer2_SetClockSource(TIMER2_CLOCK_SOURCE_CLK_PRESCALE_128);
    }
    else if((F_CPU >> 16) < freq)
    {
        OCR2A = (F_CPU >> 8) / freq - 1;
        Timer2_SetClockSource(TIMER2_CLOCK_SOURCE_CLK_PRESCALE_256);
    }
    else if((F_CPU >> 18) < freq)
    {
        OCR2A = (F_CPU >> 10) / freq - 1;
        Timer2_SetClockSource(TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1024);
    }
    else
    {
        OCR2A = 0xFF;
        Timer2_SetClockSource(TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1024);
    }
}

/*
Sets up meander on OC2A output using CTC mode.
[freq] - meander frequency in Hz.
*/
#define TIMER2_MIN_MEANDER_FREQUENCY (F_CPU >> 19)
inline void Timer2_SetupMeander_A(unsigned long freq)
{
    Timer2_DisableOC2B();
    Timer2_SetCompareOutputMode_A(TIMER2_COMPARE_OUTPUT_MODE_TOGGLE_OC2_A);
    Timer2_SetupCtcMode(freq << 1);
    Timer2_EnableOC2A();
}

/*
Sets up meander on OC2B output using CTC mode.
[freq] - meander frequency in Hz.
*/
inline void Timer2_SetupMeander_B(unsigned long freq)
{
    Timer2_DisableOC2A();
    OCR2B = 0;
    Timer2_SetCompareOutputMode_B(TIMER2_COMPARE_OUTPUT_MODE_TOGGLE_OC2_B);
    Timer2_SetupCtcMode(freq << 1);
    Timer2_EnableOC2B();
}

/*
Sets up meanders on OC2A and OC2B outputs using CTC mode.
[freq] - meander frequency in Hz.
[phase_shift] - phase shift between OC2B and OC2A meanders as a fraction of meander period (0...1).
*/
inline void Timer2_SetupMeander_AB(
    unsigned long freq,
    float phase_shift)
{
    Timer2_SetCompareOutputMode_A(TIMER2_COMPARE_OUTPUT_MODE_TOGGLE_OC2_A);
    Timer2_SetCompareOutputMode_B(TIMER2_COMPARE_OUTPUT_MODE_TOGGLE_OC2_B);
    Timer2_SetupCtcMode(freq << 1);
    OCR2B = static_cast<unsigned char>(phase_shift * OCR2A);
    Timer2_EnableOC2A();
    Timer2_EnableOC2B();
}

/*
Sets up Fast Pulse Width Modulation mode for Timer2 on OC2A output.
PWM frequency equals clk/(PRESCALER * 256).
[prescaler] can be one of the following:
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1    - clk
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_8    - clk/8
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_32   - clk/32
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_64   - clk/64
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_128  - clk/128
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_256  - clk/256
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1024 - clk/1024
[duty_cycle] - Should range from 0 to 255. Actual duty cycle of PWM signal is calculated as duty cycle/255.
*/
inline void Timer2_SetupFastPwmMode_A(
    unsigned char prescaler,
    unsigned char duty_cycle)
{
    Timer2_Stop();
    Timer2_DisableOC2B();
    OCR2A = duty_cycle;
    Timer2_SetWaveformGenerationMode(TIMER2_WAVEFORM_GENERATION_MODE_FAST_PWM);
    Timer2_SetCompareOutputMode_A(TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_A);
    Timer2_SetClockSource(prescaler);
    Timer2_EnableOC2A();
}

/*
Sets up Fast Pulse Width Modulation mode for Timer2 on OC2B output.
PWM frequency equals clk/(PRESCALER * 256).
[prescaler] can be one of the following:
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1    - clk
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_8    - clk/8
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_32   - clk/32
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_64   - clk/64
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_128  - clk/128
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_256  - clk/256
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1024 - clk/1024
[duty_cycle] - Should range from 0 to 255. Actual duty cycle of PWM signal is calculated as duty cycle/255.
*/
inline void Timer2_SetupFastPwmMode_B(
    unsigned char prescaler,
    unsigned char duty_cycle)
{
    Timer2_Stop();
    Timer2_DisableOC2A();
    OCR2B = duty_cycle;
    Timer2_SetWaveformGenerationMode(TIMER2_WAVEFORM_GENERATION_MODE_FAST_PWM);
    Timer2_SetCompareOutputMode_B(TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_B);
    Timer2_SetClockSource(prescaler);
    Timer2_EnableOC2B();
}

/*
Sets up Fast Pulse Width Modulation mode for Timer2 on OC2A and OC2B outputs.
PWM frequency equals clk/(PRESCALER * 256).
[prescaler] can be one of the following:
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1    - clk
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_8    - clk/8
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_32   - clk/32
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_64   - clk/64
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_128  - clk/128
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_256  - clk/256
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1024 - clk/1024
[duty_cycle_A], [duty_cycle_B] - Should range from 0 to 255. Actual duty cycle of PWM signal is calculated as duty cycle/255.
*/
inline void Timer2_SetupFastPwmMode_AB(
    unsigned char prescaler,
    unsigned char duty_cycle_A,
    unsigned char duty_cycle_B)
{
    Timer2_Stop();
    OCR2A = duty_cycle_A;
    OCR2B = duty_cycle_B;
    Timer2_SetWaveformGenerationMode(TIMER2_WAVEFORM_GENERATION_MODE_FAST_PWM);
    Timer2_SetCompareOutputMode_A(TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_A);
    Timer2_SetCompareOutputMode_B(TIMER2_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_B);
    Timer2_SetClockSource(prescaler);
    Timer2_EnableOC2A();
    Timer2_EnableOC2B();
}

/*
Sets up Phase Correct Pulse Width Modulation mode for Timer2 on OC2A output.
PWM frequency equals clk/(PRESCALER * 510).
[prescaler] can be one of the following:
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1    - clk
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_8    - clk/8
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_32   - clk/32
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_64   - clk/64
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_128  - clk/128
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_256  - clk/256
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1024 - clk/1024
[duty_cycle] - Should range from 0 to 255. Actual duty cycle of PWM signal is calculated as duty cycle/255.
*/
inline void Timer2_SetupPhaseCorrectPwmMode_A(
    unsigned char prescaler,
    unsigned char duty_cycle)
{
    Timer2_Stop();
    Timer2_DisableOC2B();
    OCR2A = duty_cycle;
    Timer2_SetWaveformGenerationMode(TIMER2_WAVEFORM_GENERATION_MODE_PHC_PWM);
    Timer2_SetCompareOutputMode_A(TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_A);
    Timer2_SetClockSource(prescaler);
    Timer2_EnableOC2A();
}

/*
Sets up Phase Correct Pulse Width Modulation mode for Timer2 on OC2B output.
PWM frequency equals clk/(PRESCALER * 510).
[prescaler] can be one of the following:
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1    - clk
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_8    - clk/8
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_32   - clk/32
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_64   - clk/64
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_128  - clk/128
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_256  - clk/256
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1024 - clk/1024
[duty_cycle] - Should range from 0 to 255. Actual duty cycle of PWM signal is calculated as duty cycle/255.
*/
inline void Timer2_SetupPhaseCorrectPwmMode_B(
    unsigned char prescaler,
    unsigned char duty_cycle)
{
    Timer2_Stop();
    Timer2_DisableOC2A();
    OCR2B = duty_cycle;
    Timer2_SetWaveformGenerationMode(TIMER2_WAVEFORM_GENERATION_MODE_PHC_PWM);
    Timer2_SetCompareOutputMode_B(TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_B);
    Timer2_SetClockSource(prescaler);
    Timer2_EnableOC2B();
}

/*
Sets up Phase Correct Pulse Width Modulation mode for Timer2 on OC2A and OC2B outputs.
PWM frequency equals clk/(PRESCALER * 510).
[prescaler] can be one of the following:
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1    - clk
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_8    - clk/8
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_32   - clk/32
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_64   - clk/64
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_128  - clk/128
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_256  - clk/256
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1024 - clk/1024
[duty_cycle_A], [duty_cycle_B] - Should range from 0 to 255. Actual duty cycle of PWM signal is calculated as duty cycle/255.
*/
inline void Timer2_SetupPhaseCorrectPwmMode_AB(
    unsigned char prescaler,
    unsigned char duty_cycle_A,
    unsigned char duty_cycle_B)
{
    Timer2_Stop();
    OCR2A = duty_cycle_A;
    OCR2B = duty_cycle_B;
    Timer2_SetWaveformGenerationMode(TIMER2_WAVEFORM_GENERATION_MODE_PHC_PWM);
    Timer2_SetCompareOutputMode_A(TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_A);
    Timer2_SetCompareOutputMode_B(TIMER2_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_B);
    Timer2_SetClockSource(prescaler);
    Timer2_EnableOC2A();
    Timer2_EnableOC2B();
}

/*
Enables interrupt on Timer2 overflow event (all other functions of Timer2 will be disabled) and
starts counting according to specified [clock_source]. Interrupt frequency will be equal to
timer clock source frequency divided by 256.
[internal_clock_source] parameter can be one of the following:
    TIMER2_CLOCK_SOURCE_NONE - No clock source (Timer/counter stopped).
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1    - clk
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_8    - clk/8
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_32   - clk/32
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_64   - clk/64
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_128  - clk/128
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_256  - clk/256
    TIMER2_CLOCK_SOURCE_CLK_PRESCALE_1024 - clk/1024
    TIMER2_CLOCK_SOURCE_TOSC1             - External clock source from TOSC1 pin
Remarks: use #include <avr/interrupt.h> ISR(TIMER2_OVF_vect) { ... } for interrupt handling
*/
inline void Timer2_InterruptOnOverflow(unsigned char clock_source)
{
    Timer2_Init();
    Timer2_SetupInterrupts(TIMER2_INTERRUPT_ON_OVERFLOW);
    Timer2_SetClockSource(clock_source);
    sei();
}

/*
Enables interrupt on TIMER2 compare match A event (all other functions of TIMER2 will be disabled) and
starts counting. Interrupts will be happening with specified frequency [freq].
[freq] - timer compare match interrupt frequency in Hz
Limitations: [freq] >= clk / 2^18 Hz (4 Hz  for clk = 1 MHz).
             [freq] <= clk           (1 MHz for clk = 1 MHz).
Remarks: use #include <avr/interrupt.h> ISR(TIMER2_COMPA_vect) { ... } for interrupt handling
*/
inline void Timer2_InterruptOnCompareMatchA(unsigned long freq)
{
    Timer2_Init();
    Timer2_SetupInterrupts(TIMER2_INTERRUPT_ON_COMPARE_MATCH_A);
    Timer2_SetupCtcMode(freq);
    sei();
}

#endif
