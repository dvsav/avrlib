#if !defined(_TIMER1_H) && defined (__AVR_ATmega328__)
#define _TIMER1_H

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>

#include "IOPORT.h"

    #define TIMER1_OC1A_PORT B
    #define TIMER1_OC1A_PIN 1
    #define TIMER1_OC1B_PORT B
    #define TIMER1_OC1B_PIN 2

#define TIMER1_CLOCK_SOURCE_MASK              (0b111 << CS10)
#define TIMER1_CLOCK_SOURCE_NONE              (0b000 << CS10) // No clock source (Timer/counter stopped)
#define TIMER1_CLOCK_SOURCE_CLK_PRESCALE_1    (0b001 << CS10) // clk
#define TIMER1_CLOCK_SOURCE_CLK_PRESCALE_8    (0b010 << CS10) // clk/8
#define TIMER1_CLOCK_SOURCE_CLK_PRESCALE_64   (0b011 << CS10) // clk/64
#define TIMER1_CLOCK_SOURCE_CLK_PRESCALE_256  (0b100 << CS10) // clk/256
#define TIMER1_CLOCK_SOURCE_CLK_PRESCALE_1024 (0b101 << CS10) // clk/1024
#define TIMER1_CLOCK_SOURCE_EXT_T1PIN_FALLING (0b110 << CS10) // External clock source on T1 pin. Clock on falling edge.
#define TIMER1_CLOCK_SOURCE_EXT_T1PIN_RISING  (0b111 << CS10) // External clock source on T1 pin. Clock on rising edge.

#define TIMER1_INPUT_CAPTURE_TRIG_SOURCE_ICP 0 // Input Capture Pin (ICP1) as input capture trigger source
#define TIMER1_INPUT_CAPTURE_TRIG_SOURCE_ACO 1 // Analog Comparator Output (ACO) as input capture trigger source

#define TIMER1_COMPARE_OUTPUT_MODE_MASK_A                   (0b11 << COM1A0)
#define TIMER1_COMPARE_OUTPUT_MODE_NORMAL_A                 (0b00 << COM1A0) // Non-PWM mode: Normal port operation, OC1A disconnected
#define TIMER1_COMPARE_OUTPUT_MODE_TOGGLE_OC1_A             (0b01 << COM1A0) // Non-PWM mode: Toggle OC1A on Compare Match
#define TIMER1_COMPARE_OUTPUT_MODE_CLEAR_OC1_A              (0b10 << COM1A0) // Non-PWM mode: Clear OC1A on Compare Match
#define TIMER1_COMPARE_OUTPUT_MODE_SET_OC1_A                (0b11 << COM1A0) // Non-PWM mode: Set OC1A on Compare Match
#define TIMER1_COMPARE_OUTPUT_MODE_FAST_PWM_NORMAL_A        (0b00 << COM1A0) // Fast PWM mode: Normal port operation, OC1A disconnected
#define TIMER1_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_A (0b10 << COM1A0) // Fast PWM mode: Clear OC1A on Compare Match, set OC1A at TOP (Non-Inverting)
#define TIMER1_COMPARE_OUTPUT_MODE_FAST_PWM_INVERTING_A     (0b11 << COM1A0) // Fast PWM mode: Set OC1A on Compare Match, clear OC1A at TOP (Inverting)
#define TIMER1_COMPARE_OUTPUT_MODE_PHC_PWM_NORMAL_A         (0b00 << COM1A0) // Phase Correct and Phase and Frequency Correct PWM mode: Normal port operation, OC1A disconnected
#define TIMER1_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_A  (0b10 << COM1A0) // Phase Correct and Phase and Frequency Correct PWM mode: Clear OC1A on Compare Match when up-counting. Set OC1A on Compare Match when down-counting.
#define TIMER1_COMPARE_OUTPUT_MODE_PHC_PWM_INVERTING_A      (0b11 << COM1A0) // Phase Correct and Phase and Frequency Correct PWM mode: Set OC1A on Compare Match when up-counting. Clear OC1A on Compare Match when down-counting.

#define TIMER1_COMPARE_OUTPUT_MODE_MASK_B                   (0b11 << COM1B0)
#define TIMER1_COMPARE_OUTPUT_MODE_NORMAL_B                 (0b00 << COM1B0) // Non-PWM mode: Normal port operation, OC1B disconnected
#define TIMER1_COMPARE_OUTPUT_MODE_TOGGLE_OC1_B             (0b01 << COM1B0) // Non-PWM mode: Toggle OC1B on Compare Match
#define TIMER1_COMPARE_OUTPUT_MODE_CLEAR_OC1_B              (0b10 << COM1B0) // Non-PWM mode: Clear OC1B on Compare Match
#define TIMER1_COMPARE_OUTPUT_MODE_SET_OC1_B                (0b11 << COM1B0) // Non-PWM mode: Set OC1B on Compare Match
#define TIMER1_COMPARE_OUTPUT_MODE_FAST_PWM_NORMAL_B        (0b00 << COM1B0) // Fast PWM mode: Normal port operation, OC1B disconnected
#define TIMER1_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_B (0b10 << COM1B0) // Fast PWM mode: Clear OC1B on Compare Match, set OC1B at TOP (Non-Inverting)
#define TIMER1_COMPARE_OUTPUT_MODE_FAST_PWM_INVERTING_B     (0b11 << COM1B0) // Fast PWM mode: Set OC1B on Compare Match, clear OC1B at TOP (Inverting)
#define TIMER1_COMPARE_OUTPUT_MODE_PHC_PWM_NORMAL_B         (0b00 << COM1B0) // Phase Correct and Phase and Frequency Correct PWM mode: Normal port operation, OC1B disconnected
#define TIMER1_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_B  (0b10 << COM1B0) // Phase Correct and Phase and Frequency Correct PWM mode: Clear OC1B on Compare Match when up-counting. Set OC1B on Compare Match when down-counting.
#define TIMER1_COMPARE_OUTPUT_MODE_PHC_PWM_INVERTING_B      (0b11 << COM1B0) // Phase Correct and Phase and Frequency Correct PWM mode: Set OC1B on Compare Match when up-counting. Clear OC1B on Compare Match when down-counting.

#define TIMER1_WAVEFORM_GENERATION_MODE_MASK            0b1111
#define TIMER1_WAVEFORM_GENERATION_MODE_NORMAL          0b0000 // Normal
#define TIMER1_WAVEFORM_GENERATION_MODE_PHC_PWM_8BIT    0b0001 // PWM, Phase Correct, 8-bit, TOP=0x00FF
#define TIMER1_WAVEFORM_GENERATION_MODE_PHC_PWM_9BIT    0b0010 // PWM, Phase Correct, 9-bit, TOP=0x01FF
#define TIMER1_WAVEFORM_GENERATION_MODE_PHC_PWM_10BIT   0b0011 // PWM, Phase Correct, 10-bit, TOP=0x03FF
#define TIMER1_WAVEFORM_GENERATION_MODE_CTC_OCR1A       0b0100 // CTC, TOP=OCR1A
#define TIMER1_WAVEFORM_GENERATION_MODE_FAST_PWM_8BIT   0b0101 // Fast PWM, 8-bit, TOP=0x00FF
#define TIMER1_WAVEFORM_GENERATION_MODE_FAST_PWM_9BIT   0b0110 // Fast PWM, 9-bit, TOP=0x01FF
#define TIMER1_WAVEFORM_GENERATION_MODE_FAST_PWM_10BIT  0b0111 // Fast PWM, 10-bit, TOP=0x03FF
#define TIMER1_WAVEFORM_GENERATION_MODE_PHCFC_PWM_ICR1  0b1000 // PWM, Phase and Frequency Correct, TOP=ICR1
#define TIMER1_WAVEFORM_GENERATION_MODE_PHCFC_PWM_OCR1A 0b1001 // PWM, Phase and Frequency Correct, TOP=OCR1A
#define TIMER1_WAVEFORM_GENERATION_MODE_PHC_PWM_ICR1    0b1010 // PWM, Phase Correct, TOP=ICR1
#define TIMER1_WAVEFORM_GENERATION_MODE_PHC_PWM_OCR1A   0b1011 // PWM, Phase Correct, TOP=OCR1A
#define TIMER1_WAVEFORM_GENERATION_MODE_CTC_ICR1        0b1100 // CTC, TOP=ICR1
#define TIMER1_WAVEFORM_GENERATION_MODE_FAST_PWM_ICR1   0b1110 // Fast PWM, TOP=ICR1
#define TIMER1_WAVEFORM_GENERATION_MODE_FAST_PWM_OCR1A  0b1111 // Fast PWM, TOP=OCR1A

#define TIMER1_INPUT_CAPTURE_EDGE_MASK   (1 << ICES1)
#define TIMER1_INPUT_CAPTURE_EDGE_FALLING 0           // Falling (negative) edge is used as input capture trigger
#define TIMER1_INPUT_CAPTURE_EDGE_RISING (1 << ICES1) // Rising (positive) edge is used as input capture trigger

#define TIMER1_INTERRUPT_MASK               ( (1 << TOIE1) | (1 << OCIE1A) | (1 << OCIE1B) | (1 << ICIE1) )
#define TIMER1_INTERRUPT_DISABLE             0            // Disable interrupts
#define TIMER1_INTERRUPT_ON_OVERFLOW        (1 << TOIE1)  // Enable overflow interrupt
#define TIMER1_INTERRUPT_ON_INPUT_CAPTURE   (1 << ICIE1)  // Enable input capture interrupt
#define TIMER1_INTERRUPT_ON_COMPARE_MATCH_A (1 << OCIE1A) // Enable output compare match A interrupt
#define TIMER1_INTERRUPT_ON_COMPARE_MATCH_B (1 << OCIE1B) // Enable output compare match B interrupt

// Enables Timer1 module (in Power Reduction Register).
inline void Timer1_Enable()
{
    PRR &= ~(1 << PRTIM1);
}

// Disables Timer1 module (in Power Reduction Register).
inline void Timer1_Disable()
{
    PRR |= (1 << PRTIM1);
}

/*
Sets clock source and prescaler for Timer1.
If [clock_source] is nonzero Timer1 starts counting.
[clock_source] parameter can be one of the following:
    TIMER1_CLOCK_SOURCE_NONE              - No clock source (Timer/counter stopped).
    TIMER1_CLOCK_SOURCE_CLK_PRESCALE_1    - clk
    TIMER1_CLOCK_SOURCE_CLK_PRESCALE_8    - clk/8
    TIMER1_CLOCK_SOURCE_CLK_PRESCALE_64   - clk/64
    TIMER1_CLOCK_SOURCE_CLK_PRESCALE_256  - clk/256
    TIMER1_CLOCK_SOURCE_CLK_PRESCALE_1024 - clk/1024
    TIMER1_CLOCK_SOURCE_EXT_T1PIN_FALLING - External clock source on T1 pin. Clock on falling edge.
    TIMER1_CLOCK_SOURCE_EXT_T1PIN_RISING  - External clock source on T1 pin. Clock on rising edge.
*/
inline void Timer1_SetClockSource(unsigned char clock_source)
{
    unsigned char temp = TCCR1B & ~TIMER1_CLOCK_SOURCE_MASK;
    temp |= ( clock_source & TIMER1_CLOCK_SOURCE_MASK );
    TCCR1B = temp;
}

// Gets Timer1 counter register value.
inline unsigned int Timer1_GetTimerCounter()
{
    /*
    unsigned char sreg = SREG;
    cli();
    unsigned int tcnt1 = TCNT1;
    SREG = sreg;
    return tcnt1;
    */
    
    unsigned int tcnt1;
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        tcnt1 = TCNT1;
    }
    return tcnt1;
}

// Sets Timer1 counter register value.
inline void Timer1_SetTimerCounter(unsigned int value)
{
    /*
    unsigned char sreg = SREG;
    cli();
    TCNT1 = value;
    SREG = sreg;
    */
    
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        TCNT1 = value;
    }
}

// Gets Timer1 output compare register A value.
inline unsigned int Timer1_GetOutputCompareRegister_A()
{
    /*
    unsigned char sreg = SREG;
    cli();
    unsigned int ocr1a = OCR1A;
    SREG = sreg;
    return ocr1a;
    */
    
    unsigned int ocr1a;
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        ocr1a = OCR1A;
    }
    return ocr1a;
}

// Gets Timer1 output compare register A value.
inline unsigned int Timer1_GetOutputCompareRegister_B()
{
    /*
    unsigned char sreg = SREG;
    cli();
    unsigned int ocr1b = OCR1B;
    SREG = sreg;
    return ocr1b;
    */
    
    unsigned int ocr1b;
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        ocr1b = OCR1B;
    }
    return ocr1b;
}

// Sets Timer1 output compare register A value.
inline void Timer1_SetOutputCompareRegister_A(unsigned int value)
{
    /*
    unsigned char sreg = SREG;
    cli();
    OCR1A = value;
    SREG = sreg;
    */
    
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        OCR1A = value;
    }
}

// Sets Timer1 output compare register B value.
inline void Timer1_SetOutputCompareRegister_B(unsigned int value)
{
    /*
    unsigned char sreg = SREG;
    cli();
    OCR1B = value;
    SREG = sreg;
    */
    
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        OCR1B = value;
    }
}

// Gets Timer1 input capture register value.
inline unsigned int Timer1_GetInputCaptureRegister()
{
    /*
    unsigned char sreg = SREG;
    cli();
    unsigned int icr1 = ICR1;
    SREG = sreg;
    return icr1;
    */
    
    unsigned int icr1;
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        icr1 = ICR1;
    }
    return icr1;
}

// Sets Timer1 input capture register value.
inline void Timer1_SetInputCaptureRegister(unsigned int value)
{
    /*
    unsigned char sreg = SREG;
    cli();
    ICR1 = value;
    SREG = sreg;
    */
    
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        ICR1 = value;
    }
}

/*
Sets input capture trigger source.
[trig_source] can be one of the following:
    TIMER1_INPUT_CAPTURE_TRIG_SOURCE_ICP - Input Capture Pin (ICP1) as input capture trigger source
    TIMER1_INPUT_CAPTURE_TRIG_SOURCE_ACO - Analog Comparator Output (ACO) as input capture trigger source
*/
inline void Timer1_SetInputCaptureTrigSource(unsigned char trig_source)
{
    switch(trig_source)
    {
        case TIMER1_INPUT_CAPTURE_TRIG_SOURCE_ICP:
        {
            ACSR &= ~(1 << ACIC);
            break;
        }
        case TIMER1_INPUT_CAPTURE_TRIG_SOURCE_ACO:
        {
            ACSR |= (1 << ACIC);            
            break;
        }
    }
}

// Enables input capture noise canceler.
inline void Timer1_EnableInputCaptureNoiseCanceler()
{
    TCCR1B |= (1 << ICNC1);
}

// Disables input capture noise canceler.
inline void Timer1_DisableInputCaptureNoiseCanceler()
{
    TCCR1B &= ~(1 << ICNC1);
}

/*
Selects which edge on the Input Capture Pin (ICP1) is used to trigger a capture event.
[edge] can be one of the following:
    TIMER1_INPUT_CAPTURE_EDGE_FALLING - Falling (negative) edge is used as input capture trigger
    TIMER1_INPUT_CAPTURE_EDGE_RISING  - Rising (positive) edge is used as input capture trigger
*/
inline void Timer1_InputCaptureEdgeSelect(unsigned char edge)
{
    unsigned char temp = TCCR1B & ~TIMER1_INPUT_CAPTURE_EDGE_MASK;
    temp |= (edge & TIMER1_INPUT_CAPTURE_EDGE_MASK);
    TCCR1B = temp;
}

/*
Sets compare output mode for Timer1's OC1A output.
[mode] parameter can be one of the following:
    TIMER1_COMPARE_OUTPUT_MODE_NORMAL_A     - Non-PWM mode: Normal port operation, OC1A disconnected
    TIMER1_COMPARE_OUTPUT_MODE_TOGGLE_OC1_A - Non-PWM mode: Toggle OC1A on Compare Match
    TIMER1_COMPARE_OUTPUT_MODE_CLEAR_OC1_A  - Non-PWM mode: Clear OC1A on Compare Match
    TIMER1_COMPARE_OUTPUT_MODE_SET_OC1_A    - Non-PWM mode: Set OC1A on Compare Match
    TIMER1_COMPARE_OUTPUT_MODE_FAST_PWM_NORMAL_A        - Fast PWM mode: Normal port operation, OC1A disconnected
    TIMER1_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_A - Fast PWM mode: Clear OC1A on Compare Match, set OC1A at TOP (Non-Inverting)
    TIMER1_COMPARE_OUTPUT_MODE_FAST_PWM_INVERTING_A     - Fast PWM mode: Set OC1A on Compare Match, clear OC1A at TOP (Inverting)
    TIMER1_COMPARE_OUTPUT_MODE_PHC_PWM_NORMAL_A        - Phase Correct and Phase and Frequency Correct PWM mode: Normal port operation, OC1A disconnected
    TIMER1_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_A - Phase Correct and Phase and Frequency Correct PWM mode: Clear OC1A on Compare Match when up-counting. Set OC1A on Compare Match when down-counting.
    TIMER1_COMPARE_OUTPUT_MODE_PHC_PWM_INVERTING_A     - Phase Correct and Phase and Frequency Correct PWM mode: Set OC1A on Compare Match when up-counting. Clear OC1A on Compare Match when down-counting.
*/
inline void Timer1_SetCompareOutputMode_A(unsigned char mode)
{
    unsigned char temp = TCCR1A & ~TIMER1_COMPARE_OUTPUT_MODE_MASK_A;
    temp |= ( mode & TIMER1_COMPARE_OUTPUT_MODE_MASK_A );
    TCCR1A = temp;
}

/*
Sets compare output mode for Timer1's OC1B output.
[mode] parameter can be one of the following:
    TIMER1_COMPARE_OUTPUT_MODE_NORMAL_B     - Non-PWM mode: Normal port operation, OC1B disconnected
    TIMER1_COMPARE_OUTPUT_MODE_TOGGLE_OC1_B - Non-PWM mode: Toggle OC1B on Compare Match
    TIMER1_COMPARE_OUTPUT_MODE_CLEAR_OC1_B  - Non-PWM mode: Clear OC1B on Compare Match
    TIMER1_COMPARE_OUTPUT_MODE_SET_OC1_B    - Non-PWM mode: Set OC1B on Compare Match
    TIMER1_COMPARE_OUTPUT_MODE_FAST_PWM_NORMAL_B        - Fast PWM mode: Normal port operation, OC1B disconnected
    TIMER1_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_B - Fast PWM mode: Clear OC1B on Compare Match, set OC1B at TOP (Non-Inverting)
    TIMER1_COMPARE_OUTPUT_MODE_FAST_PWM_INVERTING_B     - Fast PWM mode: Set OC1B on Compare Match, clear OC1B at TOP (Inverting)
    TIMER1_COMPARE_OUTPUT_MODE_PHC_PWM_NORMAL_B        - Phase Correct and Phase and Frequency Correct PWM mode: Normal port operation, OC1B disconnected
    TIMER1_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_B - Phase Correct and Phase and Frequency Correct PWM mode: Clear OC1B on Compare Match when up-counting. Set OC1B on Compare Match when down-counting.
    TIMER1_COMPARE_OUTPUT_MODE_PHC_PWM_INVERTING_B     - Phase Correct and Phase and Frequency Correct PWM mode: Set OC1B on Compare Match when up-counting. Clear OC1B on Compare Match when down-counting.
*/
inline void Timer1_SetCompareOutputMode_B(unsigned char mode)
{
    unsigned char temp = TCCR1A & ~TIMER1_COMPARE_OUTPUT_MODE_MASK_B;
    temp |= ( mode & TIMER1_COMPARE_OUTPUT_MODE_MASK_B );
    TCCR1A = temp;
}

/*
Sets waveform generation mode for Timer1.
[mode] parameter can be one of the following:
    TIMER1_WAVEFORM_GENERATION_MODE_NORMAL - Normal
    TIMER1_WAVEFORM_GENERATION_MODE_PHC_PWM_8BIT  - PWM, Phase Correct, 8-bit
    TIMER1_WAVEFORM_GENERATION_MODE_PHC_PWM_9BIT  - PWM, Phase Correct, 9-bit
    TIMER1_WAVEFORM_GENERATION_MODE_PHC_PWM_10BIT - PWM, Phase Correct, 10-bit
    TIMER1_WAVEFORM_GENERATION_MODE_CTC_OCR1A - CTC, TOP=OCR1A
    TIMER1_WAVEFORM_GENERATION_MODE_FAST_PWM_8BIT  - Fast PWM, 8-bit
    TIMER1_WAVEFORM_GENERATION_MODE_FAST_PWM_9BIT  - Fast PWM, 9-bit
    TIMER1_WAVEFORM_GENERATION_MODE_FAST_PWM_10BIT - Fast PWM, 10-bit
    TIMER1_WAVEFORM_GENERATION_MODE_PHCFC_PWM_ICR1  - PWM, Phase and Frequency Correct, TOP=ICR1
    TIMER1_WAVEFORM_GENERATION_MODE_PHCFC_PWM_OCR1A - PWM, Phase and Frequency Correct, TOP=OCR1A
    TIMER1_WAVEFORM_GENERATION_MODE_PHC_PWM_ICR1  - PWM, Phase Correct, TOP=ICR1
    TIMER1_WAVEFORM_GENERATION_MODE_PHC_PWM_OCR1A - PWM, Phase Correct, TOP=OCR1A
    TIMER1_WAVEFORM_GENERATION_MODE_CTC_ICR1 - CTC, TOP=ICR1
    TIMER1_WAVEFORM_GENERATION_MODE_FAST_PWM_ICR1  - Fast PWM, TOP=ICR1
    TIMER1_WAVEFORM_GENERATION_MODE_FAST_PWM_OCR1A - Fast PWM, TOP=OCR1A
*/
inline void Timer1_SetWaveformGenerationMode(unsigned char mode)
{
    unsigned char temp = TCCR1A & ~(0b11 << WGM10);
    temp |= (( mode & 0b0011 ) << WGM10);
    TCCR1A = temp;
    
    temp = TCCR1B & ~(0b11 << WGM12);
    temp |= ( ((mode & 0b1100 ) >> 2) << WGM12 );
    TCCR1B = temp;
}

/*
Enables or disables interrupts for Timer1
[mode] parameter can be a bitwise OR combination of the following flags:
    TIMER1_INTERRUPT_DISABLE            - Disable interrupts
    TIMER1_INTERRUPT_ON_OVERFLOW        - Enable overflow interrupt
    TIMER1_INTERRUPT_ON_INPUT_CAPTURE   - Enable input capture interrupt
    TIMER1_INTERRUPT_ON_COMPARE_MATCH_A - Enable output compare match A interrupt
    TIMER1_INTERRUPT_ON_COMPARE_MATCH_B - Enable output compare match B interrupt
*/
inline void Timer1_SetupInterrupts(unsigned char mode)
{
    unsigned char temp = TIMSK1 & ~TIMER1_INTERRUPT_MASK;
    temp |= ( mode & TIMER1_INTERRUPT_MASK );
    TIMSK1 = temp;
}

// Clears input-capture event flag (the one causing input capture interrupt).
inline void Timer1_ClearInputCaptureEventFlag()
{
    TIFR1 |= (1 << ICF1);
}

// Stops Timer1 and clears its counter register.
inline void Timer1_Stop()
{
    Timer1_SetClockSource(TIMER1_CLOCK_SOURCE_NONE);
    Timer1_SetTimerCounter(0); // Reset timer counter register value.
}

// Configures Timer1's Output Complare Match Output pin A as output.
inline void Timer1_EnableOC1A()
{
    IOPORT_CONFIG_PIN_OUT(TIMER1_OC1A_PORT, TIMER1_OC1A_PIN);
}

// Configures Timer1's Output Complare Match Output pin A as input.
inline void Timer1_DisableOC1A()
{
    IOPORT_CONFIG_PIN_IN(TIMER1_OC1A_PORT, TIMER1_OC1A_PIN);
}

// Configures Timer1's Output Complare Match Output pin B as output.
inline void Timer1_EnableOC1B()
{
    IOPORT_CONFIG_PIN_OUT(TIMER1_OC1B_PORT, TIMER1_OC1B_PIN);
}

// Configures Timer1's Output Complare Match Output pin B as input.
inline void Timer1_DisableOC1B()
{
    IOPORT_CONFIG_PIN_IN(TIMER1_OC1B_PORT, TIMER1_OC1B_PIN);
}

inline void Timer1_Init()
{
    // Stop Timer1, set Normal waveform generation mode, set Normal compare output mode.
    TCCR1B = 0;
    TCCR1A = 0;
    Timer1_SetTimerCounter(0);  // Reset timer counter register value.
    TIMSK1 = 0; // Disable all Timer1 interrupts.
    Timer1_Enable();
}

/*
Sets ICR1 register according to the desired frequency of compare match interrupt.
[freq] - frequency of compare match interrupt in Hz.
Limitations: [freq] >= clk / 2^24 Hz ( 1 Hz  for clk = 16 MHz).
             [freq] <= clk           (16 MHz for clk = 16 MHz).
Remarks: 1) This function should be called only if the waveform genegation mode is set to
            a one utilizing ICR1 as TOP value for the timer/counter.
         2) Don't call this function if you want to use PB0 as output!
*/
inline void Timer1_SetICR1(unsigned long freq)
{
    // freq = clk / (PRESCALER * (1 + ICR1))
    // ICR1 = clk / (PRESCALER * freq) - 1

    // Configure input capture pin (ICP1 which is PB0) as input,
    // because otherwise ICR1 would be likely to be changed by software by
    // changing PB0 when it is configured as output.
    IOPORT_CONFIG_PIN_IN(B, 0);
    
    if(F_CPU < freq)
    {
        ICR1 = 0;
        Timer1_SetClockSource(TIMER1_CLOCK_SOURCE_CLK_PRESCALE_1);
    }
    else if((F_CPU >> 16) < freq)
    {
        ICR1 = F_CPU / freq - 1;
        Timer1_SetClockSource(TIMER1_CLOCK_SOURCE_CLK_PRESCALE_1);
    }
    else if((F_CPU >> 19) < freq)
    {
        ICR1 = (F_CPU >> 3) / freq - 1;
        Timer1_SetClockSource(TIMER1_CLOCK_SOURCE_CLK_PRESCALE_8);
    }
    else if((F_CPU >> 22) < freq)
    {
        ICR1 = (F_CPU >> 6) / freq - 1;
        Timer1_SetClockSource(TIMER1_CLOCK_SOURCE_CLK_PRESCALE_64);
    }
    else if((F_CPU >> 24) < freq)
    {
        ICR1 = (F_CPU >> 8) / freq - 1;
        Timer1_SetClockSource(TIMER1_CLOCK_SOURCE_CLK_PRESCALE_256);
    }
    else if((F_CPU >> 26) < freq)
    {
        ICR1 = (F_CPU >> 10) / freq - 1;
        Timer1_SetClockSource(TIMER1_CLOCK_SOURCE_CLK_PRESCALE_1024);
    }
    else
    {
        ICR1 = 0xFFFF;
        Timer1_SetClockSource(TIMER1_CLOCK_SOURCE_CLK_PRESCALE_1024);
    }
}

/*
Sets up CTC mode (Clear Timer on Comapare Match) for Timer1 and sets timer frequency in Hz.
[freq] - timer compare match interrupt frequency in Hz
Limitations: [freq] >= clk / 2^26 Hz (0.25 Hz for clk = 16 MHz).
             [freq] <= clk           (16  MHz for clk = 16 MHz).
*/
inline void Timer1_SetupCtcMode(unsigned long freq)
{
    Timer1_Stop();
    Timer1_SetWaveformGenerationMode(TIMER1_WAVEFORM_GENERATION_MODE_CTC_ICR1);
    Timer1_SetICR1(freq);
}

/*
Sets up meander on OC1A output using CTC mode.
[freq] - meander frequency in Hz.
*/
inline void Timer1_SetupMeander_A(unsigned long freq)
{
    Timer1_DisableOC1B();
    OCR1A = 0;
    Timer1_SetCompareOutputMode_A(TIMER1_COMPARE_OUTPUT_MODE_TOGGLE_OC1_A);
    Timer1_SetupCtcMode(freq << 1);
    Timer1_EnableOC1A();
}

/*
Sets up meander on OC1B output using CTC mode.
[freq] - meander frequency in Hz.
*/
inline void Timer1_SetupMeander_B(unsigned long freq)
{
    Timer1_DisableOC1A();
    OCR1B = 0;
    Timer1_SetCompareOutputMode_B(TIMER1_COMPARE_OUTPUT_MODE_TOGGLE_OC1_B);
    Timer1_SetupCtcMode(freq << 1);
    Timer1_EnableOC1B();
}

/*
Sets up meanders on OC1A and OC1B outputs using CTC mode.
[freq] - meander frequency in Hz.
[phase_shift] - phase shift between OC1B and OC1A meanders as a fraction of meander period (0...1).
*/
inline void Timer1_SetupMeander_AB(
    unsigned long freq,
    float phase_shift)
{
    Timer1_SetCompareOutputMode_A(TIMER1_COMPARE_OUTPUT_MODE_TOGGLE_OC1_A);
    Timer1_SetCompareOutputMode_B(TIMER1_COMPARE_OUTPUT_MODE_TOGGLE_OC1_B);
    Timer1_SetupCtcMode(freq << 1);
    OCR1A = 0;
    OCR1B = static_cast<unsigned int>(phase_shift * ICR1);
    Timer1_EnableOC1A();
    Timer1_EnableOC1B();
}

/*
Sets up Fast Pulse Width Modulation mode for Timer1 on OC1A output.
[freq] - PWM frequency in Hz.
[duty_cycle] - PWM duty cycle (0...1).
*/
inline void Timer1_SetupFastPwmMode_A(
    unsigned long freq,
    float duty_cycle)
{
    Timer1_Stop();
    Timer1_DisableOC1B();
    Timer1_SetWaveformGenerationMode(TIMER1_WAVEFORM_GENERATION_MODE_FAST_PWM_ICR1);
    Timer1_SetICR1(freq);
    OCR1A = static_cast<unsigned int>(duty_cycle * ICR1);
    Timer1_SetCompareOutputMode_A(TIMER1_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_A);
    Timer1_EnableOC1A();
}

/*
Sets up Fast Pulse Width Modulation mode for Timer1 on OC1B output.
[freq] - PWM frequency in Hz.
[duty_cycle] - PWM duty cycle (0...1).
*/
inline void Timer1_SetupFastPwmMode_B(
    unsigned long freq,
    float duty_cycle)
{
    Timer1_Stop();
    Timer1_DisableOC1A();
    Timer1_SetWaveformGenerationMode(TIMER1_WAVEFORM_GENERATION_MODE_FAST_PWM_ICR1);
    Timer1_SetICR1(freq);
    OCR1B = static_cast<unsigned int>(duty_cycle * ICR1);
    Timer1_SetCompareOutputMode_B(TIMER1_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_B);
    Timer1_EnableOC1B();
}

/*
Sets up Fast Pulse Width Modulation mode for Timer1 on OC1A and OC1B outputs.
[freq] - PWM frequency in Hz.
[duty_cycle_A], [duty_cycle_B] - PWM duty cycle (0...1).
*/
inline void Timer1_SetupFastPwmMode_AB(
    unsigned long freq,
    float duty_cycle_A,
    float duty_cycle_B)
{
    Timer1_Stop();
    Timer1_DisableOC1A();
    Timer1_DisableOC1B();
    Timer1_SetWaveformGenerationMode(TIMER1_WAVEFORM_GENERATION_MODE_FAST_PWM_ICR1);
    Timer1_SetICR1(freq);
    OCR1A = static_cast<unsigned int>(duty_cycle_A * ICR1);
    OCR1B = static_cast<unsigned int>(duty_cycle_B * ICR1);
    Timer1_SetCompareOutputMode_A(TIMER1_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_A);
    Timer1_SetCompareOutputMode_B(TIMER1_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_B);
    Timer1_EnableOC1A();
    Timer1_EnableOC1B();
}

/*
Sets up Phase and Frequency Correct Pulse Width Modulation mode for Timer1 on OC1A output.
[freq] - PWM frequency in Hz.
[duty_cycle] - PWM duty cycle (0...1).
*/
inline void Timer1_SetupPhaseCorrectPwmMode_A(
    unsigned long freq,
    float duty_cycle)
{
    Timer1_Stop();
    Timer1_DisableOC1B();
    Timer1_SetWaveformGenerationMode(TIMER1_WAVEFORM_GENERATION_MODE_PHCFC_PWM_ICR1);
    Timer1_SetICR1(freq << 1);
    OCR1A = static_cast<unsigned int>(duty_cycle * ICR1);
    Timer1_SetCompareOutputMode_A(TIMER1_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_A);
    Timer1_EnableOC1A();
}

/*
Sets up Phase and Frequency Correct Pulse Width Modulation mode for Timer1 on OC1B output.
[freq] - PWM frequency in Hz.
[duty_cycle] - PWM duty cycle (0...1).
*/
inline void Timer1_SetupPhaseCorrectPwmMode_B(
    unsigned long freq,
    float duty_cycle)
{
    Timer1_Stop();
    Timer1_DisableOC1A();
    Timer1_SetWaveformGenerationMode(TIMER1_WAVEFORM_GENERATION_MODE_PHCFC_PWM_ICR1);
    Timer1_SetICR1(freq << 1);
    OCR1B = static_cast<unsigned int>(duty_cycle * ICR1);
    Timer1_SetCompareOutputMode_B(TIMER1_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_B);
    Timer1_EnableOC1B();
}

/*
Sets up Phase and Frequency Correct Pulse Width Modulation mode for Timer1 on OC1A and OC1B outputs.
[freq] - PWM frequency in Hz.
[duty_cycle_A], [duty_cycle_B] - PWM duty cycle (0...1).
*/
inline void Timer1_SetupPhaseCorrectPwmMode_AB(
    unsigned long freq,
    float duty_cycle_A,
    float duty_cycle_B)
{
    Timer1_Stop();
    Timer1_DisableOC1A();
    Timer1_DisableOC1B();
    Timer1_SetWaveformGenerationMode(TIMER1_WAVEFORM_GENERATION_MODE_PHCFC_PWM_ICR1);
    Timer1_SetICR1(freq << 1);
    OCR1A = static_cast<unsigned int>(duty_cycle_A * ICR1);
    OCR1B = static_cast<unsigned int>(duty_cycle_B * ICR1);
    Timer1_SetCompareOutputMode_A(TIMER1_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_A);
    Timer1_SetCompareOutputMode_B(TIMER1_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_B);
    Timer1_EnableOC1A();
    Timer1_EnableOC1B();
}

/*
Enables interrupt on Timer1 overflow event (all other functions of Timer1 will be disabled) and
starts counting according to specified [clock_source]. Interrupt frequency will be equal to
timer clock source frequency divided by 65536.
[clock_source] parameter can be one of the following:
    TIMER1_CLOCK_SOURCE_NONE              - No clock source (Timer/counter stopped).
    TIMER1_CLOCK_SOURCE_CLK_PRESCALE_1    - clk
    TIMER1_CLOCK_SOURCE_CLK_PRESCALE_8    - clk/8
    TIMER1_CLOCK_SOURCE_CLK_PRESCALE_64   - clk/64
    TIMER1_CLOCK_SOURCE_CLK_PRESCALE_256  - clk/256
    TIMER1_CLOCK_SOURCE_CLK_PRESCALE_1024 - clk/1024
    TIMER1_CLOCK_SOURCE_EXT_T1PIN_FALLING - External clock source on T1 pin. Clock on falling edge.
    TIMER1_CLOCK_SOURCE_EXT_T1PIN_RISING  - External clock source on T1 pin. Clock on rising edge.
Remarks: use #include <avr/interrupt.h> ISR(TIMER1_OVF_vect) { ... } for interrupt handling
*/
inline void Timer1_InterruptOnOverflow(unsigned char clock_source)
{
    Timer1_Init();
    Timer1_SetupInterrupts(TIMER1_INTERRUPT_ON_OVERFLOW);
    Timer1_SetClockSource(clock_source);
    sei();
}

/*
Enables interrupt on Timer1 input capture event (all other functions of Timer1 will be disabled) and
starts counting. Interrupts will be happening with specified frequency [freq].
[freq] - timer compare match interrupt frequency in Hz
Limitations: [freq] >= clk / 2^26 Hz (0.25 Hz for clk = 16 MHz).
             [freq] <= clk           (16  MHz for clk = 16 MHz).
Remarks: use #include <avr/interrupt.h> ISR(TIMER1_CAPT_vect) { ... } for interrupt handling
*/
inline void Timer1_InterruptOnInputCapture(unsigned long freq)
{
    Timer1_Init();
    Timer1_SetupInterrupts(TIMER1_INTERRUPT_ON_INPUT_CAPTURE);
    Timer1_SetupCtcMode(freq);
    sei();
}

/*
Timer1 will generate input capture event interrupt every [n_pulses] rising pulses on T1 pin
(all other functions of Timer1 will be disabled).
[n_pulses] - 0...65535
Remarks: use #include <avr/interrupt.h> ISR(TIMER1_COMPA_vect) { ... } for interrupt handling
*/
inline void Timer1_InterruptOnCountExternalPulses(unsigned int n_pulses)
{
    Timer1_Init();
    
    Timer1_SetWaveformGenerationMode(TIMER1_WAVEFORM_GENERATION_MODE_CTC_OCR1A);
    OCR1A = n_pulses;
    
    Timer1_SetupInterrupts(TIMER1_INTERRUPT_ON_COMPARE_MATCH_A);
    sei();
    
    Timer1_SetClockSource(TIMER1_CLOCK_SOURCE_EXT_T1PIN_RISING);
}    

#endif
