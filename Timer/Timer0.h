#ifndef _TIMER0_H
#define _TIMER0_H

#include <avr/io.h>

#include "IOPORT.h"

#if defined(__AVR_ATmega8535__) || defined(__AVR_ATmega16__)
    #define TIMER0_OC0_PORT B
    #define TIMER0_OC0_PIN 3
#endif

#define TIMER0_CLOCK_SOURCE_MASK              (0b111 << CS00)
#define TIMER0_CLOCK_SOURCE_NONE              (0b000 << CS00) // No clock source (Timer/counter stopped)
#define TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1    (0b001 << CS00) // clk
#define TIMER0_CLOCK_SOURCE_CLK_PRESCALE_8    (0b010 << CS00) // clk/8
#define TIMER0_CLOCK_SOURCE_CLK_PRESCALE_64   (0b011 << CS00) // clk/64
#define TIMER0_CLOCK_SOURCE_CLK_PRESCALE_256  (0b100 << CS00) // clk/256
#define TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1024 (0b101 << CS00) // clk/1024
#define TIMER0_CLOCK_SOURCE_EXT_T0PIN_FALLING (0b110 << CS00) // External clock source on T0 pin. Clock on falling edge.
#define TIMER0_CLOCK_SOURCE_EXT_T0PIN_RISING  (0b111 << CS00) // External clock source on T0 pin. Clock on rising edge.

#define TIMER0_WAVEFORM_GENERATION_MODE_MASK     ( (1 << WGM01) | (1 << WGM00) )
#define TIMER0_WAVEFORM_GENERATION_MODE_NORMAL   ( (0 << WGM01) | (0 << WGM00) ) // Normal mode
#define TIMER0_WAVEFORM_GENERATION_MODE_PHC_PWM  ( (0 << WGM01) | (1 << WGM00) ) // Phase Correct Pulse Width Modulation mode
#define TIMER0_WAVEFORM_GENERATION_MODE_CTC      ( (1 << WGM01) | (0 << WGM00) ) // Clear on Compare Match mode
#define TIMER0_WAVEFORM_GENERATION_MODE_FAST_PWM ( (1 << WGM01) | (1 << WGM00) ) // Fast Pulse Width Modulation mode

#define TIMER0_COMPARE_OUTPUT_MODE_MASK                   (0b11 << COM00)
#define TIMER0_COMPARE_OUTPUT_MODE_NORMAL                 (0b00 << COM00) // Non-PWM mode: Normal port operation, OC0 disconnected
#define TIMER0_COMPARE_OUTPUT_MODE_TOGGLE_OC0             (0b01 << COM00) // Non-PWM mode: Toggle OC0 on Compare Match
#define TIMER0_COMPARE_OUTPUT_MODE_CLEAR_OC0              (0b10 << COM00) // Non-PWM mode: Clear OC0 on Compare Match
#define TIMER0_COMPARE_OUTPUT_MODE_SET_OC0                (0b11 << COM00) // Non-PWM mode: Set OC0 on Compare Match
#define TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_NORMAL        (0b00 << COM00) // Fast PWM mode: Normal port operation, OC0 disconnected
#define TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING (0b10 << COM00) // Fast PWM mode: Clear OC0 on Compare Match, set OC0 at TOP (Non-Inverting, means that duty cycle equals OCR0/255)
#define TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_INVERTING     (0b11 << COM00) // Fast PWM mode: Set OC0 on Compare Match, clear OC0 at TOP (Inverting, means that duty cycle equals 1 - OCR0/255)
#define TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_NORMAL         (0b00 << COM00) // Phase Correct PWM mode: Normal port operation, OC0 disconnected
#define TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING  (0b10 << COM00) // Phase Correct PWM mode: Clear OC0 on Compare Match when up-counting. Set OC0 on Compare Match when down-counting.
#define TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_INVERTING      (0b11 << COM00) // Phase Correct PWM mode: Set OC0 on Compare Match when up-counting. Clear OC0 on Compare Match when down-counting.

#define TIMER0_INTERRUPT_MASK             (0b11 << TOIE0)
#define TIMER0_INTERRUPT_DISABLE          (0b00 << TOIE0) // Disable interrupts
#define TIMER0_INTERRUPT_ON_OVERFLOW      (1 << TOIE0)    // Enable overflow interrupt
#define TIMER0_INTERRUPT_ON_COMPARE_MATCH (1 << OCIE0)    // Enable output compare match interrupt

/*
Sets clock source and prescaler for Timer0.
If [clock_source] is nonzero Timer0 starts counting.
[clock_source] parameter can be one of the following:
    TIMER0_CLOCK_SOURCE_NONE              - No clock source (Timer/counter stopped).
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1    - clk
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_8    - clk/8
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_64   - clk/64
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_256  - clk/256
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1024 - clk/1024
    TIMER0_CLOCK_SOURCE_EXT_T0PIN_FALLING - External clock source on T0 pin. Clock on falling edge.
    TIMER0_CLOCK_SOURCE_EXT_T0PIN_RISING  - External clock source on T0 pin. Clock on rising edge.
*/
inline void Timer0_SetClockSource(unsigned char clock_source)
{
    unsigned char temp = TCCR0 & ~TIMER0_CLOCK_SOURCE_MASK;
    temp |= ( clock_source & TIMER0_CLOCK_SOURCE_MASK );
    TCCR0 = temp;
}

/*
Sets waveform generation mode for Timer0.
[mode] parameter can be one of the following:
    TIMER0_WAVEFORM_GENERATION_MODE_NORMAL   - Normal mode
    TIMER0_WAVEFORM_GENERATION_MODE_PHC_PWM  - Phase Correct Pulse Width Modulation mode
    TIMER0_WAVEFORM_GENERATION_MODE_CTC      - Clear on Compare Match mode
    TIMER0_WAVEFORM_GENERATION_MODE_FAST_PWM - Fast Pulse Width Modulation mode
*/
inline void Timer0_SetWaveformGenerationMode(unsigned char mode)
{
    unsigned char temp = TCCR0 & ~TIMER0_WAVEFORM_GENERATION_MODE_MASK;
    temp |= ( mode & TIMER0_WAVEFORM_GENERATION_MODE_MASK );
    TCCR0 = temp;
}

// Configures Timer0's Output Complare Match Output pin as output.
inline void Timer0_EnableOC0()
{
    IOPORT_CONFIG_PIN_OUT(TIMER0_OC0_PORT, TIMER0_OC0_PIN);
}

// Configures Timer0's Output Complare Match Output pin as input.
inline void Timer0_DisableOC0()
{
    IOPORT_CONFIG_PIN_IN(TIMER0_OC0_PORT, TIMER0_OC0_PIN);
}

/*
Sets compare output mode for Timer0's OC0 output.
[mode] parameter can be one of the following:
    TIMER0_COMPARE_OUTPUT_MODE_NORMAL     - Non-PWM mode: Normal port operation, OC0 disconnected
    TIMER0_COMPARE_OUTPUT_MODE_TOGGLE_OC0 - Non-PWM mode: Toggle OC0 on Compare Match
    TIMER0_COMPARE_OUTPUT_MODE_CLEAR_OC0  - Non-PWM mode: Clear OC0 on Compare Match
    TIMER0_COMPARE_OUTPUT_MODE_SET_OC0    - Non-PWM mode: Set OC0 on Compare Match
    TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_NORMAL        - Fast PWM mode: Normal port operation, OC0 disconnected
    TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING - Fast PWM mode: Clear OC0 on Compare Match, set OC0 at TOP (Non-Inverting)
    TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_INVERTING     - Fast PWM mode: Set OC0 on Compare Match, clear OC0 at TOP (Inverting)
    TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_NORMAL        - Phase Correct PWM mode: Normal port operation, OC0 disconnected
    TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING - Phase Correct PWM mode: Clear OC0 on Compare Match when up-counting. Set OC0 on Compare Match when down-counting.
    TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_INVERTING     - Phase Correct PWM mode: Set OC0 on Compare Match when up-counting. Clear OC0 on Compare Match when down-counting.
*/
inline void Timer0_SetCompareOutputMode(unsigned char mode)
{
    unsigned char temp = TCCR0 & ~TIMER0_COMPARE_OUTPUT_MODE_MASK;
    temp |= ( mode & TIMER0_COMPARE_OUTPUT_MODE_MASK );
    TCCR0 = temp;
}

/*
Enables or disables interrupts for Timer0
[mode] parameter can be a bitwise OR combination of the following flags:
    TIMER0_INTERRUPT_DISABLE          - Disable interrupts
    TIMER0_INTERRUPT_ON_OVERFLOW      - Enable overflow interrupt
    TIMER0_INTERRUPT_ON_COMPARE_MATCH - Enable output compare match interrupt
*/
inline void Timer0_SetupInterrupts(unsigned char mode)
{
    unsigned char temp = TIMSK & ~TIMER0_INTERRUPT_MASK;
    temp |= ( mode & TIMER0_INTERRUPT_MASK );
    TIMSK = temp;
}

// Resets the prescaler module of Timer0 and Timer1
// for synchronizing the Timer/Counter to program execution.
inline void Timer0_Timer1_ResetPrescaler() { SFIOR |= (1 << PSR10); }

// Gets Timer0 counter register value.
inline unsigned char Timer0_GetTimerCounter() { return TCNT0; }

// Sets Timer0 counter register value.
inline void Timer0_SetTimerCounter(unsigned char value) { TCNT0 = value; }

// Stops Timer0 and clears its counter register.
inline void Timer0_Stop()
{
    Timer0_SetClockSource(TIMER0_CLOCK_SOURCE_NONE);
    TCNT0 = 0; // Reset timer counter register value.
}

inline void Timer0_Init()
{
    TCCR0 = 0; // Stop Timer0, set Normal waveform generation mode, set Normal compare output mode.
    TCNT0 = 0; // Reset timer counter register value.
    Timer0_SetupInterrupts(TIMER0_INTERRUPT_DISABLE); // Disable all Timer0 interrupts.
}

/*
Sets up CTC mode (Clear Timer on Comapare Match) for Timer0 and sets timer frequency in Hz.
[freq] - timer compare match interrupt frequency in Hz
Limitations: [freq] >= clk / 2^18 Hz (4 Hz  for clk = 1 MHz).
             [freq] <= clk           (1 MHz for clk = 1 MHz).
*/
inline void Timer0_SetupCtcMode(unsigned long freq)
{
    // freq = clk / (PRESCALER * (1 + OCR0))
    // OCR0 = clk / (PRESCALER * freq) - 1
    
    Timer0_Stop();
    Timer0_SetWaveformGenerationMode(TIMER0_WAVEFORM_GENERATION_MODE_CTC);
    
    if((F_CPU >> 8) < freq)
    {
        OCR0 = F_CPU / freq - 1;
        Timer0_SetClockSource(TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1);
    }
    else if((F_CPU >> 11) < freq)
    {
        OCR0 = (F_CPU >> 3) / freq - 1;
        Timer0_SetClockSource(TIMER0_CLOCK_SOURCE_CLK_PRESCALE_8);
    }
    else if((F_CPU >> 14) < freq)
    {
        OCR0 = (F_CPU >> 6) / freq - 1;
        Timer0_SetClockSource(TIMER0_CLOCK_SOURCE_CLK_PRESCALE_64);
    }
    else if((F_CPU >> 16) < freq)
    {
        OCR0 = (F_CPU >> 8) / freq - 1;
        Timer0_SetClockSource(TIMER0_CLOCK_SOURCE_CLK_PRESCALE_256);
    }
    else if((F_CPU >> 18) < freq)
    {
        OCR0 = (F_CPU >> 10) / freq - 1;
        Timer0_SetClockSource(TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1024);
    }
}

/*
Sets up meander on OC0 output using CTC mode.
[freq] - meander frequency in Hz.
*/
inline void Timer0_SetupMeander(unsigned long freq)
{
    Timer0_SetCompareOutputMode(TIMER0_COMPARE_OUTPUT_MODE_TOGGLE_OC0);
    Timer0_SetupCtcMode(freq << 1);
    Timer0_EnableOC0();
}

/*
Sets up Fast Pulse Width Modulation mode for Timer0.
PWM frequency equals clk/(PRESCALER * 256).
[prescaler] can be one of the following:
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_8
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_64
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_256
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1024
[duty_cycle] - Should range from 0 to 255. Actual duty cycle of PWM signal is calculated as duty_cycle/255.
*/
inline void Timer0_SetupFastPwmMode(
    unsigned char prescaler,
    unsigned char duty_cycle)
{
    Timer0_Stop();
    OCR0 = duty_cycle;
    Timer0_SetWaveformGenerationMode(TIMER0_WAVEFORM_GENERATION_MODE_FAST_PWM);
    Timer0_SetCompareOutputMode(TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING);
    Timer0_SetClockSource(prescaler);
    Timer0_EnableOC0();
}

/*
Sets up Phase Correct Pulse Width Modulation mode for Timer0.
PWM frequency equals clk/(PRESCALER * 510).
[prescaler] can be one of the following:
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_8
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_64
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_256
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1024
[duty_cycle] - Should range from 0 to 255. Actual duty cycle of PWM signal is calculated as duty cycle/255.
*/
inline void Timer0_SetupPhaseCorrectPwmMode(
    unsigned char prescaler,
    unsigned char duty_cycle)
{
    Timer0_Stop();
    OCR0 = duty_cycle;
    Timer0_SetWaveformGenerationMode(TIMER0_WAVEFORM_GENERATION_MODE_PHC_PWM);
    Timer0_SetCompareOutputMode(TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING);
    Timer0_SetClockSource(prescaler);
    Timer0_EnableOC0();
}

#endif
