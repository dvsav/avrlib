#if !defined(_TIMER0_H) && defined (__AVR_ATmega328__)
#define _TIMER0_H

#include <avr/io.h>
#include <avr/interrupt.h>

#include "IOPORT.h"

    #define TIMER0_OC0A_PORT D
    #define TIMER0_OC0A_PIN 6
    #define TIMER0_OC0B_PORT D
    #define TIMER0_OC0B_PIN 5

#define TIMER0_CLOCK_SOURCE_MASK              (0b111 << CS00)
#define TIMER0_CLOCK_SOURCE_NONE              (0b000 << CS00) // No clock source (Timer/counter stopped)
#define TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1    (0b001 << CS00) // clk (No prescaling)
#define TIMER0_CLOCK_SOURCE_CLK_PRESCALE_8    (0b010 << CS00) // clk/8 (From prescaler)
#define TIMER0_CLOCK_SOURCE_CLK_PRESCALE_64   (0b011 << CS00) // clk/64 (From prescaler)
#define TIMER0_CLOCK_SOURCE_CLK_PRESCALE_256  (0b100 << CS00) // clk/256 (From prescaler)
#define TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1024 (0b101 << CS00) // clk/1024 (From prescaler)
#define TIMER0_CLOCK_SOURCE_EXT_T0PIN_FALLING (0b110 << CS00) // External clock source on T0 pin. Clock on falling edge.
#define TIMER0_CLOCK_SOURCE_EXT_T0PIN_RISING  (0b111 << CS00) // External clock source on T0 pin. Clock on rising edge.

#define TIMER0_COMPARE_OUTPUT_MODE_MASK_A                   (0b11 << COM0A0)
#define TIMER0_COMPARE_OUTPUT_MODE_NORMAL_A                 (0b00 << COM0A0) // Non-PWM mode: Normal port operation, OC0A disconnected
#define TIMER0_COMPARE_OUTPUT_MODE_TOGGLE_OC0_A             (0b01 << COM0A0) // Non-PWM mode: Toggle OC0A on Compare Match
#define TIMER0_COMPARE_OUTPUT_MODE_CLEAR_OC0_A              (0b10 << COM0A0) // Non-PWM mode: Clear OC0A on Compare Match
#define TIMER0_COMPARE_OUTPUT_MODE_SET_OC0_A                (0b11 << COM0A0) // Non-PWM mode: Set OC0A on Compare Match
#define TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_NORMAL_A        (0b00 << COM0A0) // Fast PWM mode: Normal port operation, OC0A disconnected
#define TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_A (0b10 << COM0A0) // Fast PWM mode: Clear OC0A on Compare Match, set OC0A at BOTTOM (Non-Inverting)
#define TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_INVERTING_A     (0b11 << COM0A0) // Fast PWM mode: Set OC0A on Compare Match, clear OC0A at BOTTOM (Inverting)
#define TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_NORMAL_A         (0b00 << COM0A0) // Phase Correct PWM mode: Normal port operation, OC0A disconnected
#define TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_A  (0b10 << COM0A0) // Phase Correct PWM mode: Clear OC0A on Compare Match when up-counting. Set OC0A on Compare Match when down-counting.
#define TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_INVERTING_A      (0b11 << COM0A0) // Phase Correct PWM mode: Set OC0A on Compare Match when up-counting. Clear OC0A on Compare Match when down-counting.

#define TIMER0_COMPARE_OUTPUT_MODE_MASK_B                   (0b11 << COM0B0)
#define TIMER0_COMPARE_OUTPUT_MODE_NORMAL_B                 (0b00 << COM0B0) // Non-PWM mode: Normal port operation, OC0B disconnected
#define TIMER0_COMPARE_OUTPUT_MODE_TOGGLE_OC0_B             (0b01 << COM0B0) // Non-PWM mode: Toggle OC0B on Compare Match
#define TIMER0_COMPARE_OUTPUT_MODE_CLEAR_OC0_B              (0b10 << COM0B0) // Non-PWM mode: Clear OC0B on Compare Match
#define TIMER0_COMPARE_OUTPUT_MODE_SET_OC0_B                (0b11 << COM0B0) // Non-PWM mode: Set OC0B on Compare Match
#define TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_NORMAL_B        (0b00 << COM0B0) // Fast PWM mode: Normal port operation, OC0B disconnected
#define TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_B (0b10 << COM0B0) // Fast PWM mode: Clear OC0B on Compare Match, set OC0B at BOTTOM (Non-Inverting)
#define TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_INVERTING_B     (0b11 << COM0B0) // Fast PWM mode: Set OC0B on Compare Match, clear OC0B at BOTTOM (Inverting)
#define TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_NORMAL_B         (0b00 << COM0B0) // Phase Correct PWM mode: Normal port operation, OC0B disconnected
#define TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_B  (0b10 << COM0B0) // Phase Correct PWM mode: Clear OC0B on Compare Match when up-counting. Set OC0B on Compare Match when down-counting.
#define TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_INVERTING_B      (0b11 << COM0B0) // Phase Correct PWM mode: Set OC0B on Compare Match when up-counting. Clear OC0B on Compare Match when down-counting.

#define TIMER0_WAVEFORM_GENERATION_MODE_MASK     (0b11 << WGM00)
#define TIMER0_WAVEFORM_GENERATION_MODE_NORMAL   (0b00 << WGM00) // Normal mode
#define TIMER0_WAVEFORM_GENERATION_MODE_PHC_PWM  (0b01 << WGM00) // Phase Correct Pulse Width Modulation mode
#define TIMER0_WAVEFORM_GENERATION_MODE_CTC      (0b10 << WGM00) // Clear on Compare Match mode
#define TIMER0_WAVEFORM_GENERATION_MODE_FAST_PWM (0b11 << WGM00) // Fast Pulse Width Modulation mode

#define TIMER0_INTERRUPT_MASK               (0b111 << TOIE0)
#define TIMER0_INTERRUPT_DISABLE            (0b000 << TOIE0) // Disable interrupts
#define TIMER0_INTERRUPT_ON_OVERFLOW        (1 << TOIE0)     // Enable overflow interrupt
#define TIMER0_INTERRUPT_ON_COMPARE_MATCH_A (1 << OCIE0A)    // Enable output compare match A interrupt
#define TIMER0_INTERRUPT_ON_COMPARE_MATCH_B (1 << OCIE0B)    // Enable output compare match B interrupt

// Enables Timer0 module (in Power Reduction Register).
inline void Timer0_Enable() { PRR &= ~(1 << PRTIM0); }

// Disables Timer0 module (in Power Reduction Register).
inline void Timer0_Disable() { PRR |= (1 << PRTIM0); }

/*
Sets clock source and prescaler for Timer0.
If [clock_source] is nonzero Timer0 starts counting.
[clock_source] parameter can be one of the following:
    TIMER0_CLOCK_SOURCE_NONE              - No clock source (Timer/counter stopped)
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1    - clk
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_8    - clk/8
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_64   - clk/64
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_256  - clk/256
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1024 - clk/1024
    TIMER0_CLOCK_SOURCE_EXT_T0PIN_FALLING - External clock source on T0 pin. Clock on falling edge.
    TIMER0_CLOCK_SOURCE_EXT_T0PIN_RISING  - External clock source on T0 pin. Clock on rising edge.
*/
inline void Timer0_SetClockSource(unsigned char clock_source)
{
    unsigned char temp = TCCR0B & ~TIMER0_CLOCK_SOURCE_MASK;
    temp |= ( clock_source & TIMER0_CLOCK_SOURCE_MASK );
    TCCR0B = temp;
}

/*
Sets compare output mode for Timer0's OC0A output.
[mode] parameter can be one of the following:
    TIMER0_COMPARE_OUTPUT_MODE_NORMAL_A     - Non-PWM mode: Normal port operation, OC0A disconnected
    TIMER0_COMPARE_OUTPUT_MODE_TOGGLE_OC0_A - Non-PWM mode: Toggle OC0A on Compare Match
    TIMER0_COMPARE_OUTPUT_MODE_CLEAR_OC0_A  - Non-PWM mode: Clear OC0A on Compare Match
    TIMER0_COMPARE_OUTPUT_MODE_SET_OC0_A    - Non-PWM mode: Set OC0A on Compare Match
    TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_NORMAL_A        - Fast PWM mode: Normal port operation, OC0A disconnected
    TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_A - Fast PWM mode: Clear OC0A on Compare Match, set OC0A at BOTTOM (Non-Inverting)
    TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_INVERTING_A     - Fast PWM mode: Set OC0A on Compare Match, clear OC0A at BOTTOM (Inverting)
    TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_NORMAL_A        - Phase Correct PWM mode: Normal port operation, OC0 disconnected
    TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_A - Phase Correct PWM mode: Clear OC0A on Compare Match when up-counting. Set OC0A on Compare Match when down-counting.
    TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_INVERTING_A     - Phase Correct PWM mode: Set OC0A on Compare Match when up-counting. Clear OC0A on Compare Match when down-counting.
*/
inline void Timer0_SetCompareOutputMode_A(unsigned char mode)
{
    unsigned char temp = TCCR0A & ~TIMER0_COMPARE_OUTPUT_MODE_MASK_A;
    temp |= ( mode & TIMER0_COMPARE_OUTPUT_MODE_MASK_A );
    TCCR0A = temp;
}

/*
Sets compare output mode for Timer0's OC0B output.
[mode] parameter can be one of the following:
    TIMER0_COMPARE_OUTPUT_MODE_NORMAL_B     - Non-PWM mode: Normal port operation, OC0B disconnected
    TIMER0_COMPARE_OUTPUT_MODE_TOGGLE_OC0_B - Non-PWM mode: Toggle OC0B on Compare Match
    TIMER0_COMPARE_OUTPUT_MODE_CLEAR_OC0_B  - Non-PWM mode: Clear OC0B on Compare Match
    TIMER0_COMPARE_OUTPUT_MODE_SET_OC0_B    - Non-PWM mode: Set OC0B on Compare Match
    TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_NORMAL_B        - Fast PWM mode: Normal port operation, OC0B disconnected
    TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_B - Fast PWM mode: Clear OC0B on Compare Match, set OC0B at BOTTOM (Non-Inverting)
    TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_INVERTING_B     - Fast PWM mode: Set OC0B on Compare Match, clear OC0B at BOTTOM (Inverting)
    TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_NORMAL_B        - Phase Correct PWM mode: Normal port operation, OC0 disconnected
    TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_B - Phase Correct PWM mode: Clear OC0B on Compare Match when up-counting. Set OC0B on Compare Match when down-counting.
    TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_INVERTING_B     - Phase Correct PWM mode: Set OC0B on Compare Match when up-counting. Clear OC0B on Compare Match when down-counting.
*/
inline void Timer0_SetCompareOutputMode_B(unsigned char mode)
{
    unsigned char temp = TCCR0A & ~TIMER0_COMPARE_OUTPUT_MODE_MASK_B;
    temp |= ( mode & TIMER0_COMPARE_OUTPUT_MODE_MASK_B );
    TCCR0A = temp;
}

/*
Sets waveform generation mode for Timer0.
[mode] parameter can be one of the following:
    TIMER0_WAVEFORM_GENERATION_MODE_NORMAL   - Normal mode
    TIMER0_WAVEFORM_GENERATION_MODE_PHC_PWM  - Phase Correct Pulse Width Modulation mode
    TIMER0_WAVEFORM_GENERATION_MODE_CTC      - Clear on Compare Match mode
    TIMER0_WAVEFORM_GENERATION_MODE_FAST_PWM - Fast Pulse Width Modulation mode
*/
inline void Timer0_SetWaveformGenerationMode(unsigned char mode)
{
    unsigned char temp = TCCR0A & ~TIMER0_WAVEFORM_GENERATION_MODE_MASK;
    temp |= ( mode & TIMER0_WAVEFORM_GENERATION_MODE_MASK );
    TCCR0A = temp;
    TCCR0B &= ~(1 << WGM02);
}

/*
Enables or disables interrupts for Timer0
[mode] parameter can be a bitwise OR combination of the following flags:
    TIMER0_INTERRUPT_DISABLE            - Disable interrupts
    TIMER0_INTERRUPT_ON_OVERFLOW        - Enable overflow interrupt
    TIMER0_INTERRUPT_ON_COMPARE_MATCH_A - Enable output compare match A interrupt
    TIMER0_INTERRUPT_ON_COMPARE_MATCH_B - Enable output compare match B interrupt
*/
inline void Timer0_SetupInterrupts(unsigned char mode)
{
    unsigned char temp = TIMSK0 & ~TIMER0_INTERRUPT_MASK;
    temp |= ( mode & TIMER0_INTERRUPT_MASK );
    TIMSK0 = temp;
}

// Keeps Timer0, Timer1 and Timer2 halted which allows you to make some settings
// for them and then start them all at once (see Timer_ExitSynchronizationMode()).
inline void Timer_EnterSynchronizationMode() { GTCCR |= (1 << TSM); }

// Simultaneously enables Timer0, Timer1 and Timer2 work.
inline void Timer_ExitSynchronizationMode() { GTCCR &= ~(1 << TSM); }

// Resets the prescaler module of Timer0 and Timer1
// for synchronizing the Timer/Counter to program execution.
inline void Timer0_Timer1_ResetPrescaler() { GTCCR |= (1 << PSRSYNC); }

// Gets Timer0 counter register value.
inline unsigned char Timer0_GetTimerCounter() { return TCNT0; }

// Sets Timer0 counter register value.
inline void Timer0_SetTimerCounter(unsigned char value) { TCNT0 = value; }

// Stops Timer0 and clears its counter register.
inline void Timer0_Stop()
{
    Timer0_SetClockSource(TIMER0_CLOCK_SOURCE_NONE);
    TCNT0 = 0; // Reset timer counter register value.
}

// Configures Timer0's Output Complare Match Output pin A as output.
inline void Timer0_EnableOC0A()
{
    IOPORT_CONFIG_PIN_OUT(TIMER0_OC0A_PORT, TIMER0_OC0A_PIN);
}

// Configures Timer0's Output Complare Match Output pin A as input.
inline void Timer0_DisableOC0A()
{
    IOPORT_CONFIG_PIN_IN(TIMER0_OC0A_PORT, TIMER0_OC0A_PIN);
}

// Configures Timer0's Output Complare Match Output pin B as output.
inline void Timer0_EnableOC0B()
{
    IOPORT_CONFIG_PIN_OUT(TIMER0_OC0B_PORT, TIMER0_OC0B_PIN);
}

// Configures Timer0's Output Complare Match Output pin B as input.
inline void Timer0_DisableOC0B()
{
    IOPORT_CONFIG_PIN_IN(TIMER0_OC0B_PORT, TIMER0_OC0B_PIN);
}

inline void Timer0_Init()
{  
    TCCR0B = 0; // Stop Timer0
    TCCR0A = 0; // Set Normal waveform generation mode, set Normal compare output mode for both A and B channels. 
    TCNT0  = 0; // Reset timer counter register value.
    TIMSK0 = 0; // Disable all Timer0 interrupts.
    Timer0_Enable();
}

/*
Sets up CTC mode (Clear Timer on Comapare Match) for Timer0 and sets timer frequency in Hz.
[freq] - timer compare match interrupt frequency in Hz
Limitations: [freq] >= clk / 2^18 Hz (4 Hz  for clk = 1 MHz).
             [freq] <= clk           (1 MHz for clk = 1 MHz).
*/
inline void Timer0_SetupCtcMode(unsigned long freq)
{
    // freq = clk / (PRESCALER * (1 + OCR0A))
    // OCR0A = clk / (PRESCALER * freq) - 1
    
    Timer0_Stop();
    Timer0_SetWaveformGenerationMode(TIMER0_WAVEFORM_GENERATION_MODE_CTC);
    
    if(F_CPU < freq)
    {
        OCR0A = 0;
        Timer0_SetClockSource(TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1);
    }
    if((F_CPU >> 8) < freq)
    {
        OCR0A = F_CPU / freq - 1;
        Timer0_SetClockSource(TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1);
    }
    else if((F_CPU >> 11) < freq)
    {
        OCR0A = (F_CPU >> 3) / freq - 1;
        Timer0_SetClockSource(TIMER0_CLOCK_SOURCE_CLK_PRESCALE_8);
    }
    else if((F_CPU >> 14) < freq)
    {
        OCR0A = (F_CPU >> 6) / freq - 1;
        Timer0_SetClockSource(TIMER0_CLOCK_SOURCE_CLK_PRESCALE_64);
    }
    else if((F_CPU >> 16) < freq)
    {
        OCR0A = (F_CPU >> 8) / freq - 1;
        Timer0_SetClockSource(TIMER0_CLOCK_SOURCE_CLK_PRESCALE_256);
    }
    else if((F_CPU >> 18) < freq)
    {
        OCR0A = (F_CPU >> 10) / freq - 1;
        Timer0_SetClockSource(TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1024);
    }
    else
    {
        OCR0A = 0xFF;
        Timer0_SetClockSource(TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1024);
    }
}

/*
Sets up meander on OC0A output using CTC mode.
[freq] - meander frequency in Hz.
*/
inline void Timer0_SetupMeander_A(unsigned long freq)
{
    Timer0_DisableOC0B();
    Timer0_SetCompareOutputMode_A(TIMER0_COMPARE_OUTPUT_MODE_TOGGLE_OC0_A);
    Timer0_SetupCtcMode(freq << 1);
    Timer0_EnableOC0A();
}

/*
Sets up meander on OC0B output using CTC mode.
[freq] - meander frequency in Hz.
*/
inline void Timer0_SetupMeander_B(unsigned long freq)
{
    Timer0_DisableOC0A();
    OCR0B = 0;
    Timer0_SetCompareOutputMode_B(TIMER0_COMPARE_OUTPUT_MODE_TOGGLE_OC0_B);
    Timer0_SetupCtcMode(freq << 1);
    Timer0_EnableOC0B();
}

/*
Sets up meanders on OC0A and OC0B outputs using CTC mode.
[freq] - meander frequency in Hz.
[phase_shift] - phase shift between OC0B and OC0A meanders as a fraction of meander period (0...1).
*/
inline void Timer0_SetupMeander_AB(
    unsigned long freq,
    float phase_shift)
{
    Timer0_SetCompareOutputMode_A(TIMER0_COMPARE_OUTPUT_MODE_TOGGLE_OC0_A);
    Timer0_SetCompareOutputMode_B(TIMER0_COMPARE_OUTPUT_MODE_TOGGLE_OC0_B);
    Timer0_SetupCtcMode(freq << 1);
    OCR0B = static_cast<unsigned char>(phase_shift * OCR0A);
    Timer0_EnableOC0A();
    Timer0_EnableOC0B();
}

/*
Sets up Fast Pulse Width Modulation mode for Timer0 on OC0A output.
PWM frequency equals clk/(PRESCALER * 256).
[prescaler] can be one of the following:
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_8
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_64
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_256
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1024
[duty_cycle] - Should range from 0 to 255. Actual duty cycle of PWM signal is calculated as duty cycle/255.
*/
inline void Timer0_SetupFastPwmMode_A(
    unsigned char prescaler,
    unsigned char duty_cycle)
{
    Timer0_Stop();
    Timer0_DisableOC0B();
    OCR0A = duty_cycle;
    Timer0_SetWaveformGenerationMode(TIMER0_WAVEFORM_GENERATION_MODE_FAST_PWM);
    Timer0_SetCompareOutputMode_A(TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_A);
    Timer0_SetClockSource(prescaler);
    Timer0_EnableOC0A();
}

/*
Sets up Fast Pulse Width Modulation mode for Timer0 on OC0B output.
PWM frequency equals clk/(PRESCALER * 256).
[prescaler] can be one of the following:
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_8
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_64
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_256
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1024
[duty_cycle] - Should range from 0 to 255. Actual duty cycle of PWM signal is calculated as duty cycle/255.
*/
inline void Timer0_SetupFastPwmMode_B(
    unsigned char prescaler,
    unsigned char duty_cycle)
{
    Timer0_Stop();
    Timer0_DisableOC0A();
    OCR0B = duty_cycle;
    Timer0_SetWaveformGenerationMode(TIMER0_WAVEFORM_GENERATION_MODE_FAST_PWM);
    Timer0_SetCompareOutputMode_B(TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_B);
    Timer0_SetClockSource(prescaler);
    Timer0_EnableOC0B();
}

/*
Sets up Fast Pulse Width Modulation mode for Timer0 on OC0A and OC0B outputs.
PWM frequency equals clk/(PRESCALER * 256).
[prescaler] can be one of the following:
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_8
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_64
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_256
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1024
[duty_cycle_A], [duty_cycle_B] - Should range from 0 to 255. Actual duty cycle of PWM signal is calculated as duty cycle/255.
*/
inline void Timer0_SetupFastPwmMode_AB(
    unsigned char prescaler,
    unsigned char duty_cycle_A,
    unsigned char duty_cycle_B)
{
    Timer0_Stop();
    OCR0A = duty_cycle_A;
    OCR0B = duty_cycle_B;
    Timer0_SetWaveformGenerationMode(TIMER0_WAVEFORM_GENERATION_MODE_FAST_PWM);
    Timer0_SetCompareOutputMode_A(TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_A);
    Timer0_SetCompareOutputMode_B(TIMER0_COMPARE_OUTPUT_MODE_FAST_PWM_NON_INVERTING_B);
    Timer0_SetClockSource(prescaler);
    Timer0_EnableOC0A();
    Timer0_EnableOC0B();
}

/*
Sets up Phase Correct Pulse Width Modulation mode for Timer0 on OC0A output.
PWM frequency equals clk/(PRESCALER * 510).
[prescaler] can be one of the following:
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_8
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_64
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_256
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1024
[duty_cycle] - Should range from 0 to 255. Actual duty cycle of PWM signal is calculated as duty cycle/255.
*/
inline void Timer0_SetupPhaseCorrectPwmMode_A(
    unsigned char prescaler,
    unsigned char duty_cycle)
{
    Timer0_Stop();
    Timer0_DisableOC0B();
    OCR0A = duty_cycle;
    Timer0_SetWaveformGenerationMode(TIMER0_WAVEFORM_GENERATION_MODE_PHC_PWM);
    Timer0_SetCompareOutputMode_A(TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_A);
    Timer0_SetClockSource(prescaler);
    Timer0_EnableOC0A();
}

/*
Sets up Phase Correct Pulse Width Modulation mode for Timer0 on OC0B output.
PWM frequency equals clk/(PRESCALER * 510).
[prescaler] can be one of the following:
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_8
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_64
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_256
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1024
[duty_cycle] - Should range from 0 to 255. Actual duty cycle of PWM signal is calculated as duty cycle/255.
*/
inline void Timer0_SetupPhaseCorrectPwmMode_B(
    unsigned char prescaler,
    unsigned char duty_cycle)
{
    Timer0_Stop();
    Timer0_DisableOC0A();
    OCR0B = duty_cycle;
    Timer0_SetWaveformGenerationMode(TIMER0_WAVEFORM_GENERATION_MODE_PHC_PWM);
    Timer0_SetCompareOutputMode_B(TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_B);
    Timer0_SetClockSource(prescaler);
    Timer0_EnableOC0B();
}

/*
Sets up Phase Correct Pulse Width Modulation mode for Timer0 on OC0A and OC0B outputs.
PWM frequency equals clk/(PRESCALER * 510).
[prescaler] can be one of the following:
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_8
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_64
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_256
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1024
[duty_cycle_A], [duty_cycle_B] - Should range from 0 to 255. Actual duty cycle of PWM signal is calculated as duty cycle/255.
*/
inline void Timer0_SetupPhaseCorrectPwmMode_AB(
    unsigned char prescaler,
    unsigned char duty_cycle_A,
    unsigned char duty_cycle_B)
{
    Timer0_Stop();
    OCR0A = duty_cycle_A;
    OCR0B = duty_cycle_B;
    Timer0_SetWaveformGenerationMode(TIMER0_WAVEFORM_GENERATION_MODE_PHC_PWM);
    Timer0_SetCompareOutputMode_A(TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_A);
    Timer0_SetCompareOutputMode_B(TIMER0_COMPARE_OUTPUT_MODE_PHC_PWM_NON_INVERTING_B);
    Timer0_SetClockSource(prescaler);
    Timer0_EnableOC0A();
    Timer0_EnableOC0B();
}

/*
Enables interrupt on Timer0 overflow event (all other functions of Timer0 will be disabled) and
starts counting according to specified [clock_source]. Interrupt frequency will be equal to
timer clock source frequency divided by 256.
[clock_source] parameter can be one of the following:
    TIMER0_CLOCK_SOURCE_NONE              - No clock source (Timer/counter stopped)
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1    - clk
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_8    - clk/8
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_64   - clk/64
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_256  - clk/256
    TIMER0_CLOCK_SOURCE_CLK_PRESCALE_1024 - clk/1024
    TIMER0_CLOCK_SOURCE_EXT_T0PIN_FALLING - External clock source on T0 pin. Clock on falling edge.
    TIMER0_CLOCK_SOURCE_EXT_T0PIN_RISING  - External clock source on T0 pin. Clock on rising edge.
Remarks: use #include <avr/interrupt.h> ISR(TIMER0_OVF_vect) { ... } for interrupt handling
*/
inline void Timer0_InterruptOnOverflow(unsigned char clock_source)
{
    Timer0_Init();
    Timer0_SetupInterrupts(TIMER0_INTERRUPT_ON_OVERFLOW);
    Timer0_SetClockSource(clock_source);
    sei();
}

/*
Enables interrupt on Timer0 compare match A event (all other functions of Timer0 will be disabled) and
starts counting. Interrupts will be happening with specified frequency [freq].
[freq] - timer compare match interrupt frequency in Hz
Limitations: [freq] >= clk / 2^18 Hz (4 Hz  for clk = 1 MHz).
             [freq] <= clk           (1 MHz for clk = 1 MHz).
Remarks: use #include <avr/interrupt.h> ISR(TIMER0_COMPA_vect) { ... } for interrupt handling
*/
inline void Timer0_InterruptOnCompareMatchA(unsigned long freq)
{
    Timer0_Init();
    Timer0_SetupInterrupts(TIMER0_INTERRUPT_ON_COMPARE_MATCH_A);
    Timer0_SetupCtcMode(freq);
    sei();
}

#endif
