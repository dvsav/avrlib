# AvrLib

AvrLib is a C++ SDK for Atmel AVR microcontrollers.

## Getting Started

Install [git version control system](https://git-scm.com/) on your computer.
Launch git. In the command prompt go to the folder where you want to copy AvrLib to.
Type: `git clone https://bitbucket.org/dvsav/avrlib.git` and there you are.

## Build

The project can be built with [WinAVR](https://sourceforge.net/projects/winavr/) and [AVR Studio 4.19](https://www.microchip.com/mplab/avr-support/avr-and-sam-downloads-archive).

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Miscellaneous

You can checkout [the project author's blog](https://dvsav.ru) if you can read in Russian.