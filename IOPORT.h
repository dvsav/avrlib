#ifndef _IOPORT_H
#define _IOPORT_H

#include <avr/io.h>

/*
Configures a whole port as input.
Parameters: PRT - port name (A, B, C, D)
*/
#define IOPORT_CONFIG_PORT_IN_NX(PRT) DDR ## PRT = 0x00
#define IOPORT_CONFIG_PORT_IN(PRT) IOPORT_CONFIG_PORT_IN_NX(PRT)

/*
Configures a whole port as output.
Parameters: PRT - port name (A, B, C, D)
*/
#define IOPORT_CONFIG_PORT_OUT_NX(PRT) DDR ## PRT = 0xFF
#define IOPORT_CONFIG_PORT_OUT(PRT) IOPORT_CONFIG_PORT_OUT_NX(PRT)

/*
Configures a single pin of a port as input.
Parameters:
PRT - port name (A, B, C, D)
N   - pin number (0, 1, 2, 3, 4, 5, 6, 7)
*/
#define IOPORT_CONFIG_PIN_IN_NX(PRT, N) DDR ## PRT &= ~(1 << N)
#define IOPORT_CONFIG_PIN_IN(PRT, N) IOPORT_CONFIG_PIN_IN_NX(PRT, N)

/*
Configures a single pin of a port as output.
Parameters:
PRT - port name (A, B, C, D)
N   - pin number (0, 1, 2, 3, 4, 5, 6, 7)
*/
#define IOPORT_CONFIG_PIN_OUT_NX(PRT, N) DDR ## PRT |= (1 << N)
#define IOPORT_CONFIG_PIN_OUT(PRT, N) IOPORT_CONFIG_PIN_OUT_NX(PRT, N)

/*
Sets a single pin of a port to 1.
Parameters:
PRT - port name (A, B, C, D)
N   - pin number (0, 1, 2, 3, 4, 5, 6, 7)
*/
#define IOPORT_SET_PIN_NX(PRT, N) PORT ## PRT |= (1 << N)
#define IOPORT_SET_PIN(PRT, N) IOPORT_SET_PIN_NX(PRT, N)

/*
Resets a single pin of a port to 0.
Parameters:
PRT - port name (A, B, C, D)
N   - pin number (0, 1, 2, 3, 4, 5, 6, 7)
*/
#define IOPORT_RESET_PIN_NX(PRT, N) PORT ## PRT &= ~(1 << N)
#define IOPORT_RESET_PIN(PRT, N) IOPORT_RESET_PIN_NX(PRT, N)

/*
Inverts a single pin of a port.
Parameters:
PRT - port name (A, B, C, D)
N   - pin number (0, 1, 2, 3, 4, 5, 6, 7)
*/
#define IOPORT_INVERT_PIN_NX(PRT, N) PORT ## PRT ^= (1 << N)
#define IOPORT_INVERT_PIN(PRT, N) IOPORT_INVERT_PIN_NX(PRT, N)

/*
Gets the state of a single pin of a port.
Parameters:
PRT - port name (A, B, C, D)
N   - pin number (0, 1, 2, 3, 4, 5, 6, 7)
*/
#define IOPORT_GET_PIN_NX(PRT, N) (PIN ## PRT & (1 << N))
#define IOPORT_GET_PIN(PRT, N) IOPORT_GET_PIN_NX(PRT, N)

/*
Tests a single pin of a port. Expands to boolean expression which is
true if pin is 1 and false otherwise.
Parameters:
PRT - port name (A, B, C, D)
N   - pin number (0, 1, 2, 3, 4, 5, 6, 7)
*/
#define IOPORT_TEST_PIN_NX(PRT, N) ((PIN ## PRT & (1 << N)) != 0)
#define IOPORT_TEST_PIN(PRT, N) IOPORT_TEST_PIN_NX(PRT, N)

#endif
