#include "OneWireInterface.h"

#define OWI_BIT_SET_0 IOPORT_CONFIG_PIN_OUT(OWI_PORT, OWI_BIT); // Configure OWI_PORT.OWI_BIT as output. OWI_BIT pulled down to 0.
#define OWI_BIT_SET_1 IOPORT_CONFIG_PIN_IN(OWI_PORT, OWI_BIT);  // Configure OWI_PORT.OWI_BIT as input. OWI_BIT pulled up to 1.
#define OWI_BIT_RELEASE OWI_BIT_SET_1
#define OWI_GET_BIT_VALUE IOPORT_GET_PIN(OWI_PORT, OWI_BIT)

unsigned char OWI_Reset()
{
    OWI_BIT_SET_0
    _delay_us(480);
    
    OWI_BIT_RELEASE
    _delay_us(15);
    
    unsigned char counter = 0;
    while(OWI_GET_BIT_VALUE != 0)
    {
        _delay_us(15);
        if(++counter > 4)
            return OWI_PRESENCE_NOT_DETECTED; // Presence signal not detected.
    }

    _delay_us(240);
    
    counter = 0;
    while(OWI_GET_BIT_VALUE == 0)
    {
        _delay_us(60);
        if(++counter > 4)
            return OWI_INVALID_LINE_STATE; // Somebody illegally pulls the line down.
    }
    
    return OWI_OK;
}

void OWI_Write_0()
{
    OWI_BIT_SET_0
    _delay_us(70);
    OWI_BIT_RELEASE
    _delay_us(10);
}

void OWI_Write_1()
{
    OWI_BIT_SET_0
    _delay_us(5);
    OWI_BIT_SET_1;
    _delay_us(60);
}

unsigned char OWI_ReadBit()
{
    OWI_BIT_SET_0
    _delay_us(5);
    OWI_BIT_RELEASE
    _delay_us(10);
    
    unsigned char value = (OWI_GET_BIT_VALUE != 0);
    
    _delay_us(70);
    
    return value;
}

void OWI_WriteByte(unsigned char byte)
{
    for(int i = 0; i < 8; i++)
    {
        if(((byte >> i) & 0x01) == 0)
            OWI_Write_0();
        else
            OWI_Write_1();
    }
}

unsigned char OWI_ReadByte()
{
    unsigned char byte = 0;
    for(int i = 0; i < 8; i++)
    {
        if(OWI_ReadBit() != 0)
            byte |= (1 << i);
    }
    return byte;
}

#undef OWI_BIT_SET_0
#undef OWI_BIT_SET_1
#undef OWI_BIT_RELEASE
#undef OWI_GET_BIT_VALUE
