// One-Wire Interface (OWI) API

#ifndef _ONE_WIRE_INTERFACE_H
#define _ONE_WIRE_INTERFACE_H

#include <avr/io.h>
#include <util/delay.h>

#include "IOPORT.h"

/*
**********************************************************************
YOU HAVE TO DEFINE THE FOLLOWING MACRODEFINITIONS
WHEREEVER YOU USE THIS HEADER FILE:

// Defines the PORT register of I/O port used for 1-wire communication.
#define OWI_PORT X
X = A, B, C or D

// Defines the bit number of I/O port used for 1-wire communication.
#define OWI_BIT N
N = 0...7
**********************************************************************
*/

// Return codes of OWI functions.
#define OWI_OK 0 // Everything is Ok.
#define OWI_PRESENCE_NOT_DETECTED 1 // Slave didn't respond in time.
#define OWI_INVALID_LINE_STATE 2 // Master detected unexpected state of the 1-wire line.

// ROM commands
#define OWI_COMMAND_SKIP_ROM   0xCC
#define OWI_COMMAND_MATCH_ROM  0x55
#define OWI_COMMAND_SEARCH_ROM 0xF0
#define OWI_COMMAND_READ_ROM   0x33

// Initializes the microcontroller's I/O port bit for communication via 1-wire interface.
inline void OWI_Init()
{
    IOPORT_CONFIG_PIN_IN(OWI_PORT, OWI_BIT); // Configure OWI pin as input.
    IOPORT_RESET_PIN(OWI_PORT, OWI_BIT);     // As long as the pin is configured as output its output value is zero.
}

/*
Sends RESET signal to the bus and reads PRESENCE signal from slave devices.
Returns OWI_OK If the PRESENCE signal has been detected.
Returns OWI_PRESENCE_NOT_DETECTED if the PRESENCE signal has not been detected.
Returns OWI_INVALID_LINE_STATE if the bus data line had unexpected state during the process.
*/
unsigned char OWI_Reset();

// Sends bit 0 to the bus.
void OWI_Write_0();

// Sends bit 1 to the bus.
void OWI_Write_1();

// Reads a single bit from the bus.
unsigned char OWI_ReadBit();

// Sends one byte to the bus.
void OWI_WriteByte(unsigned char byte);

// Reads one byte from the bus.
unsigned char OWI_ReadByte();

#endif
