#ifndef _TWI_H
#define _TWI_H

#include <avr/io.h>
#include <avr/interrupt.h>

#define TWI_ERROR 1
#define TWI_SUCCESS 0

/*
Initializes TWI.
Sets SCL frequency = F_CPU / (16 + 2 * bit_rate * 4^prescaler)
Parameters:
[bit_rate]  - content of TWBR register.
[prescaler] - content of two low bits of TWSR register (can be 0, 1, 2, 3).
*/
void TWI_Init(
    unsigned char bit_rate,
    unsigned char prescaler);

/*
Sets prescaler. SCL frequency = F_CPU / (16 + 2 * bit_rate * 4^prescaler)
Parameters: [prescaler] - content of two low bits of TWSR register (can be 0, 1, 2, 3).
*/
inline void TWI_SetPrescaler(
    unsigned char prescaler)
{
    TWSR = (TWSR & 0b11111100) | (prescaler & 0b00000011);
}

/*
Sets bit rate. SCL frequency = F_CPU / (16 + 2 * bit_rate * 4^prescaler)
Parameters: [bit_rate]  - content of TWBR register.
*/
inline void TWI_SetBitRate(
    unsigned char bit_rate)
{
    TWBR = bit_rate;
}
    
// Sends start condition.
unsigned char TWI_SendStartCondition();

// Sends repeated start condition.
unsigned char TWI_SendRepeatedStartCondition();

/*
Starts write transaction.
Parameters: [slaveAddress] - slave address.
*/
unsigned char TWI_TransmitSlaveAddressWrite(
    unsigned char slaveAddress);

/*
Starts read transaction.
Parameters: [slaveAddress] - slave address.
*/
unsigned char TWI_TransmitSlaveAddressRead(
    unsigned char slaveAddress);

/*
Transmits data byte during the write transaction.
Parameters: [dataByte] - data byte.
*/
unsigned char TWI_TransmitDataByte(
    unsigned char dataByte);

// Sends stop condition.
void TWI_SendStopCondition();

/*
Accepts data byte and sends ACK.
Parameters: [datadByte] - data byte.
*/
unsigned char TWI_ReceiveDataByteAcknowledge(
    unsigned char* datadByte);

/*
Accepts data byte and sends NACK.
Parameters: [datadByte] - data byte.
*/
unsigned char TWI_ReceiveDataByteNotacknowledge(
    unsigned char* datadByte);

#endif
