#include "TWI.h"

#define TWI_STATUS_START 0x08               // A START condition has been transmitted
#define TWI_STATUS_REPEATED_START 0x10      // A repeated START condition has been transmitted
#define TWI_STATUS_SLA_W_ACK 0x18           // SLA+W has been transmitted; ACK has been received
#define TWI_STATUS_SLA_W_NACK 0x20          // SLA+W has been transmitted; NOT ACK has been received
#define TWI_STATUS_MT_DATA_ACK 0x28         // Master transmitted data byte; ACK has been received
#define TWI_STATUS_MT_DATA_NACK 0x30        // Master transmitted data byte; NOT ACK has been received
#define TWI_STATUS_SLA_R_ACK 0x40           // SLA+R has been transmitted; ACK has been received
#define TWI_STATUS_SLA_R_NACK 0x48          // SLA+R has been transmitted; NOT ACK has been received
#define TWI_STATUS_MR_DATA_ACK 0x50         // Master received data byte; ACK has been returned
#define TWI_STATUS_MR_DATA_NACK 0x58        // Master received data byte; NOT ACK has been returned
#define TWI_STATUS_MT_ARBITRATION_LOST 0x38 // Arbitration lost in SLA+W or data bytes

void TWI_Init(
    unsigned char bit_rate,
    unsigned char prescaler)
{
    TWI_SetBitRate(bit_rate);
    TWI_SetPrescaler(prescaler);
}

unsigned char TWI_SendStartCondition()
{
    // Send START condition
    TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
    
    // Wait for TWINT Flag set
    while (!(TWCR & (1 << TWINT))) {}
    
    // Check value of TWI Status Register
    if ((TWSR & 0xF8) != TWI_STATUS_START)
        return TWI_ERROR;
    
    return TWI_SUCCESS;
}

unsigned char TWI_SendRepeatedStartCondition()
{
    // Send START condition
    TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
    
    // Wait for TWINT Flag set
    while (!(TWCR & (1 << TWINT))) {}
    
    // Check value of TWI Status Register
    if ((TWSR & 0xF8) != TWI_STATUS_REPEATED_START)
        return TWI_ERROR;
    
    return TWI_SUCCESS;
}

unsigned char TWI_TransmitSlaveAddressWrite(
    unsigned char slaveAddress)
{
    slaveAddress &= 0xFE;
    
    // Load slave address write into TWDR Register and start transmission
    TWDR = slaveAddress;
    TWCR = (1 << TWINT) | (1 << TWEN);
    
     // Wait for TWINT Flag set
    while (!(TWCR & (1 << TWINT))) {}
   
    // Check value of TWI Status Register
    if ((TWSR & 0xF8) != TWI_STATUS_SLA_W_ACK)
        return TWI_ERROR;
    
    return TWI_SUCCESS;
}

unsigned char TWI_TransmitSlaveAddressRead(
    unsigned char slaveAddress)
{
    slaveAddress |= 0x01;

    // Load slave address write into TWDR Register and start transmission
    TWDR = slaveAddress;
    TWCR = (1 << TWINT) | (1 << TWEN);
    
     // Wait for TWINT Flag set
    while (!(TWCR & (1 << TWINT))) {}
   
    // Check value of TWI Status Register
    if ((TWSR & 0xF8) != TWI_STATUS_SLA_R_ACK) // 0x40
        return TWI_ERROR;
    
    return TWI_SUCCESS;
}

unsigned char TWI_TransmitDataByte(
    unsigned char dataByte)
{
    // Load data into TWDR Register and start transmission
    TWDR = dataByte;
    TWCR = (1 << TWINT) | (1 << TWEN);

     // Wait for TWINT Flag set
    while (!(TWCR & (1 << TWINT))) {}
    
    // Check value of TWI Status Register
    if ((TWSR & 0xF8) != TWI_STATUS_MT_DATA_ACK)
        return TWI_ERROR;
    
    return TWI_SUCCESS;
}

void TWI_SendStopCondition()
{
    // Transmit STOP condition
    TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);
}

unsigned char TWI_ReceiveDataByteAcknowledge(
    unsigned char* datadByte)
{
    TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA);
    
     // Wait for TWINT Flag set
    while (!(TWCR & (1 << TWINT))) {}
    
    // Check value of TWI Status Register
    if ((TWSR & 0xF8) != TWI_STATUS_MR_DATA_ACK) // 0x50
        return TWI_ERROR;
    
    *datadByte = TWDR;
    return TWI_SUCCESS;
}

unsigned char TWI_ReceiveDataByteNotacknowledge(
    unsigned char* datadByte)
{
    TWCR = (1 << TWINT) | (1 << TWEN);
    
     // Wait for TWINT Flag set
    while (!(TWCR & (1 << TWINT))) {}
    
    // Check value of TWI Status Register
    if ((TWSR & 0xF8) != TWI_STATUS_MR_DATA_NACK) // 0x50
        return TWI_ERROR;
    
    *datadByte = TWDR;
    return TWI_SUCCESS;
}
